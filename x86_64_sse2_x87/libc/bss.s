; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace libc
align 64
libc.handle	rq 1
printf		rq 1
dprintf		rq 1
stdout		rq 1
fflush		rq 1
end namespace
