; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
extrn dlopen
extrn dlsym
extrn dlclose
;---------------------------------------------------------------------------------------------------
libc: namespace libc
align_nops
init_once:
	; we do align the stack here for the series of external calls
	enter 0,0
	and rsp, not 0xf

	lea rdi, [libc.name]
	mov rsi, 0x0000000000000001	; RTLD_LAZY
	call PLT.dlopen
	test rax, rax
		jz .err_libc_open_failed
	mov [libc.handle], rax

	mov rdi, rax
	lea rsi, [printf.sym]
	call PLT.dlsym
	test rax, rax
		jz .err_libc_printf_dlsym_failed
	mov [printf], rax

	mov rdi, [libc.handle]
	lea rsi, [dprintf.sym]
	call PLT.dlsym
	test rax, rax
		jz .err_libc_dprintf_dlsym_failed
	mov [dprintf], rax

	mov rdi, [libc.handle]
	lea rsi, [stdout.sym]
	call PLT.dlsym
	test rax, rax
		jz .err_libc_stdout_dlsym_failed
	mov [stdout], rax

	mov rdi, [libc.handle]
	lea rsi, [fflush.sym]
	call PLT.dlsym
	test rax, rax
		jz .err_libc_fflush_dlsym_failed
	mov [fflush], rax
	;-------------------------------------------------------------------------------------------
	stc
	leave
	ret
	
.exit_nok:
	clc
	leave
	ret

.err_libc_open_failed:
	mov rax, linux.write
	mov rdi, 2 ; stderr
	lea rsi, [msg.err_libc_open_failed]
	mov rdx, msg.err_libc_open_failed.bytes_n
	syscall
	jmp .exit_nok
.err_libc_printf_dlsym_failed:
	mov rax, linux.write
	mov rdi, 2 ; stderr
	lea rsi, [msg.err_printf_dlsym_failed]
	mov rdx, msg.err_printf_dlsym_failed.bytes_n
	syscall
	jmp .exit_nok
.err_libc_dprintf_dlsym_failed:
	mov rax, linux.write
	mov rdi, 2 ; stderr
	lea rsi, [msg.err_dprintf_dlsym_failed]
	mov rdx, msg.err_dprintf_dlsym_failed.bytes_n
	syscall
	jmp .exit_nok
.err_libc_stdout_dlsym_failed:
	mov rax, linux.write
	mov rdi, 2 ; stderr
	lea rsi, [msg.err_stdout_dlsym_failed]
	mov rdx, msg.err_stdout_dlsym_failed.bytes_n
	syscall
	jmp .exit_nok
.err_libc_fflush_dlsym_failed:
	mov rax, linux.write
	mov rdi, 2 ; stderr
	lea rsi, [msg.err_fflush_dlsym_failed]
	mov rdx, msg.err_fflush_dlsym_failed.bytes_n
	syscall
	jmp .exit_nok
;---------------------------------------------------------------------------------------------------
align_nops
fini_once:
	enter 0,0
	and rsp, not 0xf

	mov rdi, [libc.handle]
	call PLT.dlclose ; 0 on success
	leave
	ret
end namespace
