; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace libc
libc.name	db 'libc.so.6',0
printf.sym	db 'printf',0
dprintf.sym	db 'dprintf',0
stdout.sym	db 'stdout',0
fflush.sym	db 'fflush',0
msg: namespace msg
	err_libc_open_failed		db 'ERROR:unable to open libc.so.6',10
	.bytes_n = $ - .
	err_dprintf_dlsym_failed	db 'ERROR:libc:unable to resolve dprintf',10
	.bytes_n = $ - .
	err_printf_dlsym_failed		db 'ERROR:libc:unable to resolve printf',10
	.bytes_n = $ - .
	err_stdout_dlsym_failed		db 'ERROR:libc:unable to resolve stdout',10
	.bytes_n = $ - .
	err_fflush_dlsym_failed		db 'ERROR:libc:unable to resolve fflush',10
	.bytes_n = $ - .
end namespace ; msg
end namespace ; libc
