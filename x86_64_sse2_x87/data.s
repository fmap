; vim: set filetype=fasm foldmethod=marker commentstring=;%s :
msg: namespace msg
	args: namespace args
		usage db	'fmap [OPTIONS...] PBF_FILE_PATH TILE_DB_DIRECTORY_PATH',10,\
			 	'OPTIONS:',10,\
				9,'-z zoom_level (default=16)',10,\
				9,'-swlat bounding box south west latitute in nanodegrees(default=none)',10,\
				9,'-swlon bounding box south west longitude in nanodegrees (default=none)',10,\
				9,'-nelat bounding box north east latitute in nanodegrees (default=none)',10,\
				9,'-nelon bounding box north east longitude in nanodegrees (default=none)',10,0
		zoom.err_val_missing db 'ERROR:COMMAND ARGUMENTS:missing zoom (-z) value',10,0
		swlat.err_val_missing db 'ERROR:COMMAND ARGUMENTS:missing bounding box south west latitude (-swlat) value',10,0
		swlon.err_val_missing db 'ERROR:COMMAND ARGUMENTS:missing bounding box south west longitude (-swlon) value',10,0
		nelat.err_val_missing db 'ERROR:COMMAND ARGUMENTS:missing bounding box north east latitude (-nelat) value',10,0
		nelon.err_val_missing db 'ERROR:COMMAND ARGUMENTS:missing bounding box north east longitude (-nelon) value',10,0
	end namespace
end namespace
