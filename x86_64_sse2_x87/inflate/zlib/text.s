; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
inflate: namespace inflate
init_once:
	enter 0,0
	and rsp, not 0xf

	lea rdi, [module.name]
	mov rsi, 0x0000000000000001	; RTLD_LAZY
	call PLT.dlopen
	test rax, rax
		jz .err_module_open_failed
	mov [module.handle], rax

	symbol inflateInit
	symbol inflateEnd
	symbol inflate
	stc
.exit:
	leave
	ret
error module_open,		module_open_failed,	module.name
error module_inflateInit_sym,	sym_resolution_failed,	inflateInit.sym 
error module_inflateEnd_sym,	sym_resolution_failed,	inflateEnd.sym
error module_inflate_sym,	sym_resolution_failed,	inflate.sym
;---------------------------------------------------------------------------------------------------
; in: rdi = inflate_bytes_start
; in: rsi = inflate_bytes_n_max
; in: rdx = deflate_bytes_start
; in: rcx = deflate_bytes_n
define inflate_bytes_start		rbx
define inflate_bytes_n_max		r12
define inflate_bytes_n_max_dword	r12d ; 32bits dword
define deflate_bytes_start		r13
define deflate_bytes_n 			r14
define deflate_bytes_n_dword		r14d ; 32bits dword
align_nops
do:
	enter 4 * 8, 0
	and rsp, not 0xf ; align the stack here for external calls
	mov [rbp - 8 * 1], inflate_bytes_start
	mov [rbp - 8 * 2], inflate_bytes_n_max
	mov [rbp - 8 * 3], deflate_bytes_start
	mov [rbp - 8 * 4], deflate_bytes_n

	mov inflate_bytes_start, rdi
	mov inflate_bytes_n_max, rsi
	mov deflate_bytes_start, rdx
	mov deflate_bytes_n, rcx

	mov qword [z_stream + z_stream_t.zalloc], 0
	mov qword [z_stream + z_stream_t.zfree], 0
	mov dword [z_stream + z_stream_t.avail_in], 0
	mov qword [z_stream + z_stream_t.next_in], 0
	lea rdi, [z_stream]
	lea rsi, [zlib_version]
	mov rdx, z_stream_t.bytes_n
	call qword [inflateInit]
	test rax, rax
		jnz .exit_nok

	mov qword [z_stream + z_stream_t.next_in], deflate_bytes_start
	mov dword [z_stream + z_stream_t.avail_in], deflate_bytes_n_dword
	mov qword [z_stream + z_stream_t.next_out], inflate_bytes_start
	mov dword [z_stream + z_stream_t.avail_out], inflate_bytes_n_max_dword
	lea rdi, [z_stream]
	mov rsi, 4 ; = Z_FINISH we want a 1 call inflate
	call qword [inflate]
	cmp rax, 1 ; = Z_STREAM_END
	setne bl ; error or deflate not performed in 1 call (bad omens)

	lea rdi, [z_stream]
	call qword [inflateEnd]
	test bl, bl
		jnz .exit_nok
.exit_ok:
	stc
.exit:
	mov inflate_bytes_start, [rbp - 8 * 1]
	mov inflate_bytes_n_max, [rbp - 8 * 2]
	mov deflate_bytes_start, [rbp - 8 * 3]
	mov deflate_bytes_n, [rbp - 8 * 4]
	leave
	ret
.exit_nok:
	clc
	jmp .exit
;---------------------------------------------------------------------------------------------------
purge inflate_bytes_start
purge inflate_bytes_n_max
purge inflate_bytes_n_max_dword
purge deflate_bytes_start
purge deflate_bytes_n
purge deflate_bytes_n_dword
;---------------------------------------------------------------------------------------------------
fini_once:
	enter 0,0
	and rsp, not 0xf

	mov rdi, [module.handle]
	call PLT.dlclose ; 0 on success

	leave
	ret
;---------------------------------------------------------------------------------------------------
;{{{ macros
macro symbol name
	mov rdi, [module.handle]
	lea rsi, [name.sym]
	call PLT.dlsym
	test rax, rax
		jz .err_module_#name#_sym_failed
	mov [name], rax
end macro

macro error name, fmt, s
.err_#name#_failed:
	lea rdi, [msg.fmt]
	lea rsi, [s]
	call qword [libc.printf]
	clc
	jmp .exit
end macro
;}}} macros
end namespace
