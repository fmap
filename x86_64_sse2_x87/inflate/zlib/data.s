; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace inflate
module.name		db 'libz.so.1',0
inflateInit.sym		db 'inflateInit_',0 ; inflateInit is only a macro
inflateEnd.sym		db 'inflateEnd',0
inflate.sym		db 'inflate',0
zlib_version		db '1.2.11',0 ; needed for the real inflateInit_
msg: namespace msg
	module_open_failed	db 'unable to dlopen "%s" module',10,0
	sym_resolution_failed	db 'unable to dlsym "%s" symbol',10,0
end namespace ; msg
end namespace
