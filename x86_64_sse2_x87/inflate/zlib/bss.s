; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace inflate
align 64
z_stream	rb z_stream_t.bytes_n 
align 64
module.handle	rq 1
inflateInit	rq 1
inflateEnd	rq 1
inflate		rq 1
;---------------------------------------------------------------------------------------------------
z_stream_t: namespace z_stream_t
	next_in		:= 0	; qw, next input byte
	avail_in	:= 8	; dw, number of bytes available at next_in
	;dw pad
	total_in	:= 16	; qw, total number of input bytes read so far

	next_out	:= 24	; qw, next output byte will go here
	avail_out	:= 32	; dw, remaining free space at next_out
	;dw pad
	total_out	:= 40 	; qw, total number of bytes output so far

	msg		:= 48	; qw, last error message, NULL if no error
	state		:= 56	; qw, not visible by applications

	zalloc		:= 64	; qw, used to allocate the internal state
	zfree		:= 72	; qw, used to free the internal state
	opaque		:= 80	; qw, private data object passed to zalloc and zfree

	data_type	:= 88	; dw, best guess about the data type: binary or tex
	; dw pad
	; for deflate, or the decoding state for inflate
	adler		:= 96	; qw, Adler-32 or CRC-32 value of the uncompressed data
	reserved	:= 104	; qw, reserved for future use
	bytes_n		:= 112
end namespace ; z_stream_t
end namespace
