; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
features: namespace features
;{{{ init ------------------------------------------------------------------------------------------
align_nops
init:
	TRACE 'PBF:FEATURES:INIT'
	mov rbp, rsp
	and rsp, not 0xf

	mov rax, [ctx]
	movzx rcx, byte [rax + ctx_t.zoom]
	mov rbx, rcx; cpy zoom for later
	; (1<<zoom)*sizeof(ptr=8 bytes) = 2^zoom * 2^3 = 2^(zoom + 3) = (1<<(zoom+3))
	; = sz in bytes of the xs, array of tile columns, which each slot is a ptr on the array of
	; column tiles
	add cl, 3
	mov rsi, 1
	shl rsi, cl
	
	xor edi, edi
	; rsi = sz of xs array in bytes
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae .err_features_xs_mmap_failed
	mov [xs], rax
	; the kernel will zero the new memory pages

	lea rdi, [msg.xs]
	mov rbx, rcx
	mov rsi, 1
	shl rsi, cl ; (1<<zoom)=max n of tile columns, the num of slots of the xs array
	call qword [libc.printf]

	stc ; return true
	mov rsp, rbp
	ret
.err_features_xs_mmap_failed:
	mov rdi, 2 ; stderr
	mov rsi, rax ; errno
	lea rsi, [msg.xs_mmap_failed]
	call qword [libc.dprintf]
	clc ; return false
	mov rsp, rbp
	ret
;}}} init ------------------------------------------------------------------------------------------
select:
; from tile_db_build
define flags	r15 ; our global flags
	; install the zoom lvl vtbl -- start -------------------------------------------------------
	mov rax, [pbf.ctx]
	movzx rax, byte [rax + ctx_t.zoom]
	TRACE_FEATURES 'START:zoom=%lu', rax
	;-------------------------------------------------------------------------------------------
	cmp rax, 12
		jne select.epilog
	TRACE_FEATURES 'installing the virtual table for zoom level 12'
	lea rax, [zoom.lvl12.ways.select]
	mov [select.ways.zoom.fn], rax
	lea rax, [zoom.lvl12.relations.select]
	mov [select.relations.zoom.fn], rax
	lea rax, [zoom.lvl12.nodes.select]
	mov [select.nodes.zoom.fn], rax
	jmp ways ; start with the way-only features
	; install the zoom lvl vtbl -- end ---------------------------------------------------------
; ways -- start ------------------------------------------------------------------------------------
align_nops
ways: 
; from select
;define flags		r15
; local
define ways_finish_p	rbx
define way_start_p	r12
; local used only if a bbox is used
define sw		xmm3
define ne		xmm2
	TRACE_FEATURES 'WAYS:START'
	mov ways_finish_p, [pbf.blk.primblk.primgrp.ways.array.finish]
	mov way_start_p, [pbf.blk.primblk.primgrp.ways.array]
	; load the related bbox stuff
	bt flags, 4 ; use bbox
		jnc .loop_entry
	; set bbox vars
	mov rax, [ctx]
	movupd sw, xword [rax + ctx_t.bbox.sw]
	movupd ne, xword [rax + ctx_t.bbox.ne]
	jmp .loop_entry
	; ways loop -- start -----------------------------------------------------------------------
align_nops
.next:
	mov way_start_p, [way_start_p + blk.primblk.primgrp.ways.array.way_t.next]
.loop_entry:
	cmp way_start_p, ways_finish_p
		je relations ; we are finished with the way features, jmp to relations features
	jmp [select.ways.zoom.fn]
align_nops
.zoom_selection_done: ; return from the zoom vtbl jmp happens here
	jnc .next ; not zoom selected
	bt flags, 4 ; use bbox? ; TODO: use constant
		jnc tile_intersector ; no bbox, jmp directly to the tile intersector
	jmp bbox
;===================================================================================================
include 'zoom/text.s' ; we put the zoom code here, called right from above
;===================================================================================================
	; bbox -- start ----------------------------------------------------------------------------
align_nops
bbox:
; from select
;define flags			r15
; from ways
;define ways_finish_p		rbx
;define way_start_p		r12
;define cache_tile		xmm1
; from ways used only if a bbox is used
;define sw			xmm3
;define ne			xmm2
; local
define way_nodes_p		r13 ; ptr in the array of node ptrs from the current way
define way_nodes_finish_p	r14
define way_node_p		rbp
	mov way_nodes_p, [way_start_p + blk.primblk.primgrp.ways.array.way_t.nodes]
	mov way_nodes_finish_p, [way_start_p + blk.primblk.primgrp.ways.array.way_t.next] ; where the array of node ptrs does finish
	jmp .loop_entry
	; way nodes loop -- start ------------------------------------------------------------------
align_nops
.next_node:
	add way_nodes_p, 8
.loop_entry:
	cmp way_nodes_p, way_nodes_finish_p
		je ways.next ; no node in the bbox, next way
	mov way_node_p, [way_nodes_p] ; load the node ptr
if ~SYSTEM_LEGACY
	test way_nodes_p, 0x3f ; if way_nodes_p is aligned on a cl, prefetch the next 16 ptrs of nodes
		jnz .no_prefetch
	prefetcht0 [way_nodes_p + 2 * 64] ; greed: prefetch 2 cls (= 16 node ptrs) further away
	prefetcht0 [way_nodes_p + 3 * 64] 
.no_prefetch:
end if	
	test way_node_p, way_node_p ; a node ptr can be 0
		jz .next_node
	; bbox lats are always ok (-90.0/90.0)
	; bbox lons are modulo-180.0 (-180.0/180.0)
	; ** avoid f64 modulus arithmetics: use cmps and branches **
	;
	; naive pseudo-code follows for lons:
	;
	; if ne_lon < sw_lon then lon-warp-around = 1
	; if lon-warp-around == 0 ("likely")
	;	if sw_lon <= node_lon && node_lon <= ne_lon then in bbox
	;	else not in bbox
	; else (lon-warp-around  == 1, "unlikely")
	;	if sw_lon <= node_lon (sw_lon <= node_lon <= 180.0)
	;		|| node_lon <= ne_lon then selected (-180.0 <= node_lon <= ne_lon) then
	;			in bbox
	;	else not in bbox
	movupd xmm7, xword [way_node_p + blk.primblk.primgrp.dense.nodes.node_t.geo] ; = node_lat/node_lon
	movapd xmm6, sw ; sw_lat/sw_lon
	cmplepd xmm6, xmm7 ; sw_lat <= node_lat / sw_lon <= node_lon 
	movapd xmm5, xmm7 ; = node_lat/node_lon
	cmplepd xmm5, ne ; node_lat <= ne_lat / node_lon <= ne_lon
	; then the warp-around test (bbox crossing the "day" line)
	movsd xmm4, ne ; = ne_lon
	cmpltsd xmm4, sw ; ne_lon < sw_lon
	movq rax, xmm4 ; (nelon < sw_lon)
	xor ecx, ecx
	test rax, rax
	setnz cl ; rcx[0] = (ne_lon < sw_lon)
	movmskpd rax, xmm6
	movmskpd rdx, xmm5
	shl rax, 1 ; rax[2] = (sw_lat <= node_lat), rax[1] = (sw_lon <= node_lon)
	shl rdx, 3 ; rdx[4] = (node_lat <= ne_lat), rdx[3] = (node_lon <= ne_lon)
	or rcx, rax
	or rcx, rdx
	; content of rcx:
	; [0] = (ne_lon < sw_lon)
	; [1] = (sw_lon <= node_lon)
	; [2] = (sw_lat <= node_lat)
	; [3] = (node_lon <= ne_lon)
	; [4] = (node_lat <= ne_lat)
	mov rax, rcx
	and rax, 10100b
	cmp rax, 10100b ; (sw_lat <= node_lat) && (node_lat <= ne_lat)
		jne .next_node ; node not in lat bbox-->next node
	; from here, node is in lat bbox
	bt rcx, 0 ; (ne_lon < sw_lon)
		jc .lon_warp_around ; unlikely
	and rcx, 1010b ; we can be destructive here
	cmp rcx, 1010b ; (sw_lon <= node_lon) && (node_lon <= ne_lon)
		je tile_intersector ; node in bbox, compute way intersection with tiles
	jmp .next_node ; node not in lon bbox-->next node
align_nops
.lon_warp_around: ; unlikely
	test rcx, 1010b; (sw_lon <= node_lon) || (node_lon <= ne_lon)
		jnz tile_intersector ; node in bbox, compute way intersection with tiles
	jmp .next_node ; node not in lon bbox-->next node
	; way nodes loop -- end --------------------------------------------------------------------
purge way_node_p
purge way_nodes_finish_p
purge way_nodes_p
	; bbox -- end ------------------------------------------------------------------------------
	; tile_intersector -- start ----------------------------------------------------------------
align_nops
tile_intersector:
; from select
;define flags			r15
; from ways
;define ways_finish_p		rbx
;define way_start_p		r12
; from ways used only if a bbox is used
;define sw			xmm3
;define ne			xmm2
; local
define cache_tile		xmm1 ; this contain the last x/y vector of the last tile this way was added
define way_nodes_p		r13 ; ptr in the array of node ptrs from the current way
define way_nodes_finish_p	r14
define way_node_p		rbp
define scaled_zoom_p		rdi ; 1st param of geo_to_wm call
	TRACE_FEATURES 'intersector for way=%p', way_start_p
	mov way_nodes_p, qword [way_start_p + blk.primblk.primgrp.ways.array.way_t.nodes]
	; where the array of node ptrs does finish
	mov way_nodes_finish_p, qword [way_start_p + blk.primblk.primgrp.ways.array.way_t.next]
	; prepare the call to geo_to_wm.f64
	mov rax, [ctx]
	lea scaled_zoom_p, [rax + ctx_t.zoom.scaled.f64]
	; load the cache tile reg with an invalid value (tile_x_finish/tile_y_finish) for this zoom lvl
	lddqu cache_tile, xword [rax + ctx_t.zoom.finish.vec2s64]
	jmp .loop_entry
	; way nodes loop -- start ------------------------------------------------------------------
define geo_p		rsi ; 2nd param of geo_to_wm call
define wm_tile_p	rdx ; 3rd param of geo_to_wm call
align_nops
.next_node:
	add way_nodes_p, 8
.loop_entry:
	cmp way_nodes_p, way_nodes_finish_p
		je ways.next ; intersection done, jmp back to the selecting the way-only features
	mov way_node_p, [way_nodes_p] ; load the node ptr
if ~SYSTEM_LEGACY
	test way_nodes_p, 0x3f ; if way_nodes_p is aligned on a cl, prefetch the next 16 ptrs of nodes
		jnz .no_prefetch
	prefetcht0 [way_nodes_p + 2 * 64] ; greed: prefetch 2 cls (= 16 node ptrs) further away
	prefetcht0 [way_nodes_p + 3 * 64]
.no_prefetch:
end if	
	test way_node_p, way_node_p ; a node ptr can be 0
		jz .next_node
	bt qword [way_node_p + blk.primblk.primgrp.dense.nodes.node_t.flags], \
					blk.primblk.primgrp.dense.nodes.node_t.flags.wm_computed
		jc .cache_hit_test
	lea geo_p, [way_node_p + blk.primblk.primgrp.dense.nodes.node_t.geo]
	lea wm_tile_p, [way_node_p + blk.primblk.primgrp.dense.nodes.node_t.wm.tile]
	call geo_to_wm.f64
	or qword [way_node_p + blk.primblk.primgrp.dense.nodes.node_t.flags], \
				1 shl blk.primblk.primgrp.dense.nodes.node_t.flags.wm_computed
purge geo_p
purge wm_tile_p
.cache_hit_test:
	lddqu xmm7, xword [way_node_p + blk.primblk.primgrp.dense.nodes.node_t.wm.tile.xy]
	pcmpeqb xmm7, cache_tile
	pmovmskb rax, xmm7
	cmp rax, 0xffff
		je .next_node ; cache hit
	; cache miss, update cache reg
	lddqu cache_tile, xword [way_node_p + blk.primblk.primgrp.dense.nodes.node_t.wm.tile.xy]
	; tile lookup -- start ---------------------------------------------------------------------
	; add this way feature to this very tile db
define xs_p	r8
define x	r9
define ys_p	r10
.tile_lookup:
	mov xs_p, [xs]
	mov x, [way_node_p + blk.primblk.primgrp.dense.nodes.node_t.wm.tile.xy.x]
	mov ys_p, [xs_p + 8 * x]
	test ys_p, ys_p
		jz tile_column_new ; once, we'll jmp back to tile_column_done below
purge xs_p
purge x
define tile_p	r9
.tile_column_lookup_done:
	; we expect ys_p only from here
	mov tile_p, [way_node_p + blk.primblk.primgrp.dense.nodes.node_t.wm.tile.xy.y]
	shl tile_p, tile_t.bytes_n_log2 ; = y * tile_bytes_n
	add tile_p, ys_p ; = ys_p + y * tile_bytes_n = tile_p
purge ys_p
	; tile lookup -- end -----------------------------------------------------------------------
	; adding feature way to tile -- start ------------------------------------------------------
; from select
;define flags			r15
; from ways
;define ways_finish_p		rbx
;define way_start_p		r12
; from ways used only if a bbox is used
;define sw			xmm3
;define ne			xmm2
; from tile_intersector
;define cache_tile		xmm1
;define way_nodes_p		r13
;define way_nodes_finish_p	r14
;define way_node_p		rbp
;define scaled_zoom_p		rdi
; from tile_lookup
;define tile_p			r9
	TRACE_FEATURES 'adding feature way %p to tile %p because of node %p', way_start_p, tile_p, way_node_p
	bt qword [tile_p + tile_t.flags], tile_t.flags.ways_inited
		jnc .tile_ways_not_inited ; cold and slow, put far away
	; idx get slot -- start --------------------------------------------------------------------
.tile_ways_inited: 
define idx_slot_p		r8 ; out

define way_pbf_of		r11
define bit_val_select		r10
define bit_val_select_b		r10b
define way_pbf_of_bit_idx_msb	rcx ; using rcx specific instructions
define way_pbf_of_bit_idx_msb_b	cl
define next_idx_slot		rsi
	mov idx_slot_p, qword [tile_p + tile_t.ways.idx]
	mov way_pbf_of, way_start_p
	sub way_pbf_of, qword [blk.primblk.primgrp.ways.array] ; ptr->pbf_of
	mov way_pbf_of_bit_idx_msb, -1 ; if the pbf_of is 0, make it work
	bsr way_pbf_of_bit_idx_msb, way_start_p
	inc way_pbf_of_bit_idx_msb_b ; the n of bits to go thru
	xor bit_val_select, bit_val_select
align_nops
.next_way_pbf_of_bit:
	jecxz .ways_idx_slot_found
	bt way_pbf_of, 0
	setc bit_val_select_b
	mov next_idx_slot, [idx_slot_p + 8 * bit_val_select]
	test next_idx_slot, next_idx_slot ; do we have an idx slot
		jz .idx_insert_slot ; missing slot, switch to slot insertion parsing mode
if ~SYSTEM_LEGACY
	prefetcht0 [next_idx_slot]
end if
	mov idx_slot_p, next_idx_slot
	shr way_pbf_of, 1
	dec way_pbf_of_bit_idx_msb_b
	jmp .next_way_pbf_of_bit
align_nops
.idx_insert_slot:
	; we are in the middle of the idx: we keep going adding new slots until the right one
	mov rax, qword [tile_p + tile_t.ways.idx.next]
	mov [idx_slot_p + 8 * bit_val_select], rax
	add qword [tile_p + tile_t.ways.idx.next], tile_t.ways.idx.slot_t.bytes_n 
	mov idx_slot_p, rax
	mov qword [idx_slot_p + tile_t.ways.idx.slot_t.of], -1 ; = 'empty' slot
	dec way_pbf_of_bit_idx_msb_b
		jz .ways_idx_slot_inserted
	shr way_pbf_of, 1
	bt way_pbf_of, 0
	setc bit_val_select_b
	jmp .idx_insert_slot
align_nops
.ways_idx_slot_found:
.ways_idx_slot_inserted:	
purge way_pbf_of
purge bit_val_select
purge bit_val_select_b
purge way_pbf_of_bit_idx_msb
purge way_pbf_of_bit_idx_msb_b
purge next_idx_slot
	; idx get slot -- end ----------------------------------------------------------------------
define way_of_in_tile_db	rax
	mov way_of_in_tile_db, qword [idx_slot_p + tile_t.ways.idx.slot_t.of]
	cmp way_of_in_tile_db, -1
		jne .tile_add_way_to_feature ; already there, just need to add this way to the feature
.tile_pbf_way_import:
; from select
;define flags			r15
; from ways
;define ways_finish_p		rbx
;define way_start_p		r12
; from ways used only if a bbox is used
;define sw			xmm3
;define ne			xmm2
; from tile intersector 
;define cache_tile		xmm1
;define way_nodes_p		r13
;define way_nodes_finish_p	r14
;define way_node_p		rbp
;define scaled_zoom_p		rdi
; from tile lookup
;define tile_p			r9
; from idx slot get
;define idx_slot_p		r8
;define way_of_in_tile_db	rax
	movq xmm7,	scaled_zoom_p ; spill
	movq xmm6,	tile_p
	movq xmm5,	idx_slot_p
	mov rdi, tile_p ; dst tile
	mov rsi, way_start_p ; src way
	; factorized code, without tile idx handling since we do it above here
	call tile.from_pbf.way_import_no_idx 
		jnc tile_db_build.exit_nok
	movq scaled_zoom_p,	xmm7 ; unspill
	movq tile_p,		xmm6
	movq idx_slot_p,	xmm5
	mov qword [idx_slot_p + tile_t.ways.idx.slot_t.of], way_of_in_tile_db
purge idx_slot_p
.tile_add_way_to_feature:
	movq xmm7,	scaled_zoom_p ; spill
	movq xmm6,	tile_p
	mov rdi, tile_p			; dst tile
	mov rsi, way_of_in_tile_db	; the offset in tile db
	mov rdx, way_node_p		; the node of the tile
	call [select.ways.import.fn]
		jnc tile_db_build.exit_nok
	movq scaled_zoom_p,	xmm7 ; unspill
	movq tile_p,		xmm6
	jmp .next_node
purge way_of_in_tile_db
	; adding feature way to tile -- end --------------------------------------------------------
.tile_ways_not_inited:
; from select
;define flags			r15
; from ways
;define ways_finish_p		rbx
;define way_start_p		r12
; from ways used only if a bbox is used
;define sw			xmm3
;define ne			xmm2
; from tile intersector 
;define cache_tile		xmm1
;define way_nodes_p		r13
;define way_nodes_finish_p	r14
;define way_node_p		rbp
;define scaled_zoom_p		rdi
; from tile lookup
;define tile_p			r9
	movq xmm7, scaled_zoom_p ; spill
	movq xmm6, tile_p ; spill
	mov rdi, tile_p
	mov rsi, way_node_p
	call tile.init.ways ; factorized code
		jnc tile_db_build.exit_nok ; fatal
	movq scaled_zoom_p, xmm7 
	movq tile_p, xmm6
	jmp .tile_ways_inited
purge tile_p
	; way nodes loop -- end --------------------------------------------------------------------
purge cache_tile
purge way_nodes_p
purge way_nodes_finish_p
purge way_node_p
purge scaled_zoom_p
	; tile intersector -- end ------------------------------------------------------------------
	; ways loop -- end -------------------------------------------------------------------------
purge ways_finish_p
purge way_start_p
purge sw
purge ne
; ways -- end --------------------------------------------------------------------------------------
;===================================================================================================
; Since we start with way-only features, then tile column creation is more likely to happen here,
; put it near way-only features main code path.
include 'tile_column_new.s'
;===================================================================================================
; relations -- start -------------------------------------------------------------------------------
relations:
	TRACE_FEATURES 'RELATIONS:START'
	; EMPTY
.zoom_selection_done:
	; EMTPY
; relations -- end ---------------------------------------------------------------------------------
; nodes -- start -----------------------------------------------------------------------------------
nodes:
	TRACE_FEATURES 'NODES:START'
	; EMPTY
.zoom_selection_done:
	; EMPTY
; nodes -- end -------------------------------------------------------------------------------------
select.epilog:
	TRACE_FEATURES 'END'
	jmp pbf.tile_db_build.exit_ok
purge flags
;===================================================================================================
include 'tile.s' ; put the factorized tile code here
include 'motorway.s' ; put some motorway code here, will reuse some code from tile.s
;===================================================================================================
;{{{ macros
if TRACE_FEATURES_ENABLED
	macro TRACE_FEATURES fmt, regs&
		TRACE_PREFIX 'FEATURES', fmt, regs
	end macro
else
	macro TRACE_FEATURES fmt, regs&
	end macro
end if
;}}} macros
end namespace ; features
