; vim: set filetype=fasm foldmethod=marker commentstring=;%s :
namespace features
msg:
	.xs_mmap_failed		db 'ERROR(%ld):PBF:FEATURES:XS:unable to mmap the array of tile columns (x dimension)',10,0
	.xs			db 'PBF:FEATURES:INIT:mmaping an array for the tile columns along the x dimension of %lu slots, the maximum from the zoom level',10,0
	.ys_mmap_failed		db 'ERROR(%ld):PBF:FEATURES:YS:unable to mmap the array of tiles for the tile column %lu (y dimension)',10,0
namespace tile.init
	namespace ways
	msg:
		.err_ways_openat_failed		db 'ERROR(%ld):PBF:FEATURES:TILE:unable to create the ways file %s',10,0 
		.err_ways_idx_mmap_failed	db 'ERROR(%ld):PBF:FEATURES:TILE:unable to mmap the idx of ways',10,0 
		.err_ways_nodes_init_failed	db 'ERROR:PBF:FEATURES:TILE:unable to init the nodes section',10,0 
	end namespace
	namespace nodes
	msg:
		.err_nodes_openat_failed	db 'ERROR(%ld):PBF:FEATURES:unable to create the node file %s',10,0 
		.err_nodes_idx_mmap_failed	db 'ERROR(%ld):PBF:FEATURES:unable to mmap the idx of nodes',10,0 
	end namespace
	namespace dirs_create
	msg:
		.err_x_dir_mkdirat_failed 	db 'ERROR(%ld):PBF:FEATURES:unable to create the x directory %s (tile column)',10,0
		.err_y_dir_mkdirat_failed 	db 'ERROR(%ld):PBF:FEATURES:unable to create the y directory %s (tile)',10,0
	end namespace
end namespace ; tile.init
namespace tile.from_pbf.way_import_no_idx
	msg:
		.err_write_keys_vals_failed		db 'ERROR(%ld):PBF:FEATURES:PBF_IMPORT:WAY:unable to write (key/val)s in tile database',10,0
		.err_write_nodes_of_failed		db 'ERROR(%ld):PBF:FEATURES:PBF_IMPORT:WAY:unable to write the offset of the array of nodes in the tile database',10,0
		.err_write_tile_node_of_failed		db 'ERROR(%ld):PBF:FEATURES:PBF_IMPORT:WAY:unable to write the tile node offset in the tile way',10,0
end namespace
namespace tile.from_pbf.node_import
	msg:
		.err_write_node_failed db 'ERROR(%ld):PBF:FEATURES:PBF_IMPORT:NODE:unable to write the node in tile database',10,0
end namespace
namespace motorway
	msg:
		.err_motorways_openat_failed	db 'ERROR(%ld):PBF:FEATURES:MOTORWAYS:unable to create the motorways file %s',10,0
		.err_write_motorway_of_failed	db 'ERROR(%ld):PBF:FEATURES:MOTORWAYS:unable to write the offset into the array of ways in the tile database',10,0
end namespace
end namespace ; features
