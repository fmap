; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace features
; vtbl -- START ------------------------------------------------------------------------------------
select.ways.zoom.fn		rq 1
select.relations.zoom.fn	rq 1
select.nodes.zoom.fn		rq 1
select.ways.import.fn		rq 1 ; this is specific to the way-feature to import
; vtbl -- END --------------------------------------------------------------------------------------
; xs, the tile columns. Each slot of the array is a pointer on the array of the actual column tiles,
; namely ys. In ys, an array slot in a tile which type is described below.
xs				rq 1
;---------------------------------------------------------------------------------------------------
namespace tile.init
x_str				rb (1 + 7) ; a sz byte then 7 bytes for digits, more than enough
y_str				rb (1 + 7) ;  a sz byte then 7 bytes for digits, more than enough
path				rb 512 ; 512 bytes for path should be more than enough
end namespace
;---------------------------------------------------------------------------------------------------
tile_t: namespace tile_t
	flags			:= 0	; qw
		; core
		flags.dirs_created	:= 0
		flags.nodes_inited	:= 1
		flags.ways_inited	:= 2
		flags.relations_inited	:= 3
		; features
		flags.motorways_inited	:= 4
	; a node in the tile db, will add the geo coords if needed
	;	u := 0	; f64
	;	v := 8	; f64
	;	(key/val)s, terminated with a 0-szed key
	nodes			:= 8
	namespace nodes
		of		:= 8	; qw, track the current of into the following file
		fd		:= 16	; qw, sequential write
		idx		:= 24	; qw, this idx maps the uniq pbf unserialized node of to the node of into the tile array of nodes
		idx.next	:= 32	; qw, next available slot, the sz of the idx depends on the n of nodes from the pbf parsing
			idx.slot_t: namespace idx.slot_t
				bit0	:= 0	; qw, ptr on idx slot for this bit
				bit1	:= 8	; qw, idem
				of	:= 16	; qw, of on the node in the tile array of nodes
				bytes_n	:= 24
			end namespace
	end namespace
	;-------------------------------------------------------------------------------------------
	; a way in the tile db
	; 	nodes_of := 0 ;offset of the array of node offsets
	;	(key/val)s, terminated with a 0-szed key
	;	nodes: (which offset is at the beginning of the way)
	ways			:= 40
	namespace ways
		of		:= 40	; qw, track the current of into the following file
		fd		:= 48	; qw, sequential write
		idx		:= 56	; qw, this idx maps the uniq pbf unserialized way of to the way of into the tile array of ways
		idx.next	:= 64	; qw, next available slot, the sz of the idx depends on the n of ways from the pbf parsing
			idx.slot_t: namespace idx.slot_t
				bit0	:= 0	; qw, ptr on idx slot for this bit
				bit1	:= 8	; qw, idem
				of	:= 16	; qw, of on the way in the tile array of ways
				bytes_n	:= 24
			end namespace
	end namespace
	;-------------------------------------------------------------------------------------------
	; EMPTY
	relations		:= 72
	namespace relations
		of		:= 72	; qw, track the current of into the following file
		fd		:= 80	; qw, sequential write
		idx		:= 88	; qw, this idx maps the uniq pbf unserialized relation of to the relation of into the tile array of relations
		idx.next	:= 96	; qw, next available slot, the sz of the idx depends on the n of relations from the pbf parsing
			idx.slot_t: namespace idx.slot_t
				bit0	:= 0	; qw, ptr on idx slot for this bit
				bit1	:= 8	; qw, idem
				of	:= 16	; qw, of on the relation in the tile array of relations
			end namespace
	end namespace
	;===========================================================================================
	features		:= 104
	namespace features
		motorways	:= 104
		namespace motorways
			fd	:= 104	; qw, array of ofs in the tile array of ways
		end namespace
	end namespace
	; we pad the sz in bytes of a tile to the closest power of 2
	bytes_n_log2		:= 8 ; 2^8 = 256 bytes = 32 * 8
	bytes_n			:= 256
end namespace ; tile_t
end namespace ; features
