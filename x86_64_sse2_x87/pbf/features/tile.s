; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
tile: namespace tile
init: namespace init
; Init of nodes section, ways section, relations section could be factorized even more, but we chose
; not in order to keep it open for ez customization per section.
; Shared definitions:
define tile_p			rbx
define node_p			r12 ; the reference pbf node used for this very tile
define path_finish		r13
define flags			r14
	flags.path_tile_dir_rdy := 0 ; init.path = 'x/y' with its path_finish 
;===================================================================================================
; C-ABI call (notstackalign), input:
; 	rdi = tile_p
;	rsi = node_p
ways: namespace ways
	enter 8 * 4, 0
	and rsp, not 0xf ; align stack for external calls

	mov [rsp + 8 * 0], rbx
	mov [rsp + 8 * 1], r12
	mov [rsp + 8 * 2], r13
	mov [rsp + 8 * 3], r14

	mov tile_p, rdi
	mov node_p, rsi
	xor flags, flags

	call init.xy_to_str
	bt qword [tile_p + tile_t.flags], tile_t.flags.dirs_created
		jc dirs_inited
	call init.dirs_create ; will build path_tile_dir
		jnc epilog
dirs_inited:
	bt flags, flags.path_tile_dir_rdy
		jc path_tile_dir_is_rdy
	call path_tile_dir_do
path_tile_dir_is_rdy:
	mov rax, '/ways' ; terminator zero added
	mov qword [path_finish], rax
	add path_finish, lengthof '/ways' + 1 ; add a zero terminator
	; get a fd ---------------------------------------------------------------------------------
	mov rax, [ctx]
	mov rdi, [rax + ctx_t.tile_db_dir.fd]
	lea rsi, [path]
	mov rdx, linux.O_CREAT or linux.O_TRUNC or linux.O_WRONLY
	mov r10, 110110110b ; RW for all but umask
	mov rax, linux.openat
	syscall
	cmp rax, linux.errno.last
		jae err_ways_openat_failed
	mov [tile_p + tile_t.ways.fd], rax
	mov qword [tile_p + tile_t.ways.of], 0
	; idx init ---------------------------------------------------------------------------------
	mov rax, qword [blk.primblk.primgrp.ways.n] ; worst case, all ways
	mov rcx, tile_t.ways.idx.slot_t.bytes_n
	mul rcx ; = rdx:rax = bytes n for the idx

	xor edi, edi
	mov rsi, rax ; = bytes n for the idx
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae err_ways_idx_mmap_failed
	mov [tile_p + tile_t.ways.idx], rax
	mov qword [rax + tile_t.ways.idx.slot_t.of], -1 ; set up invalid offset in the first slot
	add rax, tile_t.ways.idx.slot_t.bytes_n
	mov [tile_p + tile_t.ways.idx.next], rax
	; we need the inited nodes ----------------------------------------------------------------- 
	bt qword [tile_p + tile_t.flags], 1 shl tile_t.flags.nodes_inited
		jnc nodes_init_do
nodes_inited:
	; ------------------------------------------------------------------------------------------
	or qword [tile_p + tile_t.flags], 1 shl tile_t.flags.ways_inited
	stc
epilog:
	mov rbx, [rsp + 8 * 0]
	mov r12, [rsp + 8 * 1]
	mov r13, [rsp + 8 * 2]
	mov r14, [rsp + 8 * 3]
	leave
	ret
nodes_init_do:
	mov rdi, tile_p
	mov rsi, node_p
	call nodes
		jnc err_ways_nodes_init_failed
	jmp nodes_inited
err_ways_openat_failed: ; errno in rax
	mov rdi, 2
	lea rsi, [msg.err_ways_openat_failed]
	mov rdx, rax
	lea rcx, [init.path]
	call qword [libc.dprintf]
	clc
	jmp epilog
err_ways_idx_mmap_failed: ; errno in rax
	mov rdi, 2
	lea rsi, [msg.err_ways_idx_mmap_failed]
	mov rdx, rax
	call qword [libc.dprintf]
	clc
	jmp epilog
err_ways_nodes_init_failed: ; errno in rax
	mov rdi, 2
	lea rsi, [msg.err_ways_nodes_init_failed]
	call qword [libc.dprintf]
	clc
	jmp epilog
end namespace ; ways_init
;===================================================================================================
; C-ABI call (nostackalign), input:
; 	rdi = tile_p
;	rsi = node_p
nodes: namespace nodes
	enter 8 * 4, 0
	and rsp, not 0xf ; align stack for external calls

	mov [rsp + 8 * 0], rbx
	mov [rsp + 8 * 1], r12
	mov [rsp + 8 * 2], r13
	mov [rsp + 8 * 3], r14

	mov tile_p, rdi
	mov node_p, rsi
	xor flags, flags

	call init.xy_to_str
	bt qword [tile_p + tile_t.flags], tile_t.flags.dirs_created
		jc dirs_inited
	call init.dirs_create ; will build path_tile_dir
		jnc epilog
dirs_inited:
	bt flags, flags.path_tile_dir_rdy
		jc path_tile_dir_is_rdy
	call path_tile_dir_do
path_tile_dir_is_rdy:
	mov rax, '/nodes' ; terminator zero added
	mov qword [path_finish], rax
	add path_finish, lengthof '/nodes' + 1 ; add a zero terminator
	; get a fd ---------------------------------------------------------------------------------
	mov rax, [ctx]
	mov rdi, [rax + ctx_t.tile_db_dir.fd]
	lea rsi, [path]
	mov rdx, linux.O_CREAT or linux.O_TRUNC or linux.O_WRONLY
	mov r10, 110110110b ; RW for all but umask
	mov rax, linux.openat
	syscall
	cmp rax, linux.errno.last
		jae err_nodes_openat_failed
	mov [tile_p + tile_t.nodes.fd], rax
	mov qword [tile_p + tile_t.nodes.of], 0
	; idx init ---------------------------------------------------------------------------------
	mov rax, qword [blk.primblk.primgrp.dense.nodes.n] ; worst case, all nodes
	mov rcx, tile_t.nodes.idx.slot_t.bytes_n
	mul rcx ; = rdx:rax = bytes n for the idx

	xor edi, edi
	mov rsi, rax ; = bytes n for the idx
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae err_nodes_idx_mmap_failed
	mov [tile_p + tile_t.nodes.idx], rax
	mov qword [rax + tile_t.ways.idx.slot_t.of], -1 ; set up invalid offset in the first slot
	add rax, tile_t.nodes.idx.slot_t.bytes_n
	mov [tile_p + tile_t.nodes.idx.next], rax ; the kernel will zero it
	; ------------------------------------------------------------------------------------------
	or qword [tile_p + tile_t.flags], 1 shl tile_t.flags.nodes_inited
	stc
epilog:
	mov rbx, [rsp + 8 * 0]
	mov r12, [rsp + 8 * 1]
	mov r13, [rsp + 8 * 2]
	mov r14, [rsp + 8 * 3]
	leave
	ret
err_nodes_openat_failed: ; errno in rax
	mov rdi, 2
	lea rsi, [msg.err_nodes_openat_failed]
	mov rdx, rax
	lea rcx, [init.path]
	call qword [libc.dprintf]
	clc
	jmp epilog
err_nodes_idx_mmap_failed: ; errno in rax
	mov rdi, 2
	lea rsi, [msg.err_nodes_idx_mmap_failed]
	mov rdx, rax
	call qword [libc.dprintf]
	clc
	jmp epilog
end namespace ; nodes_init
;===================================================================================================
dirs_create: namespace dirs_create
	lea rdi, [init.path]
path_x_add: ; rdi points on the terminating zero char of path
	; x_str is "inverted"
	movzx rcx, byte [init.x_str] ; = x_str_sz
	lea rsi, [init.x_str] ; points on the end of x_str ("past" the last char)
	add rsi, rcx ; rsi points on the first char of y_str
.next_char:
	mov al, [rsi] ; cpy the char
	mov [rdi], al
	inc rdi ; path next char
	dec cl
		jz .insert_terminator
	dec rsi ; "inverted" x next char
	jmp .next_char
.insert_terminator:
	mov byte [rdi], 0
	mov path_finish, rdi ; to add the rest of the path later
	;-------------------------------------------------------------------------------------------
	; stat the x directory (tile columun)
	mov rax, [ctx]
	mov rdi, [rax + ctx_t.tile_db_dir.fd]
	lea rsi, [init.path]
	lea rdx, [stat] ; global storage
	xor r10, r10 ; flags = 0

	mov rax, linux.fstatat
	syscall
	cmp rax, linux.errno.last
		jae x_dir_create
x_dir_done:
	;-------------------------------------------------------------------------------------------
path_y_add:
	mov rdi, path_finish ; points on the x dir terminator
	mov byte [rdi], '/'
	inc rdi
	; y_str is "inverted"
	movzx rcx, byte [init.y_str] ; = y_str_sz
	lea rsi, [init.y_str] ; points on the end of y_str ("past" the last char)
	add rsi, rcx ; rsi points on the first char of y_str
.next_char:
	mov al, [rsi] ; cpy the char
	mov [rdi], al
	inc rdi ; path next char
	dec cl
		jz .insert_terminator
	dec rsi ; "inverted" y next char
	jmp .next_char
.insert_terminator:
	mov byte [rdi], 0
	mov path_finish, rdi 
	or flags, 1 shl flags.path_tile_dir_rdy
	;-------------------------------------------------------------------------------------------
	; stat the y directory (tile)
	mov rax, [ctx]
	mov rdi, [rax + ctx_t.tile_db_dir.fd]
	lea rsi, [path]
	lea rdx, [stat]
	xor r10, r10 ; flags = 0

	mov rax, linux.fstatat
	syscall
	cmp rax, linux.errno.last
		jae y_dir_create
y_dir_done:
	;-------------------------------------------------------------------------------------------
	or qword [tile_p + tile_t.flags], 1 shl tile_t.flags.dirs_created
	stc
	ret
;===================================================================================================
x_dir_create:
	mov rax, [ctx]
	mov rdi, [rax + ctx_t.tile_db_dir.fd]
	lea rsi, [path]
	mov rdx, 111111111b ; RWX for all

	mov rax, linux.mkdirat
	syscall
	cmp rax, linux.errno.last
		jae err_x_dir_mkdirat_failed
	jmp x_dir_done
	;-------------------------------------------------------------------------------------------
y_dir_create:
	mov rax, [ctx]
	mov rdi, [rax + ctx_t.tile_db_dir.fd]
	lea rsi, [path]
	mov rdx, 111111111b ; RWX for all

	mov rax, linux.mkdirat
	syscall
	cmp rax, linux.errno.last
		jae err_y_dir_mkdirat_failed
	jmp y_dir_done
	;-------------------------------------------------------------------------------------------
err_y_dir_mkdirat_failed: ; errno in rax
	lea rsi, [msg.err_y_dir_mkdirat_failed]
	jmp err_exit
err_x_dir_mkdirat_failed: ; errno in rax
	lea rsi, [msg.err_x_dir_mkdirat_failed]
	jmp err_exit
err_exit: ; errno in rax
	mov rdi, 2
	mov rdx, rax
	lea rcx, [path]
	call qword [libc.dprintf]
	clc
	ret
end namespace ; dirs_create
;===================================================================================================
; expect x and y to be converted already
; REUSED FROM EXTERNAL CODE (i.e. MOTORWAYS)
path_tile_dir_do:
	lea rdi, [init.path]
.path_x_add: ; rdi points on the terminating zero char of path
	; x_str is "inverted"
	movzx rcx, byte [init.x_str] ; = x_str_sz
	lea rsi, [init.x_str] ; points on the end of x_str ("past" the last char)
	add rsi, rcx ; rsi points on the first char of y_str
.x_next_char:
	mov al, [rsi] ; cpy the char
	mov [rdi], al
	inc rdi ; path next char
	dec cl
		jz .insert_separator
	dec rsi ; "inverted" x next char
	jmp .x_next_char
.insert_separator:
	mov byte [rdi], '/'
	inc rdi
	; y_str is "inverted"
	movzx rcx, byte [init.y_str] ; = y_str_sz
	lea rsi, [init.y_str] ; points on the end of y_str ("past" the last char)
	add rsi, rcx ; rsi points on the first char of y_str
.y_next_char:
	mov al, [rsi] ; cpy the char
	mov [rdi], al
	inc rdi ; path next char
	dec cl
		jz .save_finish
	dec rsi ; "inverted" y next char
	jmp .y_next_char
.save_finish:
	mov byte [rdi], 0 ; zero terminate the path
	mov path_finish, rdi
	ret
;===================================================================================================
; REUSED FROM EXTERNAL CODE (i.e. MOTORWAYS)
xy_to_str: namespace xy_to_str
define str_p			rdi ; tmp
x_conv:
	xor edx, edx
	mov rax, qword [node_p + blk.primblk.primgrp.dense.nodes.node_t.wm.tile.xy.x]
	xor ecx, ecx
	mov r11, 10
	lea str_p, [init.x_str + 1]
.digit:
	div r11
	add dl, '0' ; to ascii
	mov byte [str_p], dl
	inc cl
	test rax, rax
		jz .finished
	inc str_p
	xor edx, edx
	jmp .digit
.finished:
	mov [init.x_str], cl
y_conv:
	xor edx, edx
	mov rax, qword [node_p + blk.primblk.primgrp.dense.nodes.node_t.wm.tile.xy.y]
	xor ecx, ecx
	; mov r11, 10
	lea str_p, [init.y_str + 1]
.digit:
	div r11
	add dl, '0' ; to ascii
	mov byte [str_p], dl
	inc cl
	test rax, rax
		jz .finished
	inc str_p
	xor edx, edx
	jmp .digit
.finished:
	mov [init.y_str], cl
	ret
purge str_p
end namespace ; xy_to_str
;===================================================================================================
purge tile_p
purge node_p
purge path_finish
purge flags
end namespace ; init
;===================================================================================================
from_pbf: namespace from_pbf
align_nops
node_import: namespace node_import
; in rdi = tile_p
; in rsi = pbf_node_p
; out rax = node offset in tile array
; expect the tile to be properly inited, but the node may require wm computation
define tile_p			rbx
define tile_node_bytes_n	r12
define idx_slot_p		r13
define pbf_node_p		r14
define rbx_save		rsp + 8 * 0
define r12_save		rsp + 8 * 1
define r13_save		rsp + 8 * 2
define r14_save		rsp + 8 * 3
	enter 8 * 4, 0
	and rsp, not 0xf ; align the stack for the external calls
	mov [rbx_save], rbx
	mov [r12_save], r12
	mov [r13_save], r13
	mov [r14_save], r14

	mov tile_p, rdi
	mov pbf_node_p, rsi

	; idx get slot -- start --------------------------------------------------------------------
define pbf_node_of			r11
define bit_val_select			r9
define bit_val_select_b			r9b
define pbf_node_of_bit_idx_msb		rcx ; using rcx specific instructions
define pbf_node_of_bit_idx_msb_b	cl
define next_idx_slot			r8
	mov idx_slot_p, qword [tile_p + tile_t.nodes.idx]
	mov pbf_node_of, pbf_node_p
	sub pbf_node_of, qword [blk.primblk.primgrp.dense.nodes] ; ptr->pbf offset
	mov pbf_node_of_bit_idx_msb, -1 ; if the pbf offset is 0, make it work
	bsr pbf_node_of_bit_idx_msb, pbf_node_p
	inc pbf_node_of_bit_idx_msb_b ; the n of bits to go thru
	xor bit_val_select, bit_val_select
align_nops
.next_pbf_node_of_bit:
	jecxz .idx_slot_found
	bt pbf_node_of, 0
	setc bit_val_select_b
	mov next_idx_slot, [idx_slot_p + 8 * bit_val_select]
	test next_idx_slot, next_idx_slot ; do we have an idx slot
		jz .idx_insert_slot ; missing slot, switch to slot insertion parsing mode
if ~SYSTEM_LEGACY
	prefetcht0 [next_idx_slot]
end if
	mov idx_slot_p, next_idx_slot
	shr pbf_node_of, 1
	dec pbf_node_of_bit_idx_msb_b
	jmp .next_pbf_node_of_bit
align_nops
.idx_insert_slot:
	; we are in the middle of the idx: we keep going adding new slots until the right one
	mov rax, qword [tile_p + tile_t.nodes.idx.next]
	mov [idx_slot_p + 8 * bit_val_select], rax
	add qword [tile_p + tile_t.nodes.idx.next], tile_t.nodes.idx.slot_t.bytes_n
	mov idx_slot_p, rax
	mov qword [idx_slot_p + tile_t.nodes.idx.slot_t.of], -1 ; = 'empty' slot
	dec pbf_node_of_bit_idx_msb_b
		jz .idx_slot_inserted
	shr pbf_node_of, 1
	bt pbf_node_of, 0
	setc bit_val_select_b
	jmp .idx_insert_slot
align_nops
.idx_slot_found:
.idx_slot_inserted:	
purge pbf_node_of_node
purge bit_val_select
purge bit_val_select_b
purge pbf_node_of_bit_idx_msb
purge pbf_node_of_bit_idx_msb_b
purge next_idx_slot
	; idx get slot -- end ----------------------------------------------------------------------
	mov rax, qword [idx_slot_p + tile_t.nodes.idx.slot_t.of]
	cmp rax, -1
		jne exit_ok ; we have already the offset in rax to return to the caller
add_node_to_tile: 
	; compute the tile stuff if needed before storage -- start ---------------------------------
	bt qword [pbf_node_p + blk.primblk.primgrp.dense.nodes.node_t.flags], \
					blk.primblk.primgrp.dense.nodes.node_t.flags.wm_computed
		jc .pbf_node_wm_computed
	mov rax, [ctx]
	lea rdi, [rax + ctx_t.zoom.scaled.f64]					; 1st arg
	lea rsi, [pbf_node_p + blk.primblk.primgrp.dense.nodes.node_t.geo]	; 2nd arg
	lea rdx, [pbf_node_p + blk.primblk.primgrp.dense.nodes.node_t.wm.tile]	; 3rd arg
	call geo_to_wm.f64
	or qword [pbf_node_p + blk.primblk.primgrp.dense.nodes.node_t.flags], \
				1 shl blk.primblk.primgrp.dense.nodes.node_t.flags.wm_computed
.pbf_node_wm_computed:
	; compute the tile stuff if needed before storage -- end -----------------------------------
	; write into the tile db nodes file -- start -----------------------------------------------
	mov rdi, qword [tile_p + tile_t.nodes.fd]
	lea rsi, [pbf_node_p + blk.primblk.primgrp.dense.nodes.node_t.wm.tile.uv] ; 2nd arg of the write syscall
	mov rdx, qword [pbf_node_p + blk.primblk.primgrp.dense.nodes.node_t.next] ; ptr on next node = ptr on the end of current node
purge pbf_node_p
	sub rdx, rsi ; = tile_node_bytes_n and 3rd arg of the write syscall
	mov tile_node_bytes_n, rdx
	; write the pbf node in the tile node
	mov rax, linux.write
	syscall
	cmp rax, linux.errno.last
		jae err_write_node_failed
	; write into the tile db nodes file -- end -------------------------------------------------
	mov rax, qword [tile_p + tile_t.nodes.of] ; return the tile_node offset to the caller
	mov [idx_slot_p + tile_t.nodes.idx.slot_t.of], rax ; insert the now known offset into the idx
	add [tile_p + tile_t.nodes.of], tile_node_bytes_n ; update the offset tracker
exit_ok:
	stc
epilog:
	mov rbx, qword [rbx_save]
	mov r12, qword [r12_save]
	mov r13, qword [r13_save]
	mov r14, qword [r14_save]
	leave
	ret
err_write_node_failed: ; errno in rax
	mov rdi, 2 ; stderr
	lea rsi, [msg.err_write_node_failed]
	mov rdx, rax ; errno
	call qword [libc.dprintf]
	clc
	jmp epilog
purge tile_p
purge tile_node_bytes_n
purge idx_slot_p
purge rbx_save
purge r12_save
purge r13_save
purge r14_save
end namespace ; node_import
;===================================================================================================
align_nops
way_import_no_idx: namespace way_import_no_idx
; rdi = tile_p
; rsi = pbf_way_p
; expect the tile properly inited
; TODO: one day, move as many stack variables as possible to rbx,
;       r12,r13,r14,15 (yes, sacrifice the r15 flags)
define rbx_save			rsp + 8 * 0
define r12_save			rsp + 8 * 1
define tile_p			rsp + 8 * 2
define pbf_way_p		rsp + 8 * 3
define tile_way_bytes_n		rsp + 8 * 4
define way_nodes_finish_p	rsp + 8 * 5
define tile_node_of		rsp + 8 * 6
	enter 8 * 7, 0
	and rsp, not 0xf  ; align the stack for the external calls
	mov [rbx_save], rbx
	mov [r12_save], r12
	mov [tile_p], rdi
	mov [pbf_way_p], rsi
	
define pbf_way_keys_vals_p	rbx
define keys_vals_bytes_n	r12
	; compute the (key/val)s bytes n = offset of tile way nodes
	mov rcx, qword [rsi + blk.primblk.primgrp.ways.array.way_t.nodes] ; the nodes come after the (key/val)s in the pbf way
	lea pbf_way_keys_vals_p, qword [rsi + blk.primblk.primgrp.ways.array.way_t.keys_vals]
	sub rcx, pbf_way_keys_vals_p ; = (key/val)s bytes n
	; we don't test here we actually have (key/val)s to write since way-only features have some 
	mov keys_vals_bytes_n, rcx
	add rcx, 8 ; add the field containing the offset of the array of nodes of the tile way, which will come afte the (key\val)s
	mov [tile_way_bytes_n], rcx

	; write the offset of the array of the nodes in the tile way, right after the (key/val)s
	mov rdi, [rdi + tile_t.ways.fd]
	lea rsi, [tile_way_bytes_n]
	mov rdx, 8
	mov rax, linux.write
	syscall
	cmp rax, linux.errno.last
		jae err_write_nodes_of_failed

	; write the pbf way (key/val)s in the tile way (key/val)s
	mov rdi, qword [tile_p]
	mov rdi, qword [rdi + tile_t.ways.fd]
	mov rsi, pbf_way_keys_vals_p
	mov rdx, keys_vals_bytes_n
	mov rax, linux.write
	syscall
	cmp rax, linux.errno.last
		jae err_write_keys_vals_failed
purge pbf_way_keys_vals_p
purge keys_vals_bytes_n
	; way nodes loop -- start ------------------------------------------------------------------
define way_nodes_p		rbx
define way_node_p		r12
	mov rax, qword [pbf_way_p]
	mov way_nodes_p, qword [rax + blk.primblk.primgrp.ways.array.way_t.nodes]
	mov rax, qword [rax + blk.primblk.primgrp.ways.array.way_t.next]
	mov [way_nodes_finish_p], rax
	jmp loop_entry
align_nops
next_node:
	add way_nodes_p, 8
loop_entry:
	cmp way_nodes_p, qword [way_nodes_finish_p]
		je nodes_imported
	mov way_node_p, [way_nodes_p] ; load the node ptr
	test way_node_p, way_node_p ; a node ptr can be 0
		jz next_node
	mov rdi, qword [tile_p]
	mov rsi, way_node_p
	call node_import
		jnc epilog
	mov [tile_node_of], rax
	; write the tile node offset in the tile array into the tile way array of tile nodes 
	mov rdi, qword [tile_p]
	mov rdi, qword [rdi + tile_t.ways.fd]
	lea rsi, [tile_node_of]
	mov rdx, 8
	mov rax, linux.write
	syscall
	cmp rax, linux.errno.last
		jae err_write_tile_node_of_failed
	add qword [tile_way_bytes_n], 8 ; 1 tile node offset is 8 bytes more to the tile way
	jmp next_node	
purge way_nodes_p
purge way_node_p
	; way nodes loop -- end --------------------------------------------------------------------
align_nops
nodes_imported:
	mov rdi, [tile_p]
	mov rcx, [tile_way_bytes_n]
	mov rax, [rdi + tile_t.ways.of] ; return the offset of this tile_way to the caller
	add [rdi + tile_t.ways.of], rcx ; now, update the offset tracker
	stc
epilog:
	mov rbx, qword [rbx_save]
	mov r12, qword [r12_save]
	leave
	ret
err_write_keys_vals_failed: ; errno in rax
	mov rdi, 2 ; stderr
	lea rsi, [msg.err_write_keys_vals_failed]
	mov rdx, rax ; errno
	call qword [libc.dprintf]
	clc
	jmp epilog
err_write_nodes_of_failed: ; errno in rax
	mov rdi, 2 ; stderr
	lea rsi, [msg.err_write_nodes_of_failed]
	mov rdx, rax ; errno
	call qword [libc.dprintf]
	clc
	jmp epilog
err_write_tile_node_of_failed: ; errno in rax
	mov rdi, 2 ; stderr
	lea rsi, [msg.err_write_tile_node_of_failed]
	mov rdx, rax ; errno
	call qword [libc.dprintf]
	clc
	jmp epilog
purge rbx_save
purge r12_save
purge tile_p
purge pbf_way_p
purge tile_way_bytes_n
purge way_nodes_finish_p
purge tile_node_of
end namespace ; way_import_no_idx
end namespace ; from_pbf
;===================================================================================================
end namespace ; tile
