; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
motorway: namespace motorway
; input:
;	rdi = tile_p
;	rsi = offset of the way in the tile db
;	rdx = node_p of the one provoking this very tile addition, only used if initing required
align_nops
import:
define tile_p			rbx
define rbx_save			rsp + 8 * 0
define r12_save			rsp + 8 * 1
define r13_save			rsp + 8 * 2
define motorway_of		rsp + 8 * 3
	enter 8 * 4, 0
	and rsp, not 0xf ; align stack for external calls
	mov [rbx_save], rbx

	mov tile_p, rdi
	mov [motorway_of], rsi

	bt qword [tile_p + tile_t.flags], tile_t.flags.motorways_inited
		jnc motorways_init
	mov rdi, qword [tile_p + tile_t.features.motorways.fd]
motorways_inited:
	; write the offset into the array of the ways in the tile motorways ------------------------
	lea rsi, [motorway_of]
	mov rdx, 8
	mov rax, linux.write
	syscall
	cmp rax, linux.errno.last
		jae err_write_motorway_of_failed
	stc
epilog:
	mov rbx, qword [rbx_save]
	leave
	ret
	;-------------------------------------------------------------------------------------------
	; we presume created directories
define node_p		r12 ; r12 due te re-using tile init code
define path_finish	r13 ; r13 due to re-using tile init code
motorways_init:
	; build the path re-using tile init code ---------------------------------------------------
	mov [r12_save], r12
	mov [r13_save], r13
	mov node_p, rdx
	call tile.init.xy_to_str ; we share r12 and r13
	call tile.init.path_tile_dir_do ; we share r12 and r13
	mov r12, qword [r12_save]
purge node_p
	mov rax, '/motorwa'
	mov qword [path_finish], rax
	mov eax, 'ys'
	mov dword [path_finish + 8], eax ; 0 terminator added
	mov r13, qword [r13_save]
purge path_finish
	; get a fd ---------------------------------------------------------------------------------
	mov rax, [ctx]
	mov rdi, [rax + ctx_t.tile_db_dir.fd]
	lea rsi, [tile.init.path]
	mov rdx, linux.O_CREAT or linux.O_TRUNC or linux.O_WRONLY
	mov r10, 110110110b ; RW for all but umask
	mov rax, linux.openat
	syscall
	cmp rax, linux.errno.last
		jae err_motorways_openat_failed
	mov [tile_p + tile_t.features.motorways.fd], rax
	mov rdi, rax ; caller expect the fd being in rdi as well

	or qword [tile_p + tile_t.flags], 1 shl tile_t.flags.motorways_inited
	jmp motorways_inited
	;-------------------------------------------------------------------------------------------
err_motorways_openat_failed:
	mov rdi, 2
	lea rsi, [msg.err_motorways_openat_failed]
	mov rdx, rax
	lea rcx, [tile.init.path]
	call qword [libc.dprintf]
	clc
	jmp epilog
err_write_motorway_of_failed:
	mov rdi, 2
	lea rsi, [msg.err_write_motorway_of_failed]
	mov rdx, rax
	call qword [libc.dprintf]
	clc
	jmp epilog
purge tile_p
purge rbx_save
purge r12_save
purge r13_save
purge motorway_of
end namespace ; motorways
