; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
align_nops
tile_column_new: namespace tile_column_new
; from select
define flags			r15
; from ways
define ways_finish_p		rbx
define way_start_p		r12
; from ways used only if a bbox is used
define sw			xmm3
define ne			xmm2
; from tile_intersector
define cache_tile		xmm1
define way_nodes_p		r13
define way_nodes_finish_p	r14
define way_node_p		rbp
define scaled_zoom_p		rdi
; from tile lookup
define xs_p			r8 
define x			r9
define ys_p			r10 ; out
	; save and align the stack ptr
	movq xmm7, rsp
	and rsp, not 0xf
	; save callee clobbered regs
	movq xmm6, scaled_zoom_p
	movq xmm5, way_node_p
	movq xmm4, xs_p
	; xmm3 and xmm2 could be booked if we have a bbox
	movq xmm0, x
	; compute the array sz in bytes
	mov rax, [ctx]
	movzx rcx, byte [rax + ctx_t.zoom]
	add cl, tile_t.bytes_n_log2
	mov rsi, 1
	; 1 << (zoom+tile_t.bytes_n_log2) = (1 << zoom) * 2^tile_t.bytes_n_log2 = column_tiles_n * tile_bytes_n
	shl rsi, cl ; = tile_column_bytes_n

	xor edi, edi
	; rsi = sz of xy array in bytes
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae err_features_ys_mmap_failed ; no return
	; The kernel will zero the new mem pages. Restore callee clobbered regs.
	movq x, xmm0
	movq xs_p, xmm4
	movq way_node_p, xmm5
	movq scaled_zoom_p, xmm6
	movq rsp, xmm7
	; stitch regs for the jmp back and save the ptr of the array of column tiles
	mov [xs_p + 8 * x], rax
	mov ys_p, rax
	jmp tile_intersector.tile_column_lookup_done ; this is where we jmp back to tile_lookup

err_features_ys_mmap_failed: ; ouch, don't bother
	mov rdi, 2 ; stderr
	mov rsi, rax ; errno
	lea rsi, [msg.ys_mmap_failed]
	movq rdx, xmm0 ; = x
	call qword [libc.dprintf]
	movq rsp, xmm7 ; restore only rsp
	jmp tile_db_build.exit_nok ; no return
purge flags
purge ways_finish_p
purge way_start_p
purge sw
purge ne
purge cache_tile
purge way_nodes_p
purge way_nodes_finish_p
purge way_node_p
purge scaled_zoom_p
purge xs_p
purge x
purge ys_p
end namespace
