; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
relations: namespace relations
select:
	clc ; not selected
	jmp features.relations.zoom_selection_done
end namespace ; zoom.lvl12.relations
