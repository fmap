; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
ways: namespace ways
; from select
define flags		r15
; from features way
define ways_finish_p	rbx
define way_start_p	r12
; local
define keys_vals_p	r13
define mask7b		r14
align_nops
select:
	lea keys_vals_p, [way_start_p + blk.primblk.primgrp.ways.array.way_t.keys_vals]
	mov mask7b, 0x00ffffffffffffff ; 7 bytes str
align_nops
.next_key_val:
	movzx rcx, byte [keys_vals_p]
	test cl, cl
		jz .not_selected ; 0-szed key terminator
	;{{{ key="highway" -------------------------------------------------------------------------
	cmp cl, lengthof 'highway'
		jne .skip_from_key_sz_byte
	inc keys_vals_p ; reach the key first char
	mov rax, [keys_vals_p]
	and rax, mask7b
	mov rdx, 'highway'
	cmp rax, rdx
		jne .skip_from_key_first_char
	add keys_vals_p, rcx ; reach the val sz word
	;{{{ "highway" vals ------------------------------------------------------------------------
	movzx rcx, word [keys_vals_p]
	test cx, cx
		jz .skip_empty_val; 0-szed val
	;{{{ val="motorway"
	cmp cx, lengthof 'motorway'
		jne .skip_from_val_sz_word
	add keys_vals_p, 2 ; reach the val first char
	mov rax, 'motorway'
	cmp [keys_vals_p], rax
		jne .skip_from_val_first_char
	; (highway,motorway)
	lea rax, [motorway.import]; install the way-feature import func for a motorway
	mov [features.select.ways.import.fn], rax
	jmp .selected
	;}}} val="motorway"
	;}}} "highway" vals ------------------------------------------------------------------------
	;}}} key="highway" -------------------------------------------------------------------------
align_nops
.skip_from_key_sz_byte:
	inc rcx
.skip_from_key_first_char:
	add keys_vals_p, rcx ; skip key
	movzx rcx, word [keys_vals_p]
.skip_from_val_sz_word:
	add rcx, 2
.skip_from_val_first_char:
	add keys_vals_p, rcx ; skip val
	jmp .next_key_val
.skip_empty_val:
	add keys_vals_p, 2 ; skip sz word
	jmp .next_key_val
align_nops
.not_selected:
	clc
	jmp features.ways.zoom_selection_done
.selected:
	stc
	jmp features.ways.zoom_selection_done
purge flags
purge ways_finish_p
purge way_start_p
purge keys_vals_p
purge mask7b
;===================================================================================================
align_nops
motorway_import:
end namespace ; zoom.lvl12.ways
