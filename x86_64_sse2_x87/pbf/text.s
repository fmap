; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
pbf: namespace pbf
;---------------------------------------------------------------------------------------------------
init_once:
	call blob.init_once
		jnc .nok
	call blk.init_once
		jnc .nok
	stc
	ret
.nok:
	clc
	ret
;---------------------------------------------------------------------------------------------------
define file_path	rbx
define file_fd		r12
file_mmap:
	mov rbp, rsp ; align the stack now for the external calls
	and rsp, not 0xf

	mov rax, [ctx]
	mov file_path, [rax + ctx_t.file_path]

	lea rdi, [msg.opening]
	mov rsi, file_path
	call qword [libc.printf]

	mov rax, linux.open
	mov rdi, file_path
	mov rsi, 2000000o	; flags O_RDONLY | O_CLOEXEC
	xor edx, edx		; mode
	syscall
	cmp rax, linux.errno.last
		jae .err_open_failed
	mov [fd], rax
	mov file_fd, rax

	mov rax, linux.fstat
	mov rdi, file_fd
	lea rsi, [stat]
	syscall
	cmp rax, linux.errno.last
		jae .err_stat_failed

	lea rdi, [msg.stat_sz]
	mov rsi, file_path
	mov rdx, qword [stat + linux.stat_t.sz]
	call qword [libc.printf]

	mov rax, linux.mmap
	xor edi, edi
	mov rsi, qword [stat + linux.stat_t.sz]
	mov rdx, 1 		; PROT_READ 
	mov r10, 2		; MAP_PRIVATE
	mov r8, [fd]
	xor r9d, r9d
	syscall
	cmp rax, linux.errno.last
		jae .err_mmap_failed
	mov [start], rax

	lea rdi, [msg.mmap_addr]
	mov rsi, [start]
	call qword [libc.printf]

	stc
	mov rsp, rbp
	ret
.err_mmap_failed: ; errno in rax
	lea rdi, [msg.mmap_failed]
	mov rsi, file_path
	mov rdx, rax
	call qword [libc.printf]
	clc
	mov rsp, rbp
	ret
.err_stat_failed: ; errno in rax
	lea rdi, [msg.stat_failed]
	mov rsi, file_path
	mov rdx, rax
	call qword [libc.printf]
	clc
	mov rsp, rbp
	ret
.err_open_failed: ; errno in rax
	lea rdi, [msg.open_failed]
	mov rsi, file_path
	mov rdx, rax
	call qword [libc.printf]
	clc
	mov rsp, rbp
	ret
purge file_path
purge file_fd
;---------------------------------------------------------------------------------------------------
define ctx_p rbx
ctx_print:
	mov rbp, rsp ; align the stack now for external calls
	and rsp, not 0xf

	mov ctx_p, [ctx]
	lea rdi, [msg.ctx.start]
	call qword [libc.printf]

	lea rdi, [msg.ctx.general]
	mov rsi, [ctx_p + ctx_t.file_path]
	mov rdx, [ctx_p + ctx_t.tile_db_dir.path]
	movzx rcx, byte [ctx_p + ctx_t.zoom]
	call qword [libc.printf]

	mov rax, [ctx_p + ctx_t.flags]
	; only print the bbox if we have at least one user provided geo coord
	and qword [ctx_p + ctx_t.flags], 0xf
		jz .finish
	;-------------------------------------------------------------------------------------------
.swlat:
	lea rdi, [msg.ctx.bbox.swlat]
	movsd xmm0, qword [ctx_p + ctx_t.bbox.sw.lat]
	mov rax, 1
	bt qword [ctx_p + ctx_t.flags], 0
		jc .swlat_user_provided
	lea rsi, [msg.ctx.bbox.adjust]
	jmp .swlat_print
.swlat_user_provided:
	lea rsi, [msg.ctx.bbox.user]
.swlat_print:
	call qword [libc.printf]
	;-------------------------------------------------------------------------------------------
.swlon:
	lea rdi, [msg.ctx.bbox.swlon]
	movsd xmm0, qword [ctx_p + ctx_t.bbox.sw.lon]
	mov rax, 1
	bt qword [ctx_p + ctx_t.flags], 1
		jc .swlon_user_provided
	lea rsi, [msg.ctx.bbox.adjust]
	jmp .swlon_print
.swlon_user_provided:
	lea rsi, [msg.ctx.bbox.user]
.swlon_print:
	call qword [libc.printf]
	;-------------------------------------------------------------------------------------------
.nelat:
	lea rdi, [msg.ctx.bbox.nelat]
	movsd xmm0, qword [ctx_p + ctx_t.bbox.ne.lat]
	mov rax, 1
	bt qword [ctx_p + ctx_t.flags], 2
		jc .nelat_user_provided
	lea rsi, [msg.ctx.bbox.adjust]
	jmp .nelat_print
.nelat_user_provided:
	lea rsi, [msg.ctx.bbox.user]
.nelat_print:
	call qword [libc.printf]
	;-------------------------------------------------------------------------------------------
.nelon:
	lea rdi, [msg.ctx.bbox.nelon]
	movsd xmm0, [ctx_p + ctx_t.bbox.ne.lon]
	mov rax, 1
	bt qword [ctx_p + ctx_t.flags], 3
		jc .nelon_user_provided
	lea rsi, [msg.ctx.bbox.adjust]
	jmp .nelon_print
.nelon_user_provided:
	lea rsi, [msg.ctx.bbox.user]
.nelon_print:
	call qword [libc.printf]
	;-------------------------------------------------------------------------------------------
.finish:
	lea rdi, [msg.ctx.finish]
	call qword [libc.printf]
	; we flush stdout
	mov rax, [libc.stdout]
	mov rdi, [rax]
	call [libc.fflush] 
	mov rsp, rbp
	ret
purge ctx_p
;---------------------------------------------------------------------------------------------------
define ctx_p			rbx
ctx_bbox_adjust:
	mov ctx_p, [ctx]
.sw_lat:
	bt qword [ctx_p + ctx_t.flags], 0
		jc .sw_lon
	mov rax, -90.0 * 1000000000; f64
	mov [ctx_p + ctx_t.bbox.sw.lat], rax
.sw_lon:
	bt qword [ctx_p + ctx_t.flags], 1
		jc .ne_lat
	mov rax, -180.0 * 1000000000
	mov [ctx_p + ctx_t.bbox.sw.lon], rax
.ne_lat:
	bt qword [ctx_p + ctx_t.flags], 2
		jc .ne_lon
	mov rax, 90.0 * 1000000000
	mov [ctx_p + ctx_t.bbox.ne.lat], rax
.ne_lon:
	bt qword [ctx_p + ctx_t.flags], 3
		jc .epilog
	mov rax, 180.0 * 1000000000
	mov [ctx_p + ctx_t.bbox.ne.lon], rax
.epilog:
	ret
purge ctx_p
;---------------------------------------------------------------------------------------------------
; scratch: rax, rdx, rcx
define ctx_p	rbx
wm_scaled_zoom_compute:
	mov ctx_p, [ctx]
	movzx rcx, byte [ctx_p + ctx_t.zoom] ; zoom is <= 255
	mov rax, 1
	shl rax, cl ; 1<<zoom, could use the x86 scale inst
	mov qword [ctx_p + ctx_t.zoom.scaled.f64], rax ; prepare f64 conv
	mov qword [ctx_p + ctx_t.zoom.scaled.s64], rax ; store for later usage
	mov qword [ctx_p + ctx_t.zoom.finish.vec2s64.x], rax
	mov qword [ctx_p + ctx_t.zoom.finish.vec2s64.y], rax
	; do the conv with the x87 (should use)
	fild qword [ctx_p + ctx_t.zoom.scaled.f64]
	fstp qword [ctx_p + ctx_t.zoom.scaled.f64] ; store for later usage
	ret
purge ctx_p
;---------------------------------------------------------------------------------------------------
define ctx_p	rbx
tile_db_dir_open:
	mov ctx_p, [ctx]

	mov rbp, rsp ; align the stack now for the external calls
	and rsp, not 0xf

	mov rax, linux.openat
	mov rdi, linux.AT_FDCWD
	mov rsi, [ctx_p + ctx_t.tile_db_dir.path]
	mov rdx, linux.O_RDWR or linux.O_DIRECTORY
	xor r10, r10
	syscall
	cmp rax, linux.errno.last
		jae .err_openat_failed
	mov [ctx_p + ctx_t.tile_db_dir.fd], rax
	stc
	mov rsp, rbp
	ret
.err_openat_failed: ; errno in rax
	mov rdi, 2
	lea rsi, [msg.tile_db_dir_openat_failed]
	mov rdx, [ctx_p + ctx_t.tile_db_dir.path]
	mov rcx, rax
	call qword [libc.dprintf]
	clc
	mov rsp, rbp
	ret
purge ctx_p
;---------------------------------------------------------------------------------------------------
; the parsing conversion entry point
; INTERNAL FLAGS -- START (NOT CTX FLAGS)
; TODO: switch to definitions of constants
; [0] - found the blob_hdr_type field
; [1] - blob_hdr_type is known, valid if [0] is 1
; [2] - blob_hdr_type = 0('OSMHeader')|1('OSMData'), valid if [1] is 1
; [3] - found the blob_hdr_datasize field
; [4] - use bbox
; INTERNAL FLAGS -- END (NOT CTX FLAGS)
; in: rdi ctx
define finish		rbx
define blob_hdr_p	r12
define flags		r15
; local
define ctx_p		rax
tile_db_build: ; very probably public/visible one day
	enter 8 * 5, 0
	mov [rbp - 8 * 1], rbx
	mov [rbp - 8 * 2], r12
	mov [rbp - 8 * 3], r13
	mov [rbp - 8 * 4], r14
	mov [rbp - 8 * 5], r15
	push rbp

	mov [ctx], rdi

	call wm_scaled_zoom_compute
	call ctx_bbox_adjust
	call ctx_print
	call tile_db_dir_open
		jnc .exit_nok
	call features.init
		jnc .exit_nok
	call file_mmap
		jnc .exit_nok ; carry already set

	; the first msg is a blob hdr
	mov blob_hdr_p, [start]
	mov finish, blob_hdr_p
	add finish, qword [stat + linux.stat_t.sz]
	TRACE 'PBF:start = %p:finish = %p', blob_hdr_p, finish
	;-------------------------------------------------------------------------------------------
	; setup internal flags
	xor flags, flags
	mov ctx_p, [ctx]
	test qword [ctx_p + ctx_t.flags], 0xf
		jz .flags_setup_done
	or flags, 10000b ; [4] = 1, use bbox
.flags_setup_done:
	;-------------------------------------------------------------------------------------------
	jmp blob.hdr.parse
.relations_2nd_pass:; in this pass we will resolve the ids of members of relations to pointers
	and flags, 10000b ; clear all flags except the bbox one
	jmp pbf.blk.primblk.primgrp.relations.resolve_ids
; in this pass we will select the features based on the bbox, zoom level and those we actually do
; render
.features_select_pass:
	and flags, 10000b ; clear all flags except the bbox one
	jmp pbf.features.select
.exit_ok:
	xor eax, eax ; return 0
.exit: ; XXX: cleanup is currently not a thing
	pop rbp
	mov rbx, [rbp - 8 * 1]
	mov r12, [rbp - 8 * 2]
	mov r13, [rbp - 8 * 3]
	mov r14, [rbp - 8 * 4]
	mov r15, [rbp - 8 * 5]
	leave
	ret
.exit_nok:
	test rax, rax 
	setz al ; return 1 if rax is zero
	jmp .exit
;---------------------------------------------------------------------------------------------------
purge ctx_p
purge finish
purge blob_hdr_p
purge flags
;---------------------------------------------------------------------------------------------------
stats.print: ; very probably public/visible one day
	enter 0,0
	and rsp, not 0xf
	lea rdi, [stats.msg]
	mov rsi, [blk.primblk.primgrp.dense.nodes.n]
	mov rdx, [blk.primblk.primgrp.ways.n]
	mov rcx, [blk.primblk.primgrp.relations.n]
	call qword [libc.printf]
	leave
	ret
;---------------------------------------------------------------------------------------------------
include 'blob/text.s'
include 'blk/text.s'
include 'features/text.s'
include 'math.s'
;===================================================================================================
;{{{ macros
; in: p, pointer on varint 
; out: out_val, varint value as 64 bits reg
; out: p, pointer on byte right after the varint ("finish")
; clobber: rax, cl
macro varint_load out_val, p
	local next_byte
	local no_more
	zreg out_val ; see util macros
	xor cl, cl
next_byte:
	xor eax, eax
	mov al, [p]
	add p, 1
	btr ax, 7
		jnc no_more
	shl rax, cl
	add cl, 7
	or out_val, rax
	jmp next_byte
no_more:
	shl rax, cl
	or out_val, rax
end macro
;---------------------------------------------------------------------------------------------------
; in/out: val_p, point on the start of val, updated to point right after val
; in: field_type, field type
; clobber: ax, internal_varint_load
macro val_skip val_p, in_field_type, internal_varint_load
	local type_varint_byte_skip
	local type_64bits_skip
	local type_length_delimited_skip
	local type_32bits_skip
	local exit
	test in_field_type, in_field_type
		jz type_varint_byte_skip
	cmp in_field_type, 1
		je type_64bits_skip
	cmp in_field_type, 2
		je type_length_delimited_skip
	cmp in_field_type, 5
		je type_32bits_skip
	jmp exit
type_varint_byte_skip:
	mov al, [val_p]
	add val_p, 1
	bt ax, 7 ; only 16bits
		jc type_varint_byte_skip
	jmp exit
type_64bits_skip:
	add val_p, 8
	jmp exit
type_length_delimited_skip:
	varint_load internal_varint_load, val_p
	add val_p, internal_varint_load ; pointer on val + val_sz
	jmp exit
type_32bits_skip:
	add val_p, 4
exit:
end macro
;}}} macros
end namespace ; pbf
