; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
blk: namespace blk
align_nops
init_once:
	TRACE 'PBF:BLK:INIT_ONCE'
	call primblk.init_once
		jnc .nok
	stc ; return true
	ret
.nok:
	clc ; return false
	ret
;---------------------------------------------------------------------------------------------------
; from blob
;define p 		rbx ; in
;define p_finish	r12 ; in
define flags		r15 ; in/out
align_nops
parse:
	bt flags, 2 ; 0 = OSMHeader, 1 = OSMData (aka primblk...)
		jc primblk.parse
	; fallthru to blk.hdr.parse
;---------------------------------------------------------------------------------------------------
purge flags
;===================================================================================================
include 'hdr/text.s'
include 'primblk/text.s'
;===================================================================================================
end namespace ; blk
