; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace blk.primblk.strtbl
align 64
idx rq 1 ; strtbl look-up idx filled with str addrs
idx.bytes_n = 32 * 1024 * 1024 ; 32MiB, 4 millions str addrs, overkill on purpose
end namespace
