; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
strtbl: namespace strtbl
align_nops
init_once:
	TRACE 'PBF:BLK:PRIMBLK:STRTBL:INIT_ONCE'

	mov rbp, rsp
	and rsp, not 0xf

	xor edi, edi
	mov rsi, idx.bytes_n
	mov rdx, 0x1 or 0x2	; PROT_READ  | PROT_WRITE
	mov r10, 0x20 or 0x02	; MAP_ANONYMOUS | MAP_PRIVATE
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae .err_strtbl_idx_mmap_failed
	mov [idx], rax
	TRACE 'PBF:BLK:PRIMBLK:STRTBL:IDX:INIT_ONCE:string table index mmaped at %p', rax 
	stc ; return true
	mov rsp, rbp
	ret
.err_strtbl_idx_mmap_failed: ; in: rax = error code
	mov rdi, 2 ; stderr
	lea rsi, [idx.msg.mmap_failed]
	mov rdx, rax
	call qword [libc.dprintf]
	clc ; return false
	mov rsp, rbp
	ret
;---------------------------------------------------------------------------------------------------
; we create an idx for fast look-up
; scratch: rax rcx rdx
; leaf code
; from primblk
;define primblk_p		rbx
;define primblk_p_finish	r12
;define primgrp_p		r13
;define primgrp_p_finish	r14
;define flags			r15
define idx_start_p		r8
define p			r10
define p_finish			rsi
; local
define idx_p			rdi
align_nops
parse:
	mov idx_p, idx_start_p
.str_next:
	cmp p, p_finish
		je primblk.parse.next_key
	inc p ; skip the msg field num/type since it is always num = 1/type = 2 namely 0x0a
	mov [idx_p], p ; idx_p = ptr on current byte
	varint_load rdx, p ; = str_sz
	add p, rdx ; skip str
	add idx_p, 8 ; points on next str ptr
	jmp .str_next
;---------------------------------------------------------------------------------------------------
;purge primblk_p
;purge primblk_p_finish
;purge primgrp_p
;purge primgrp_p_finish
;purge flags
purge idx_start_p
purge p
purge p_finish
purge idx_p
end namespace ; strtbl
