; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
primgrp: namespace primgrp
align_nops
init_once:
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:INIT_ONCE'
	call dense.init_once
		jnc .nok
	call ways.init_once
		jnc .nok
	call relations.init_once
		jnc .nok
	stc ; return true
	ret
.nok:
	clc ; return false
	ret
;===================================================================================================
; scratch: rax rcx r11 r9 xmm7 xmm6 xmm5 xmm4
; from primblk
define primblk_p			rbx
	define dense_p			rbx
	define ways_p			rbx
	define relations_p		rbx
define primblk_p_finish			r12
define flags				r15
define p				r13
	define dense_p_finish 		r13
	define ways_p_finish		r13
	define relations_finish_p	r13
define p_finish				r14
define granularity			rbp ; f64
define strtbl_idx_p			r8
define lat_of_lon_of			xmm3 ; f64/f64
align_nops
parse:
.next_key:
	cmp p, p_finish
		je primblk.parse.next_key
	varint_load r11, p ; key
	mov r9, r11 ; = key copy
	shr r11, 3 ; = field num

	cmp r11, 1
		je .nodes_found
	cmp r11, 2
		je .dense_found
	cmp r11, 3
		je .ways_found
	cmp r11, 4
		je .relations_found
	and r9, 111b ; = field type

	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:UNHANDLE FIELD type=%lu num=%lu', r9, r11
	val_skip p, r9, r11
	jmp .next_key
align_nops
.nodes_found:
	varint_load r11, p ; nodes_sz
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:NODES:start=%p:size=%lu(0x%lx) bytes', p, r11, r11
	add p, r11 ; skip
	; XXX: NODES ARE PROBABLY DEPRECATED IN FAVOR OF DENSE
	jmp .next_key
align_nops
.dense_found:
	varint_load r11, p ; dense_sz
	mov r9, p ;  = dense_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:DENSE:start=%p:size=%lu(0x%lx) bytes', r9, r11, r11
	add p, r11 ; = dense_p_finish
	; make some reg room for dense parsing -- START
	sub rsp, 8 * 4
	mov [rsp + 8 * 0], primblk_p
	mov [rsp + 8 * 1], primblk_p_finish
	mov [rsp + 8 * 2], p
	mov [rsp + 8 * 3], p_finish
	; make some reg room for dense parsing -- END
	mov dense_p, r9
	jmp dense.parse ; return will happen in .restore_from_dense
align_nops
.return_from_dense:
	mov primblk_p,		[rsp + 8 * 0]
	mov primblk_p_finish,	[rsp + 8 * 1]
	mov p,			[rsp + 8 * 2]
	mov p_finish,		[rsp + 8 * 3]
	add rsp, 8 * 4
	jmp .next_key
align_nops
.ways_found:
	inc qword [ways.n]
	varint_load r11, p ; ways_sz
	mov r9, p ; ways_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:WAYS:start=%p:size=%lu(0x%lx) bytes', r9, r11, r11
	add p, r11 ; = ways_p_finish
	; make some reg room for ways parsing -- START
	sub rsp, 8 * 5
	mov [rsp + 8 * 0],	primblk_p
	mov [rsp + 8 * 1],	primblk_p_finish
	mov [rsp + 8 * 2],	p
	mov [rsp + 8 * 3],	p_finish
	mov [rsp + 8 * 4],	granularity
	; make some reg room for ways parsing -- END
	mov ways_p, r9
	jmp ways.parse ; return will happen below in .return_from_ways
align_nops
.return_from_ways:
	mov primblk_p,		[rsp + 8 * 0] 
	mov primblk_p_finish,	[rsp + 8 * 1] 
	mov p,			[rsp + 8 * 2] 
	mov p_finish,		[rsp + 8 * 3] 
	mov granularity,	[rsp + 8 * 4]
	add rsp, 8 * 5
	jmp .next_key
align_nops
.relations_found:
	inc qword [relations.n]
	varint_load r11, p ; relations_sz
	mov r9, p ; relations_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:RELATIONS:start=%p:size=%lu(0x%lx) bytes', p, r11, r11
	add p, r11 ; = relations_finish_p
	; make some reg room for relations parsing -- START
	sub rsp, 8 * 5
	mov [rsp + 8 * 0],	primblk_p
	mov [rsp + 8 * 1],	primblk_p_finish
	mov [rsp + 8 * 2],	p
	mov [rsp + 8 * 3],	p_finish
	mov [rsp + 8 * 4],	granularity
	; make some reg room for relations parsing -- END
	mov relations_p, r9
	jmp relations.parse
align_nops
.return_from_relations:
	mov primblk_p,		[rsp + 8 * 0] 
	mov primblk_p_finish,	[rsp + 8 * 1] 
	mov p,			[rsp + 8 * 2] 
	mov p_finish,		[rsp + 8 * 3] 
	mov granularity,	[rsp + 8 * 4]
	add rsp, 8 * 5
	jmp .next_key
;---------------------------------------------------------------------------------------------------
purge primblk_p
purge dense_p
purge ways_p
purge primblk_p_finish
purge flags
purge p
purge dense_p_finish
purge ways_p_finish
purge p_finish
purge granularity
purge strtbl_idx_p
purge lat_of_lon_of
;===================================================================================================
include 'dense/text.s'
include 'ways/text.s'
include 'relations/text.s'
;===================================================================================================
end namespace ; primgrp
