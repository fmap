; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace blk.primblk.primgrp.relations
align 64
array			rq 1
array.finish		rq 1 ; finish/next relation, inited to above array
n			rq 1 ; how many relations in this pbf
array.idx		rq 1 ; binary tree, bits from the relation id
array.idx.finish	rq 1 ; next available idx slot
namespace array ;{{{ -------------------------------------------------------------------------------
	bytes_n = 8 * 1024 * 1024 * 1024 ; 8GiB
	relation_t: namespace relation_t ;{{{ ------------------------------------------------------
		next		:= 0 ; ptr on the next relation (lookup for relation-only)
		members		:= 8 ; qw ptr on the members
		; (key/val)s:
		; keys start with a byte containing their sz in bytes
		; vals start with a short containing their sz in bytes
		; the array is terminated with a 0-szed key
		keys_vals	:= 16
		; after the (key,val)s come the members
		member_t: namespace member_t
			type	:= 0 ; byte, 0xff for the terminator
			; XXX: the following field is actually filled with the decoded id first
			; then will be resolved to a ptr in a second pass using the various
			; idxs
			p	:= 1 ; qw
			role	:= 9 ; role starts with a byte containing its sz in bytes
		end namespace
	end namespace ;}}} relation_t --------------------------------------------------------------
end namespace ;}}} array ---------------------------------------------------------------------------
namespace array.idx ;{{{ ---------------------------------------------------------------------------
	slot_t: namespace slot_t
		bit0		:= 0	; qw, ptr on idx slot for this bit
		bit1		:= 8	; qw, idem
		relation	:= 16	; qw, ptr in the array for this id
		bytes_n		:= 24
	end namespace
	n = 1000 * 1000 * 1000 ; 1 billion relations
	bytes_n = n * slot_t.bytes_n ; 24GB
end namespace ;}}} array.idx -----------------------------------------------------------------------
end namespace
