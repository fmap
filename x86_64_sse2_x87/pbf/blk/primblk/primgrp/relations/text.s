; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
; XXX: We actually used this code unit as a training ground. Expect it to be very overkill.
relations: namespace relations
;{{{ init_once -------------------------------------------------------------------------------------
align_nops
init_once:
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:RELATIONS:INIT_ONCE'
	mov rbp, rsp
	and rsp, not 0xf

	xor edi, edi
	mov rsi, array.bytes_n
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae .err_relations_mmap_failed
	mov [array], rax
	mov [array.finish], rax

	lea rdi, [msg.idx_relations_n]
	mov rsi, array.idx.n
	call qword [libc.printf]

	xor edi, edi
	mov rsi, array.idx.bytes_n
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae .err_relations_idx_mmap_failed
	mov [array.idx], rax
	add rax, 8 * 3
	mov [array.idx.finish], rax ; next availabe idx slot

	stc ; return true
	mov rsp, rbp
	ret
.err_relations_mmap_failed:
	mov rdi, 2 ; stderr
	mov rsi, rax ; errno
	lea rsi, [msg.mmap_failed]
	call qword [libc.dprintf]
	clc ; return false
	mov rsp, rbp
	ret
.err_relations_idx_mmap_failed:
	mov rdi, 2 ; stderr
	mov rsi, rax ; errno
	lea rsi, [msg.idx_mmap_failed]
	call qword [libc.dprintf]
	clc ; return false
	mov rsp, rbp
	ret
;}}} init_once -------------------------------------------------------------------------------------
; local memcpy ABI
define memcpy_dst		rbx	; memcpy_dst(out) = memcpy_dst(in) + memcpy_bytes_n
define memcpy_src		r11	; clobbered, don't care
define memcpy_bytes_n		r9	; clobbered, don't care
define memcpy_link		rcx	; the return addr
;===================================================================================================
;{{{ parse -----------------------------------------------------------------------------------------
; scratch ~ rax rcx r11 r9
; from primgrp
define relations_p		rbx	; in
define relations_finish_p 	r13	; in
define flags			r15	; in/out
define strtbl_idx_p		r8
define lat_of_lon_of		xmm3 	; f64/f64
; local
define id			r12
define keys_p			r14	; can be 0
define keys_p_d			r14d	; can be 0
define keys_finish_p		rbp
define vals_p			r10	; num of vals is num of keys
define memids_p			rdx 	; memids = MEMber IDs
define memids_p_d		edx
define memids_finish_p		rsi
define types_p			rdi	; num of types is num of memids
define roles_p_xmm		xmm2 	; the role of a member is a string
align_nops
parse:
	xor keys_p_d, keys_p_d ; if no (key/val)
	xor memids_p_d, memids_p_d ; presume we can have no memids (should not happen though)
align_nops
.next_msg_key:
	; "relationS" is plural because of the primgrp field name, but there is
	; actually only one "relation" which will be repeated
	cmp relations_p, relations_finish_p
		je unserialize
	varint_load r11, relations_p ; msg key
	mov r9, r11 ; = msg key copy
	shr r11, 3 ; = field num
	cmp r11, 1
		je .id_found
	cmp r11, 2
		je .keys_found
	cmp r11, 3
		je .vals_found
	cmp r11, 8
		je .roles_found
	cmp r11, 9
		je .memids_found
	cmp r11, 10
		je .types_found
	and r9, 111b ; field type
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:RELATIONS:MSG:FIELD %lu OF TYPE %lu', r11, r9
	val_skip relations_p, r9, r11
	jmp .next_msg_key
align_nops
.id_found:
	varint_load id, relations_p
	jmp .next_msg_key
align_nops
.keys_found:
	varint_load r11, relations_p ; keys_sz
	mov keys_p, relations_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:RELATIONS:KEYS:start = %p:size = %lu(0x%lx) bytes', relations_p, r11, r11
	add relations_p, r11
	mov keys_finish_p, relations_p
	jmp .next_msg_key
align_nops
.vals_found:
	varint_load r11, relations_p ; vals_sz
	mov vals_p, relations_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:RELATIONS:VALS:start = %p:size = %lu(0x%lx) bytes', vals_p, r11, r11
	add relations_p, r11
	jmp .next_msg_key
align_nops
.memids_found:
	varint_load r11, relations_p ; memids_sz
	mov memids_p, relations_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:RELATIONS:MEMIDS:start = %p:size = %lu(0x%lx) bytes', memids_p, r11, r11
	add relations_p, r11
	mov memids_finish_p, relations_p
	jmp .next_msg_key
align_nops
.types_found:
	varint_load r11, relations_p ; types_sz
	mov types_p, relations_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:RELATIONS:TYPES:start = %p:size = %lu(0x%lx) bytes', types_p, r11, r11
	add relations_p, r11
	jmp .next_msg_key
align_nops
.roles_found:
	varint_load r11, relations_p ; roles_sz
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:RELATIONS:ROLES:start = %p:size = %lu(0x%lx) bytes', relations_p, r11, r11
	movq roles_p_xmm, relations_p
	add relations_p, r11
	jmp .next_msg_key
purge relations_finish_p
purge relations_p
;}}} parse -----------------------------------------------------------------------------------------
;{{{ unserialize -----------------------------------------------------------------------------------
; scratch ~ rax rcx r11 r9
; from primgrp
;define flags			r15	; in/out
;define strtbl_idx_p		r8
;define lat_of_lon_of		xmm3 	; f64/f64
; local from parse
;define id			r12
;define keys_p			r14	; can be 0
;define keys_p_d			r14d	; can be 0
;define keys_finish_p		rbp
;define vals_p			r10	; num of vals is num of keys
;define memids_p		rdx 	; memids = MEMber IDs
;define memids_p_d		edx
;define memids_finish_p		rsi
;define types_p			rdi	; num of types is num of memids
;define roles_p_xmm		xmm2 	; the role of a member is a string
; local
define relation_p		rbx
define relation_start_p		r13
align_nops
unserialize:
	mov relations_p, [array.finish]
	mov relation_start_p, relations_p
;{{{ keys_val_cpy ----------------------------------------------------------------------------------
keys_vals_cpy:
	add relation_p, array.relation_t.keys_vals ; relation_p points on the start of the keys_vals section
	test keys_p, keys_p
		jz unserialize.keys_vals_done
	jmp .entry
align_nops
.next_key_val:
	cmp keys_p, keys_finish_p
		je unserialize.keys_vals_done
.entry:
	varint_load r11, keys_p ; = key_id, keys_p points on next key_id
	mov r11, [strtbl_idx_p + 8 * r11] ; = key_src_addr
	varint_load r9, r11 ; r9 = key_bytes_n, r11 = key_src_addr
	mov [relation_p], r9b ; store the byte containing the sz of the key str in bytes
	inc relation_p
	; memcpy_dst = relation_p(rbx), memcpy_src = key_src_addr(r11), memcpy_bytes_n = key_bytes_n(r9)
	lea memcpy_link, [.key_cpy_done]
	jmp memcpy ; relation_p = relation_p + key_bytes_n
align_nops
.key_cpy_done:
	varint_load r11, vals_p ; = val_id, vals_p points on next val_id
	mov r11, [strtbl_idx_p + 8 * r11] ; = val_src_addr
	varint_load r9, r11; r9 = val_bytes_n, r11 = val_src_addr
	mov [relation_p], r9w ; store the short containing the sz of the val str in bytes
	add relation_p, 2
	; memcpy_dst = relation_p(rbx), memcpy_src = val_src_addr(r11), memcpy_bytes_n = val_bytes_n(r9)
	lea memcpy_link, [.next_key_val]
	jmp memcpy ; relation_p = relation_p + key_bytes_n
;}}} keys_val_cpy ----------------------------------------------------------------------------------
align_nops
unserialize.keys_vals_done:
	mov byte [relation_p], 0 ; insert the 0-szed key/terminator
	inc relation_p
	; here relation_p points on the start of the section of members
	mov [relation_start_p + array.relation_t.members], relation_p
	test memids_p, memids_p
		jz unserialize.members_done ; unlikely
purge vals_p
purge keys_finish_p
purge keys_p_d
purge keys_p
;{{{ members ---------------------------------------------------------------------------------------
; scratch ~ rax rcx r11 r9
; from primgrp
;define flags			r15	; in/out
;define strtbl_idx_p		r8
;define lat_of_lon_of		xmm3 	; f64/f64
; local from parse
;define id			r12
;define memids_p		rdx 	; memids = MEMber IDs
;define memids_p_d		edx
;define memids_finish_p		rsi
;define types_p			rdi	; num of types is num of memids
;define roles_p_xmm		xmm2 	; the role of a member is a string
; local from unserialize
;define relation_p		rbx
;define relation_start_p	r13
; local
define roles_p			r14 	; get it from xmm roles_p_xmm
define prev_memid		rbp
define prev_memid_d		ebp
define member_p			relation_p ; alias
define member_p.p		member_p + array.relation_t.member_t.p
define member_p.type		member_p + array.relation_t.member_t.type
members:
	movq roles_p, roles_p_xmm
	xor prev_memid_d, prev_memid_d ; prev_id = 0
	jmp .entry
align_nops
.next_member:
	cmp memids_p, memids_finish_p
		je unserialize.members_done
.entry:
	; decode the relation id -------------------------------------------------------------------
	varint_load r11, memids_p ; load/skip the raw zigzag delta_refs
	mov rax, r11 
	shr r11, 1 ; i >> 1
	and rax, 1 ; i & 1
	neg rax ; -(i & 1)
	xor r11, rax ; (i >> 1) ^ -(i & 1)  ; = delta_id
	add prev_memid, r11 ; = prev_memid = memid because DELTA encoded
	mov [member_p.p], prev_memid ; store memid
	; decode the type --------------------------------------------------------------------------
	varint_load r11, types_p
	mov byte [member_p.type], r11b ; a byte in enough
	; cpy the role str -------------------------------------------------------------------------
	add member_p, array.relation_t.member_t.role ; member_p points on the first byte of the role str
	varint_load r11, roles_p ; = role_str_id
	mov r11, [strtbl_idx_p + 8 * r11] ; role_src_addr
	varint_load r9, r11 ; r9 = role_bytes_n, r11 = role_src_addr
	mov [member_p], r9b ; insert the byte containing the sz of the role str in bytes
	inc member_p
	; memcpy_dst = member_p(rbx), memcpy_src = role_src_addr(r11), memcpy_bytes_n = role_bytes_n(r9)
	lea memcpy_link, [.next_member]
	jmp memcpy ; member_p = member_p + role_bytes_n
purge member_p.type
purge member_p.p
purge member_p
purge prev_memid_d
purge prev_memid
purge roles_p
;}}} members ---------------------------------------------------------------------------------------
purge roles_p_xmm
purge memids_finish_p
purge memids_p_d
purge memids_p
purge types_p
unserialize.members_done:
	; here relation_p points on the terminating member (only a 0xff type and nothing else)
	mov byte [relation_p], 0xff ; the terminator type is at the start of relation_p
	inc relation_p ; = next relation
	mov [relation_start_p + array.relation_t.next], relation_p
	mov [array.finish], relation_p
purge relation_p
;{{{ idx_insert_node -------------------------------------------------------------------------------
; scratch ~ rax
; from primgrp
;define flags			r15	; in/out
;define strtbl_idx_p		r8
;define lat_of_lon_of		xmm3 	; f64/f64
; local from parse
;define id			r12
; local from unserialize
;define relation_start_p	r13
; local
define idx_finish_p		rbx
define id_bit_idx		r14
define id_bit_idx_d		r14d
define bit_val_select		rcx ; we use rcx here because we may use rcx compressed instructions
define bit_val_select_d		ecx
define bit_val_select_b		cl
define id_bit_idx_msb		rbp
define idx_slot_p		r9
; TODO:REMOVE
;define idx_slot_p.relation	idx_slot_p + array.idx.slot_t.relation
idx_insert_node:
	mov idx_slot_p, [array.idx]
	mov idx_finish_p, [array.idx.finish]
	TRACE_RELATIONS_IDX 'id=%#016lx nodes.idx=%p nodes.finish=%p', id, idx_slot_p, idx_finish_p
	xor id_bit_idx_d, id_bit_idx_d
	xor bit_val_select_d, bit_val_select_d
	mov id_bit_idx_msb, -1 ; this is to make it work with id = 0
	bsr id_bit_idx_msb, id ; if id = 0, id_bit_idx_msb is untouched namely -1
	TRACE_RELATIONS_IDX 'id_bit_idx_msb=%lu', id_bit_idx_msb
align_nops
.next_id_bit:
	TRACE_RELATIONS_IDX 'id_bit_idx=%lu', id_bit_idx
	cmp id_bit_idx_msb, id_bit_idx
		jb .insert_here
	bt id, id_bit_idx
	setc bit_val_select_b ; bit_val_select = 1 if id[id_bit_idx] = 1, else 0
	TRACE_RELATIONS_IDX 'bit_val_select=%lu', bit_val_select
	mov rax, [idx_slot_p + 8 * bit_val_select]
	TRACE_RELATIONS_IDX 'slot=%p bit slot=%p', idx_slot_p, rax
	test rax, rax
		jnz .idx_existing_slot
	TRACE_RELATIONS_IDX 'non existing slot using finish=%p', idx_finish_p
	mov rax, idx_finish_p
	add idx_finish_p, array.idx.slot_t.bytes_n ; we could zero the mem here, but mmap does it for us
if ~SYSTEM_LEGACY
	prefetchw [idx_finish_p + 64 * 2]	; try to get ready
	prefetchw [idx_finish_p + 64 * 3]
end if
	mov [idx_slot_p + 8 * bit_val_select], rax
align_nops
.idx_existing_slot:
	mov idx_slot_p, rax
	TRACE_RELATIONS_IDX 'next_slot=%p', idx_slot_p
	inc id_bit_idx

	jmp .next_id_bit
align_nops
.insert_here:
if TRACE_RELATIONS_IDX_ENABLED
	mov rax, [idx_slot_p + array.idx.slot_t.relation]
	test rax, rax
		jz .relation_slot_available
	TRACE_RELATIONS_IDX 'WARNING: overwritting an existing relation=%p', rax
.relation_slot_available:
	TRACE_RELATIONS_IDX 'inserting relation=%p in slot=%p', relation_start_p, idx_slot_p
end if
	mov [idx_slot_p + array.idx.slot_t.relation], relation_start_p
	; XXX: if one day we need to be backed by a file we will need to DONT_NEED "madvise" the
	mov [array.idx.finish], idx_finish_p
	; the current relation memory
purge idx_slot_p
purge id_bit_idx_msb
purge bit_val_select_b
purge bit_val_select_d
purge bit_val_select
purge id_bit_idx_d
purge id_bit_idx
purge idx_finish_p
;}}} idx_insert_node -------------------------------------------------------------------------------
purge relation_start_p
purge id
purge lat_of_lon_of
purge strtbl_idx_p
purge flags
	jmp primgrp.parse.return_from_relations
;}}} unserialize -----------------------------------------------------------------------------------
;{{{ local memcpy(sse) -----------------------------------------------------------------------------
; XXX: lddqu performs aligned reads, carefull where you use this, but should be fine with classic
; paginated memory.
; scratch ~ rax xmm7 xmm6 xmm5 xmm4
align_nops
memcpy:
	cmp memcpy_bytes_n, 16 * 4
		jb .below64
	; since we can be cache line mis-aligned, speculatively do prefetch 2 cls ahead
	prefetchnta [memcpy_src + 64 * 2]  ; speculative non-temporal "3rd" cache line ahead
	prefetchnta [memcpy_src + 64 * 3]  ; speculative non-temporal "4th" cache line ahead
if ~SYSTEM_LEGACY
	prefetchw [memcpy_dst + 64 * 2]
	prefetchw [memcpy_dst + 64 * 3]
end if
	lddqu xmm7, [memcpy_src + 16 * 0]
	lddqu xmm6, [memcpy_src + 16 * 1]
	lddqu xmm5, [memcpy_src + 16 * 2]
	lddqu xmm4, [memcpy_src + 16 * 3]

	movdqu [memcpy_dst + 16 * 0], xmm7
	movdqu [memcpy_dst + 16 * 1], xmm6
	movdqu [memcpy_dst + 16 * 2], xmm5
	movdqu [memcpy_dst + 16 * 3], xmm4

	add memcpy_dst, 16 * 4
	sub memcpy_bytes_n, 16 * 4
		jz .done
	add memcpy_src, 16 * 4
	jmp memcpy
align_nops
.below64:
	cmp memcpy_bytes_n, 16 * 3
		jb .below48

	lddqu xmm7, [memcpy_src + 16 * 0]
	lddqu xmm6, [memcpy_src + 16 * 1]
	lddqu xmm5, [memcpy_src + 16 * 2]

	movdqu [memcpy_dst + 16 * 0], xmm7
	movdqu [memcpy_dst + 16 * 1], xmm6
	movdqu [memcpy_dst + 16 * 2], xmm5

	add memcpy_dst, 16 * 3
	sub memcpy_bytes_n, 16 * 3
		jz .done
	add memcpy_src, 16 * 3
	; fall-thru
align_nops
.below48:
	cmp memcpy_bytes_n, 16 * 2
		jb .below32

	lddqu xmm7, [memcpy_src + 16 * 0]
	lddqu xmm6, [memcpy_src + 16 * 1]

	movdqu [memcpy_dst + 16 * 0], xmm7
	movdqu [memcpy_dst + 16 * 1], xmm6

	add memcpy_dst, 16 * 2
	sub memcpy_bytes_n, 16 * 2
		jz .done
	add memcpy_src, 16 * 2
	; fall-thru
align_nops
.below32:
	cmp memcpy_bytes_n, 16 * 1 
		jb .below16

	lddqu xmm7, [memcpy_src + 16 * 0]
	movdqu [memcpy_dst + 16 * 0], xmm7
	
	add memcpy_dst, 16 * 1
	sub memcpy_bytes_n, 16 * 1
		jz .done
	add memcpy_src, 16 * 1
	; fall-thru
align_nops
.below16:
	cmp memcpy_bytes_n, 8 * 1 
		jb .below8
	mov rax, [memcpy_src + 8 * 0]
	mov [memcpy_dst + 8 * 0], rax

	add memcpy_dst, 8 * 1
	sub memcpy_bytes_n, 8 * 1
		jz .done
	add memcpy_src, 8 * 1
	; fall-thru
align_nops
.below8:
	cmp memcpy_bytes_n, 7
		je .cpy7
	cmp memcpy_bytes_n, 6
		je .cpy6
	cmp memcpy_bytes_n, 5
		je .cpy5
	cmp memcpy_bytes_n, 4
		je .cpy4
	cmp memcpy_bytes_n, 3
		je .cpy3
	cmp memcpy_bytes_n, 2
		je .cpy2
	cmp memcpy_bytes_n, 1
		je .cpy1
	jmp memcpy_link
align_nops
.cpy7:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy6:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy5:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy4:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy3:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy2:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy1:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.done:
	jmp memcpy_link
;}}} local memcpy(sse) -----------------------------------------------------------------------------
; {{{ resolve_ids ----------------------------------------------------------------------------------
; from pbf.tile_db_build
define flags			r15
; local
define relation_p		rbx
define relations_finish_p	r12
define member_p			r13
define nodes_idx		r14
define ways_idx			rbp
define relations_idx		r11
define id			r10
define bit_val_select		r9
define bit_val_select_b 	r9b
define id_bit_idx_msb		rcx ; we use rcx for compressed opcodes
define id_bit_idx_msb_d		ecx
define idx_slot_p		r8
align_nops
resolve_ids:
	mov relation_p, [array]
	mov relations_finish_p, [array.finish]
	mov nodes_idx, [primgrp.dense.nodes.idx]
	mov ways_idx, [primgrp.ways.array.idx]
	mov relations_idx, [primgrp.relations.array.idx]
align_nops
.next_relation:
	TRACE_2ND_PASS 'relation=%p relation_finish_p=%p', relation_p, relations_finish_p
	cmp relation_p, relations_finish_p
		je pbf.tile_db_build.features_select_pass ; prep 3rd pass
	mov member_p, [relation_p + array.relation_t.members]
align_nops
.next_member:
	mov al, [member_p + array.relation_t.member_t.type]
	; this field actually contains the id after the first major pass
	mov id, [member_p + array.relation_t.member_t.p]
	TRACE_2ND_PASS 'member=%p type=%#2lx(lowest byte only) id=%#lx(invalid if type=0xff)', member_p, rax, id
	cmp al, 0xff
		jne .have_more_members
	mov relation_p, [relation_p + array.relation_t.next]
	TRACE_2ND_PASS 'no more members next relation=%p', relation_p
	jmp .next_relation
align_nops
.have_more_members:
	cmp al, 0x00
	cmove idx_slot_p, nodes_idx
		je .idx_selected
	cmp al, 0x01
	cmove idx_slot_p, ways_idx
		je .idx_selected
	cmp al, 0x02
	cmove idx_slot_p, relations_idx
.idx_selected:
	xor bit_val_select, bit_val_select ; bit_val_select = 0
	mov id_bit_idx_msb, -1 ; this is to make it work with ref = 0
	bsr id_bit_idx_msb, id ; if id = 0, ref_bit_idx_msb is untouched namely -1
	inc id_bit_idx_msb_d ; store the idx + 1, 32bits because of the following jecxz
align_nops
.next_id_bit:
	jecxz .idx_slot_lookup_done ; bit_idx + 1 is stored in cx then 0 means we are finished
	bt id, 0
	setc bit_val_select_b
	mov idx_slot_p, [idx_slot_p + 8 * bit_val_select]
	test idx_slot_p, idx_slot_p ; if the idx slot in 0, we have a missing node
		jz .missing_node ; unlikely
if ~SYSTEM_LEGACY
	prefetcht0 [idx_slot_p]
end if
	shr id, 1
	dec id_bit_idx_msb_d
	jmp .next_id_bit
align_nops
.idx_slot_lookup_done:
	mov rax, [idx_slot_p + 2 * 8] ; XXX: all 3 idxs (nodes/ways/relations) have the same struct
	mov [member_p + array.relation_t.member_t.p], rax
	TRACE_2ND_PASS 'id found in idx, replacing with pointer %p', rax
.skip_member_role: ; from unlikely code code below
	add member_p, array.relation_t.member_t.role + 1 ; points on the byte right after the byte containing the sz of the role str in bytes
	movzx rax, byte [member_p - 1]
	add member_p, rax
	jmp .next_member

.missing_node: ; unlikely
	mov qword [member_p + array.relation_t.member_t.p], 0
	TRACE_2ND_PASS 'missing id, pointer to 0'
	jmp .skip_member_role
purge idx_slot_p
purge id_bit_idx_msb_d
purge id_bit_idx_msb
purge bit_val_select_b
purge bit_val_select
purge id
purge relations_idx
purge ways_idx
purge nodes_idx
purge member_p
purge relations_finish_p
purge relation_p
purge flags
; }}} resolve_ids ----------------------------------------------------------------------------------
;===================================================================================================
purge memcpy_link
purge memcpy_bytes_n
purge memcpy_src
purge memcpy_dst
;{{{ macros ----------------------------------------------------------------------------------------
if TRACE_RELATIONS_IDX_ENABLED
	macro TRACE_RELATIONS_IDX fmt, regs&
		TRACE_PREFIX 'RELATIONS_IDX', fmt, regs
	end macro
else
	macro TRACE_RELATIONS_IDX fmt, regs&
	end macro
end if
;---------------------------------------------------------------------------------------------------
if TRACE_RELATIONS_2ND_PASS_ENABLED
	macro TRACE_2ND_PASS fmt, regs&
		TRACE_PREFIX 'RELATIONS_2ND_PASS', fmt, regs
	end macro
else
	macro TRACE_2ND_PASS fmt, regs&
	end macro
end if
;}}} macros ----------------------------------------------------------------------------------------
end namespace ; relations
