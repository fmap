namespace blk.primblk.primgrp.relations
	msg: namespace msg
		mmap_failed	db 'ERROR(%ld):PBF:PRIMBLK:PRIMGRP:RELATIONS:NODES:unable to mmap relations',10,0
		idx_mmap_failed	db 'ERROR(%ld):PBF:PRIMBLK:PRIMGRP:RELATIONS:NODES:IDX:unable to mmap the index',10,0
		idx_relations_n	db 'PBF:BLK:PRIMBLK:WAYS:INIT_ONCE:reserving a memory mapping for an index able to handle %lu relations',10,0
	end namespace
end namespace

