; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace blk.primblk.primgrp.dense
align 64
nodes			rq 1
nodes.finish		rq 1 ; finish node, inited to aboves nodes
nodes.n			rq 1 ; how many dense nodes in this pbf
nodes.idx		rq 1 ; binary tree, bits from the node id
nodes.idx.finish	rq 1 ; next available idx slot
namespace nodes ;{{{ -------------------------------------------------------------------------------
	bytes_n = 64 * 1024 * 1024 * 1024 ; 64GiB 
	node_t: namespace node_t ;{{{ --------------------------------------------------------------
		next		:= 0	; ptr on next node (lookup for node-only features)
		flags		:= 8	; expected to be inited to 0
			flags.wm_computed := 0
		geo		:= 16	; vec2f64
		geo.lon		:= 16 	; f64
		geo.lat		:= 24	; f64
		wm.tile		:= 32	; vec2s64/vec2f64
		wm.tile.xy	:= 32	; vec2s64, >=0
		wm.tile.xy.x	:= 32	; s64
		wm.tile.xy.y	:= 40 	; s64
		; section which will go into the tile db  -- START =================================
		wm.tile.uv	:= 48 	; vec2f64, location into the tile, [0,1[
		wm.tile.uv.u	:= 48 	; f64
		wm.tile.uv.v	:= 56	; f64
		; (key,val)s:
		; keys start with a byte containing their sz in bytes
		; vals start with a short containing their sz in bytes
		; the array is terminated with a 0-szed key
		keys_vals	:= 64
		; section which will go into the tile db  -- END ===================================
	end namespace ;}}} node_t ------------------------------------------------------------------
end namespace ;}}} nodes ---------------------------------------------------------------------------
namespace nodes.idx ;{{{ ----------------------------------------------------------------------------
	slot_t: namespace slot_t
		bit0	:= 0	; qw, ptr on idx slot for this bit
		bit1	:= 8	; qw, idem
		node	:= 16	; qw, ptr on the node in the array
		bytes_n := 24
	end namespace
	n = 1000 * 1000 * 1000 ; 1 billion nodes
	bytes_n = n * slot_t.bytes_n ; 24GB
end namespace ;}}} nodes.idx ------------------------------------------------------------------------
end namespace
