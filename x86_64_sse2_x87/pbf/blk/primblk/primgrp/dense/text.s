; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
; XXX: We actually used this code unit as a training ground. Expect it to be very overkill.
dense: namespace dense
;{{{ init_once -------------------------------------------------------------------------------------
align_nops
init_once:
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:DENSE:INIT_ONCE'
	mov rbp, rsp
	and rsp, not 0xf

	xor edi, edi
	mov rsi, nodes.bytes_n
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae .err_nodes_mmap_failed
	mov [nodes], rax
	mov [nodes.finish], rax

	lea rdi, [nodes.msg.idx_nodes_n]
	mov rsi, nodes.idx.n
	call qword [libc.printf]

	xor edi, edi
	mov rsi, nodes.idx.bytes_n
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae .err_nodes_idx_mmap_failed
	mov [nodes.idx], rax
	add rax, nodes.idx.slot_t.bytes_n
	mov [nodes.idx.finish], rax ; next availabe idx slot

	stc ; return true
	mov rsp, rbp
	ret
.err_nodes_mmap_failed:
	mov rdi, 2 ; stderr
	mov rsi, rax ; errno
	lea rsi, [nodes.msg.mmap_failed]
	call qword [libc.dprintf]
	clc ; return false
	mov rsp, rbp
	ret
.err_nodes_idx_mmap_failed:
	mov rdi, 2 ; stderr
	mov rsi, rax ; errno
	lea rsi, [nodes.msg.idx_mmap_failed]
	call qword [libc.dprintf]
	clc ; return false
	mov rsp, rbp
	ret
;}}} init_once -------------------------------------------------------------------------------------
; local memcpy ABI
define memcpy_dst		rbx	; memcpy_dst(out) = memcpy_dst(in) + memcpy_bytes_n
define memcpy_src		r11	; clobbered, don't care
define memcpy_bytes_n		r9	; clobbered, don't care
define memcpy_link		rcx	; the return addr
;===================================================================================================
;{{{ parse -----------------------------------------------------------------------------------------
; scratch ~ rax rcx r11 r9
; from primgrp
define dense_p			rbx ; in
define dense_p_finish 		r13 ; in
define flags			r15
define granularity		rbp ; f64
define strtbl_idx_p		r8
define lat_of_lon_of		xmm3 ; f64/f64
; local
define id_p			r12
define id_p_finish		r14
define lat_p			r10
define lon_p			rdx
define keys_vals_p		rsi	; can be 0
align_nops
parse:
	xor keys_vals_p, keys_vals_p ; = 0 because may not have any (see specs)
align_nops
.next_key:
	cmp dense_p, dense_p_finish
		je unserialize
	varint_load r11, dense_p ; = key
	mov r9, r11 ; = key copy
	shr r11, 3 ; = field num
	cmp r11, 1
		je .id_found
	cmp r11, 8
		je .lat_found
	cmp r11, 9
		je .lon_found
	cmp r11, 10
		je .keys_vals_found
	and r9, 111b ; = field type
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:DENSE:MSG:FIELD %lu OF TYPE %lu', r11, r9
	val_skip dense_p, r9, r11
	jmp .next_key
align_nops
.id_found:
	varint_load r11, dense_p ; = packed_ids_sz
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:DENSE:ID:start = %p:size = %lu(0x%lx) bytes', dense_p, r11, r11
	mov id_p, dense_p
	add dense_p, r11 ; dense_p + packed_ids_sz
	mov id_p_finish, dense_p
	jmp .next_key
align_nops
.lat_found:
	varint_load r11, dense_p ;= packed_lat_sz
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:DENSE:LAT:start = %p:size = %lu(0x%lx) bytes', dense_p, r11, r11
	mov lat_p, dense_p
	add dense_p, r11 ; dense_p + packed_lat_sz
	jmp .next_key
align_nops
.lon_found:
	varint_load r11, dense_p ;= packed_lon_sz
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:DENSE:LON:start = %p:size = %lu(0x%lx) bytes', dense_p, r11, r11
	mov lon_p, dense_p
	add dense_p, r11 ; p + packed_lon_sz
	jmp .next_key
align_nops
.keys_vals_found:
	varint_load r11, dense_p ;= packed_keys_vals_sz
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:DENSE:KEYS_VALS:start = %p:size = %lu(0x%lx) bytes', dense_p, r11, r11
	mov keys_vals_p, dense_p
	add dense_p, r11 ; dense_p + packed_keys_vals_sz
	jmp .next_key
purge dense_p_finish
purge dense_p
;}}} parse -----------------------------------------------------------------------------------------
;{{{ unserialize -----------------------------------------------------------------------------------
; scratch ~ rax rcx r11 r9 xmm7 xmm6 xmm5
; from primgrp
;define flags			r15
;define granularity		rbp ; f64
;define strtbl_idx_p		r8
;define lat_of_lon_of		xmm3 ; f64/f64
; local from parse:
;define id_p			r12
;define id_p_finish		r14
;define lat_p			r10
;define lon_p			rdx
;define keys_vals_p		rsi ; can be 0
; local
define node_p			rbx	; reuse dense_p
define prev_id			r13	; reuse dense_p_finish
define node_start_p		rdi
define granularity_xmm		xmm2
define prev_lat_prev_lon	xmm1
define one_one			xmm0
; PERF
tsc_nodes TSC.STORAGE 'TSC:DENSE:%lu'
align_nops
unserialize:
	mov node_p, [nodes.finish]
if ~SYSTEM_LEGACY
	prefetchw [node_p] ; 2 cls should be very common
	prefetchw [node_p + 64]
end if
	pxor one_one, one_one ; construct the constant
	pcmpeqd one_one, one_one ; xmm = 0xfffff... (128bits)
	psrlq one_one, 63

	xor prev_id, prev_id ; = 0
	pxor prev_lat_prev_lon, prev_lat_prev_lon ; = 0

	movq granularity_xmm, granularity
	punpcklqdq granularity_xmm, granularity_xmm
	; PERF
	TSC.SAMPLE tsc_nodes, start
align_nops
.next_id:
	cmp id_p, id_p_finish
		je .epilog
	inc qword [nodes.n]
;---------------------------------------------------------------------------------------------------
	; no-branch vector zigzag decoding on sse (no avx2 coze our dev laptops)
	; encoded mod 2 == 1 : < 0  : -(encoded/2) - 1 = decoded
	; encoded mod 2 == 0 ; >= 0 : encoded/2 = decoded
if ~SYSTEM_LEGACY
	varint_load r11, lat_p
	pinsrq xmm5, r11, 1 ; lat_lon int64/int64
	varint_load r11, lon_p
	pinsrq xmm5, r11, 0 ; lat_lon int64/int64
else
	varint_load r11, lat_p
	movq xmm7, r11
	varint_load r11, lon_p
	movq xmm5, r11 ; lat_lon int64/int64
	punpcklqdq xmm5, xmm7 ;  = lat_lon int64/int64
end if
	movdqa xmm7, xmm5
	psrlq xmm5, 1 ; i >> 1,  i = lat_lon int64/int64
	pand xmm7, one_one ; i & 1, i = lat_lon int64/int64
	xorps xmm6, xmm6 ; = 0
	psubq xmm6, xmm7 ; 0 - (i & 1) = - (i & 1) 
	pxor xmm5, xmm6 ; (i >> 1) ^ -(i & 1) = DELTA GEO OF granularity units for lat_lon int64/int64
	paddq prev_lat_prev_lon, xmm5 ; = PREV GEO OF = GEO OF granularity units
;---------------------------------------------------------------------------------------------------
	; we can switch now from int64/int64 to f64/f64 since we finished decoding GEO coords

	; lon granularity units int64->f64 -- START
	movq rax, prev_lat_prev_lon
	cvtsi2sd xmm7, rax
	; lon granularity units int64->f64 -- END

	; lat granularity units int64->f64 -- START
if ~SYSTEM_LEGACY
	pextrq rax, prev_lat_prev_lon, 1 ;  = lat granularity units
else
	movdqa xmm6, prev_lat_prev_lon
	punpckhqdq xmm6, xmm6 ; DESTRUCTIVE:duplicate xmm high dq in xmm low dq, namely lat
	movq rax, xmm6 ; = lat granularity units
end if
	cvtsi2sd xmm6, rax
	; lat granularity units int64->f64 -- END

	unpcklpd xmm7, xmm6 ; = lat_lon f64/f64

	mulpd xmm7, granularity_xmm ; lat_lon granularity units -> lat_lon nanodegrees f64/f64
	addpd xmm7, lat_of_lon_of ; lat_lon full nanodegrees f64/f64

	; cannot use non-temporal store here due to mis-alignment
	movupd xword [node_p + nodes.node_t.geo], xmm7 ; little endian storage
;---------------------------------------------------------------------------------------------------
	mov node_start_p, node_p ; save the start of the node since are going to modify node_p

	test keys_vals_p, keys_vals_p
		jnz keys_vals_cpy ; unlikely because (key/val)s are more likely on ways/relations
	; insert an empty key/terminator while updating node_p
	add node_p, nodes.node_t.keys_vals + 1 ; room for the 0 terminator (= empty key)
	mov byte [node_p - 1], 0 ; insert the 0 terminator
align_nops
.node_next_compute:
	; here, node_p points of the finish byte, aka the next node
	mov qword [node_start_p + nodes.node_t.next], node_p
	mov [nodes.finish], node_p ; save the finish/next node
;---------------------------------------------------------------------------------------------------
	; decode the id
	varint_load r11, id_p ; load/skip the raw zigzag delta_id
	mov rax, r11 
	shr r11, 1 ; i >> 1
	and rax, 1 ; i & 1
	neg rax ; -(i & 1)
	xor r11, rax ; (i >> 1) ^ -(i & 1)  ; = delta_id
	add prev_id, r11 ; = prev_id = id
;{{{ idx_insert_node -------------------------------------------------------------------------------
; scratch ~ rax xmm7 xmm6
; from primgrp
;define flags			r15
;define granularity		rbp ; f64
;define strtbl_idx_p		r8
;define lat_of_lon_of		xmm3	; f64/f64
; local from parse:
;define id_p			r12
;define id_p_finish		r14
;define lat_p			r10
;define lon_p			rdx
;define keys_vals_p		rsi	; can be 0
; local from unserialize
;define node_p			rbx
;define prev_id			r13	; = id
;define node_start_p		rdi
;define granularity_xmm		xmm2
;define prev_lat_prev_lon	xmm1
;define one_one			xmm0
;local
define id			prev_id
define idx_finish_p		r9
define id_bit_idx		keys_vals_p	; spill
define bit_val_select		rcx
define bit_val_select_b		cl
define id_bit_idx_msb		lon_p		; spill
define idx_slot_p		r11
idx_insert_node:
	mov idx_slot_p, [nodes.idx]
	mov idx_finish_p, [nodes.idx.finish]
	movq xmm7, id_bit_idx_msb	; spill
	movq xmm6, id_bit_idx		; spill
	TRACE_NODE_IDX 'id=%#016lx nodes.idx=%p nodes.finish=%p', id, idx_slot_p, idx_finish_p
	xor id_bit_idx, id_bit_idx
	xor bit_val_select, bit_val_select
	mov id_bit_idx_msb, -1 ; this is to make it work with id = 0
	bsr id_bit_idx_msb, id ; if id = 0, id_bit_idx_msb is untouched namely -1
	TRACE_NODE_IDX 'id_bit_idx_msb=%lu', id_bit_idx_msb
align_nops
.next_id_bit:
	TRACE_NODE_IDX 'id_bit_idx=%lu', id_bit_idx
	cmp id_bit_idx_msb, id_bit_idx
		jb .insert_here
	bt id, id_bit_idx
	setc bit_val_select_b ; bit_val_select = 1 if id[id_bit_idx] = 1, else 0
	TRACE_NODE_IDX 'bit_val_select=%lu', bit_val_select
	mov rax, [idx_slot_p + 8 * bit_val_select]
	TRACE_NODE_IDX 'slot=%p bit slot=%p', idx_slot_p, rax
	test rax, rax
		jnz .idx_existing_slot
	TRACE_NODE_IDX 'non existing slot using finish=%p', idx_finish_p
	mov rax, idx_finish_p
	add idx_finish_p, nodes.idx.slot_t.bytes_n ; we could zero the mem here, but mmap does it for us
if ~SYSTEM_LEGACY
	prefetchw [idx_finish_p +  64 * 2] ; try to get ready
	prefetchw [idx_finish_p + 64 * 3]
end if
	mov [idx_slot_p + 8 * bit_val_select], rax
align_nops
.idx_existing_slot:
	mov idx_slot_p, rax
	TRACE_NODE_IDX 'next_slot=%p', idx_slot_p
	inc id_bit_idx

	jmp .next_id_bit
align_nops
.insert_here:
if TRACE_NODE_IDX_ENABLED
	mov rax, [idx_slot_p + nodes.idx.slot_t.node]
	test rax, rax
		jz .node_slot_available
	TRACE_NODE_IDX 'WARNING: overwritting an existing node=%p', rax
.node_slot_available:
	TRACE_NODE_IDX 'inserting node=%p in slot=%p', node_start_p, idx_slot_p
end if
	mov [idx_slot_p + nodes.idx.slot_t.node], node_start_p
	mov [nodes.idx.finish], idx_finish_p
	movq id_bit_idx_msb,	xmm7 ; unspill
	movq id_bit_idx,	xmm6 ; unspill
purge idx_slot_p
purge id_bit_idx_msb
purge bit_val_select_b
purge bit_val_select
purge id_bit_idx
purge idx_finish_p
purge id
;---------------------------------------------------------------------------------------------------
	; XXX: if one day we need to be backed by a file we will need to DONT_NEED "madvise" the
	; the current node memory
	jmp unserialize.next_id
;}}} idx_insert_node -------------------------------------------------------------------------------
align_nops
unserialize.epilog:
	; PERF -- START
	TSC.SAMPLE tsc_nodes, stop
	TSC.PRINT tsc_nodes
	; PERF -- STOP
	jmp primgrp.parse.return_from_dense
;}}} unserialize -----------------------------------------------------------------------------------
;{{{ keys_vals_cpy(unlikely) ----------------------------------------------------------------------
; scratch ~ rax rcx r11 r9 xmm7 xmm6 xmm5 xmm4
; from primgrp
;define flags			r15
;define granularity		rbp ; f64
;define strtbl_idx_p		r8
;define lat_of_lon_of		xmm3 ; f64/f64
; local from parse:
;define id_p			r12
;define id_p_finish		r14
;define lat_p			r10
;define lon_p			rdx
;define keys_vals_p		rsi	; can be 0
; local from unserialize
;define node_p			rbx
;define prev_id			r13
;define node_start_p		rdi
;define granularity_xmm		xmm2
;define prev_lat_prev_lon	xmm1
;define one_one			xmm0
align_nops
keys_vals_cpy:
	add node_p, nodes.node_t.keys_vals ; = key_dst_addr
align_nops
.next_key_val:
	varint_load r11, keys_vals_p ; load/skip the int32 key_id/end marker
	test r11, r11 ; int32 key_id == 0 ?
		jz .epilog ; no more key=val for this node
	mov r11, [strtbl_idx_p + 8 * r11] ; = key_src_addr
	varint_load r9, r11 ; r9 = key_bytes_n, r11 = key_src_addr
	mov [node_p], r9b ; store the byte of key_bytes_n at the start of the str
	inc node_p
	; memcpy_dst = node_p(rbx), memcpy_src = key_src_addr(r11), memcpy_bytes_n = key_bytes_n(r9)
	lea memcpy_link, [.key_cpy_done] ; where memcpy will jmp back
	jmp memcpy ; node_p = node_p + key_bytes_n
align_nops
.key_cpy_done:
	; node_p = val_dst_addr
	varint_load r11, keys_vals_p ; load/skip the int32 val_id
	mov r11, [strtbl_idx_p + 8 * r11] ; = val_src_addr
	varint_load r9, r11; r9 = val_bytes_n, r11 = val_src_addr
	mov [node_p], r9w ; store the short of val_bytes_n at the start of the str
	add node_p, 2
	; memcpy_dst = node_p(rbx), memcpy_src = val_src_addr(r11), memcpy_bytes_n = val_bytes_n(r9)
	lea memcpy_link, [.next_key_val] ; where memcpy will jmp back
	jmp memcpy ; node_p = node_p + val_bytes_n
align_nops
.epilog:
	mov byte [node_p], 0 ; insert the 0-sized key terminator
	inc node_p ; node_p now points on the next node, aka current node finish byte
	jmp unserialize.node_next_compute
purge one_one
purge prev_lat_prev_lon
purge granularity_xmm
purge node_start_p
purge prev_id
purge node_p
purge keys_vals_p
purge lon_p
purge lat_p
purge id_p_finish
purge id_p
purge lat_of_lon_of
purge strtbl_idx_p
purge strtbl_idx_p
purge granularity
purge flags
;}}} keys_vals_cpy ---------------------------------------------------------------------------------
;{{{ local memcpy(sse) -----------------------------------------------------------------------------
; XXX: lddqu performs aligned reads, carefull where you use this, but should be fine with classic
; paginated memory.
; scratch ~ rax xmm7 xmm6 xmm5 xmm4
align_nops
memcpy:
	cmp memcpy_bytes_n, 16 * 4
		jb .below64
	; since we can be cache line mis-aligned, speculatively do prefetch 2 cls ahead
	prefetchnta [memcpy_src + 64 * 2]  ; speculative non-temporal "3rd" cache line ahead
	prefetchnta [memcpy_src + 64 * 3]  ; speculative non-temporal "4th" cache line ahead
if ~SYSTEM_LEGACY
	prefetchw [memcpy_dst + 64 * 2]
	prefetchw [memcpy_dst + 64 * 3]
end if
	lddqu xmm7, [memcpy_src + 16 * 0]
	lddqu xmm6, [memcpy_src + 16 * 1]
	lddqu xmm5, [memcpy_src + 16 * 2]
	lddqu xmm4, [memcpy_src + 16 * 3]

	movdqu [memcpy_dst + 16 * 0], xmm7
	movdqu [memcpy_dst + 16 * 1], xmm6
	movdqu [memcpy_dst + 16 * 2], xmm5
	movdqu [memcpy_dst + 16 * 3], xmm4

	add memcpy_dst, 16 * 4
	sub memcpy_bytes_n, 16 * 4
		jz .done
	add memcpy_src, 16 * 4
	jmp memcpy
align_nops
.below64:
	cmp memcpy_bytes_n, 16 * 3
		jb .below48

	lddqu xmm7, [memcpy_src + 16 * 0]
	lddqu xmm6, [memcpy_src + 16 * 1]
	lddqu xmm5, [memcpy_src + 16 * 2]

	movdqu [memcpy_dst + 16 * 0], xmm7
	movdqu [memcpy_dst + 16 * 1], xmm6
	movdqu [memcpy_dst + 16 * 2], xmm5

	add memcpy_dst, 16 * 3
	sub memcpy_bytes_n, 16 * 3
		jz .done
	add memcpy_src, 16 * 3
	; fall-thru
align_nops
.below48:
	cmp memcpy_bytes_n, 16 * 2
		jb .below32

	lddqu xmm7, [memcpy_src + 16 * 0]
	lddqu xmm6, [memcpy_src + 16 * 1]

	movdqu [memcpy_dst + 16 * 0], xmm7
	movdqu [memcpy_dst + 16 * 1], xmm6

	add memcpy_dst, 16 * 2
	sub memcpy_bytes_n, 16 * 2
		jz .done
	add memcpy_src, 16 * 2
	; fall-thru
align_nops
.below32:
	cmp memcpy_bytes_n, 16 * 1 
		jb .below16

	lddqu xmm7, [memcpy_src + 16 * 0]
	movdqu [memcpy_dst + 16 * 0], xmm7
	
	add memcpy_dst, 16 * 1
	sub memcpy_bytes_n, 16 * 1
		jz .done
	add memcpy_src, 16 * 1
	; fall-thru
align_nops
.below16:
	cmp memcpy_bytes_n, 8 * 1 
		jb .below8
	mov rax, [memcpy_src + 8 * 0]
	mov [memcpy_dst + 8 * 0], rax

	add memcpy_dst, 8 * 1
	sub memcpy_bytes_n, 8 * 1
		jz .done
	add memcpy_src, 8 * 1
	; fall-thru
align_nops
.below8:
	cmp memcpy_bytes_n, 7
		je .cpy7
	cmp memcpy_bytes_n, 6
		je .cpy6
	cmp memcpy_bytes_n, 5
		je .cpy5
	cmp memcpy_bytes_n, 4
		je .cpy4
	cmp memcpy_bytes_n, 3
		je .cpy3
	cmp memcpy_bytes_n, 2
		je .cpy2
	cmp memcpy_bytes_n, 1
		je .cpy1
	jmp memcpy_link
align_nops
.cpy7:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy6:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy5:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy4:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy3:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy2:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy1:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.done:
	jmp memcpy_link
;}}} local memcpy(sse) -----------------------------------------------------------------------------
;===================================================================================================
purge memcpy_dst
purge memcpy_src
purge memcpy_bytes_n
purge memcpy_link
;{{{ macros ----------------------------------------------------------------------------------------
if TRACE_NODE_IDX_ENABLED
	macro TRACE_NODE_IDX fmt, regs&
		TRACE_PREFIX 'NODE_IDX', fmt, regs
	end macro
else
	macro TRACE_NODE_IDX fmt, regs&
	end macro
end if
;}}} macros ----------------------------------------------------------------------------------------
end namespace ; dense
