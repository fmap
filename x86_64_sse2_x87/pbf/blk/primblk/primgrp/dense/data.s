; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace blk.primblk.primgrp.dense.nodes
	msg: namespace msg
		mmap_failed	db 'ERROR(%ld):PBF:PRIMBLK:PRIMGRP:DENSE:NODES:unable to mmap nodes',10,0
		idx_mmap_failed	db 'ERROR(%ld):PBF:PRIMBLK:PRIMGRP:DENSE:NODES:IDX:unable to mmap the index',10,0
		idx_nodes_n	db 'PBF:BLK:PRIMBLK:DENSE:INIT_ONCE:reserving a memory mapping for an index able to handle %lu nodes',10,0
	end namespace
end namespace
