; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace blk.primblk.primgrp.ways
align 64
array			rq 1
array.finish		rq 1 ; finish way, inited to above array
n			rq 1 ; how many ways in this pbf
array.idx		rq 1 ; binary tree, bits from the way id
array.idx.finish	rq 1 ; next available idx slot
namespace array ;{{{ -------------------------------------------------------------------------------
	bytes_n = 16 * 1024 * 1024 * 1024 ; 16GiB
	way_t: namespace way_t ;{{{ ----------------------------------------------------------------
		next		:= 0 ; qw, ptr on next way (lookup for way-only features)
		nodes		:= 8 ; qw ptr on the array of ptrs of nodes after the (key/val)s
		; (key/val)s:
		; keys start with a byte containing their sz in bytes
		; vals start with a short containing their sz in bytes
		; the array is terminated with a 0-szed key
		keys_vals	:= 16
		; nodes: following are refs as ptrs of nodes, pointed by the above nodes and
		;        finished by the way next ptr
	end namespace ; }}} way_t ------------------------------------------------------------------
end namespace ;}}} array ---------------------------------------------------------------------------
namespace array.idx ;{{{ ---------------------------------------------------------------------------
	slot_t: namespace slot_t
		bit0	:= 0	; qw, ptr on idx slot for this bit
		bit1	:= 8	; qw, idem
		way	:= 16	; qw, ptr in the array for this id
		bytes_n	:= 24
	end namespace
	n = 1000 * 1000 * 1000 ; 1 billion ways
	bytes_n = n * slot_t.bytes_n ; 24GB
end namespace ;}}} array.idx -----------------------------------------------------------------------
end namespace
