namespace blk.primblk.primgrp.ways
	msg: namespace msg
		mmap_failed	db 'ERROR(%ld):PBF:PRIMBLK:PRIMGRP:WAYS:NODES:unable to mmap ways',10,0
		idx_mmap_failed	db 'ERROR(%ld):PBF:PRIMBLK:PRIMGRP:WAYS:NODES:IDX:unable to mmap the index',10,0
		idx_ways_n	db 'PBF:BLK:PRIMBLK:WAYS:INIT_ONCE:reserving a memory mapping for an index able to handle %lu ways',10,0
	end namespace
end namespace
