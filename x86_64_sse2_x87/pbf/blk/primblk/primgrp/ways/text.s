; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
; XXX: We actually used this code unit as a training ground. Expect it to be very overkill.
ways: namespace ways
;{{{ init_once -------------------------------------------------------------------------------------
align_nops
init_once:
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:WAYS:INIT_ONCE'
	mov rbp, rsp
	and rsp, not 0xf

	xor edi, edi
	mov rsi, array.bytes_n
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae .err_ways_mmap_failed
	mov [array], rax
	mov [array.finish], rax

	lea rdi, [msg.idx_ways_n]
	mov rsi, array.idx.n
	call qword [libc.printf]

	xor edi, edi
	mov rsi, array.idx.bytes_n
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae .err_ways_idx_mmap_failed
	mov [array.idx], rax
	add rax, 8 * 3
	mov [array.idx.finish], rax ; next availabe idx slot

	stc ; return true
	mov rsp, rbp
	ret
.err_ways_mmap_failed:
	mov rdi, 2 ; stderr
	mov rsi, rax ; errno
	lea rsi, [msg.mmap_failed]
	call qword [libc.dprintf]
	clc ; return false
	mov rsp, rbp
	ret
.err_ways_idx_mmap_failed:
	mov rdi, 2 ; stderr
	mov rsi, rax ; errno
	lea rsi, [msg.idx_mmap_failed]
	call qword [libc.dprintf]
	clc ; return false
	mov rsp, rbp
	ret
;}}} init_once -------------------------------------------------------------------------------------
; local memcpy ABI
define memcpy_dst		rbx	; memcpy_dst(out) = memcpy_dst(in) + memcpy_bytes_n
define memcpy_src		r11	; clobbered, don't care
define memcpy_bytes_n		r9	; clobbered, don't care
define memcpy_link		rcx	; the return addr
;===================================================================================================
;{{{ parse -----------------------------------------------------------------------------------------
; scratch ~ rax rcx r11 r9
; from primgrp
define ways_p		rbx	; in
define ways_p_finish 	r13	; in
define flags		r15	; in/out
define strtbl_idx_p	r8
define lat_of_lon_of	xmm3 	; f64/f64
; local
define id		r12
define keys_p		r14	; can be 0
define keys_p_d		r14d	; can be 0
define keys_p_finish	rbp
define vals_p		r10	; num of vals is num of keys
define refs_p		rdx	; presume it can be 0
define refs_p_d		edx 	; presume it can be 0
define refs_finish_p	rsi
align_nops
parse:
	xor keys_p_d, keys_p_d ; if no (key/val)
	xor refs_p_d, refs_p_d ; presume we can have no refs (should not happen though)
align_nops
.next_msg_key:
	; "wayS" is plural because of the primgrp field name, but there is actually only one "way"
	; which will be repeated
	cmp ways_p, ways_p_finish
		je unserialize
	varint_load r11, ways_p ; msg key
	mov r9, r11 ; = msg key copy
	shr r11, 3 ; = field num
	cmp r11, 1
		je .id_found
	cmp r11, 2
		je .keys_found
	cmp r11, 3
		je .vals_found
	cmp r11, 8
		je .refs_found
	and r9, 111b ; field type
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:WAYS:MSG:FIELD %lu OF TYPE %lu', r11, r9
	val_skip ways_p, r9, r11
	jmp .next_msg_key
align_nops
.id_found:
	varint_load id, ways_p
	jmp .next_msg_key
align_nops
.keys_found:
	varint_load r11, ways_p ; keys_sz
	mov keys_p, ways_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:WAYS:KEYS:start = %p:size = %lu(0x%lx) bytes', keys_p, r11, r11
	add ways_p, r11
	mov keys_p_finish, ways_p
	jmp .next_msg_key
align_nops
.vals_found:
	varint_load r11, ways_p ; vals_sz
	mov vals_p, ways_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:WAYS:VALS:start = %p:size = %lu(0x%lx) bytes', vals_p, r11, r11
	add ways_p, r11
	jmp .next_msg_key
align_nops
.refs_found:
	varint_load r11, ways_p ; refs_sz
	mov refs_p, ways_p
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:WAYS:REFS:start = %p:size = %lu(0x%lx) bytes', refs_p, r11, r11
	add ways_p, r11
	mov refs_finish_p, ways_p
	jmp .next_msg_key
purge ways_p_finish
purge ways_p
;}}} parse -----------------------------------------------------------------------------------------
;{{{ unserialize -----------------------------------------------------------------------------------
; scratch ~ rax rcx r11 r9
; from primgrp
;define flags		r15	; in/out
;define strtbl_idx_p	r8
;define lat_of_lon_of	xmm3 	; f64/f64
; local from parse
;define id		r12
;define keys_p		r14	; can be 0
;define keys_p_d	r14d	; can be 0
;define keys_p_finish	rbp
;define vals_p		r10	; num of vals is num of keys
;define refs_p		rdx	; presume it can be 0
;define refs_p_d	edx 	; presume it can be 0
;define refs_finish_p	rsi
; local
define way_p			rbx
define way_start_p		r13
align_nops
unserialize:
	mov way_p, [array.finish]
	mov way_start_p, way_p ; save the start of the way since are going to modify way_p

;{{{ keys_vals_cpy ----------------------------------------------------------------------------------
keys_vals_cpy:
	add way_p, array.way_t.keys_vals ; way_p points on the start of the keys_vals section
	test keys_p, keys_p
		jz unserialize.keys_vals_done
	jmp .entry
align_nops
.next_key_val:
	cmp keys_p, keys_p_finish
		je unserialize.keys_vals_done
.entry:
	varint_load r11, keys_p ; = key_id, keys_p points on next key_id
	mov r11, [strtbl_idx_p + 8 * r11] ; = key_src_addr
	varint_load r9, r11 ; r9 = key_bytes_n, r11 = key_src_addr
	mov [way_p], r9b ; insert the byte containing the sz of the key str in bytes
	inc way_p
	; memcpy_dst = way_p(rbx), memcpy_src = key_src_addr(r11), memcpy_bytes_n = key_bytes_n(r9)
	lea memcpy_link, [.key_cpy_done] ; where memcpy will jmp back
	jmp memcpy ; way_p = way_p + key_bytes_n
align_nops
.key_cpy_done:
	varint_load r11, vals_p ; = val_id, vals_p points on next val_id
	mov r11, [strtbl_idx_p + 8 * r11] ; = val_src_addr
	varint_load r9, r11; r9 = val_bytes_n, r11 = val_src_addr
	mov [way_p], r9w ; insert the short containing the sz of the val str in bytes
	add way_p, 2
	; memcpy_dst = way_p(rbx), memcpy_src = val_src_addr(r11), memcpy_bytes_n = val_bytes_n(r9)
	lea memcpy_link, [.next_key_val] ; where memcpy will jmp back
	jmp memcpy ; way_p = way_p + val_bytes_n
;}}} keys_val_cpy ----------------------------------------------------------------------------------
align_nops
unserialize.keys_vals_done:
	mov byte [way_p], 0 ; insert the 0-szed key/terminator
	inc way_p ; here way_p points on the start of the section of ptrs on nodes
	mov [way_start_p + array.way_t.nodes], way_p
	test refs_p, refs_p
		jz unserialize.refs_solved ; unlikely
purge vals_p
purge keys_p_finish
purge keys_p_d
purge keys_p
;{{{ refs_solve ------------------------------------------------------------------------------------
; XXX: it is expected to have missing nodes in ways-->do 0 their ptrs
; scratch ~ rax rcx r11
; from primgrp
;define flags			r15	; in/out
;define strtbl_idx_p		r8
;define lat_of_lon_of		xmm3 	; f64/f64
; local from parse
;define id			r12
;define refs_p			rdx	; presume it can be 0
;define refs_p_d		edx 	; presume it can be 0
;define refs_finish_p		rsi
; local from unserialize
;define way_p			rbx
;define way_start_p		r13
; local
define prev_ref	 		r14
define prev_ref_d		r14d
define ref			rbp
define bit_val_select		r10
define bit_val_select_b 	r10b
define ref_bit_idx_msb		rcx
define ref_bit_idx_msb_d	ecx
define idx_slot_p		r9
refs_solve:
	xor prev_ref_d, prev_ref_d ; prev_ref = 0
align_nops
.next_ref:
	; decode the id ----------------------------------------------------------------------------
	varint_load r11, refs_p ; load/skip the raw zigzag delta_refs
	mov rax, r11 
	shr r11, 1 ; i >> 1
	and rax, 1 ; i & 1
	neg rax ; -(i & 1)
	xor r11, rax ; (i >> 1) ^ -(i & 1)  ; = delta_id
	add prev_ref, r11 ; = prev_ref = ref
	mov ref, prev_ref
	; lookup the node ptr in the nodes idx -----------------------------------------------------
	xor bit_val_select, bit_val_select ; bit_val_select = 0
	mov idx_slot_p, [primgrp.dense.nodes.idx]
	mov ref_bit_idx_msb, -1 ; this is to make it work with ref = 0
	bsr ref_bit_idx_msb, ref ; if ref = 0, ref_bit_idx_msb is untouched namely -1
	inc ref_bit_idx_msb_d ; store the idx + 1, 32bits because of the following jecxz
align_nops
.next_ref_bit:
	jecxz .idx_slot_lookup_done ; bit_idx + 1 is stored in cx
	bt ref, 0
	setc bit_val_select_b
	mov idx_slot_p, [idx_slot_p + 8 * bit_val_select]
	test idx_slot_p, idx_slot_p ; if the idx slot in 0, we have a missing node
		jz .missing_slot_node ; unlikely
if ~SYSTEM_LEGACY
	prefetcht0 [idx_slot_p]
end if
	shr ref, 1
	dec ref_bit_idx_msb_d
	jmp .next_ref_bit
.missing_slot_node: ; unlikely
	mov qword [way_p], 0
	jmp .next_way_node_ptr
align_nops
.idx_slot_lookup_done:	
	mov rax, [idx_slot_p + dense.nodes.idx.slot_t.node] ; XXX: can be 0
	mov [way_p], rax
.next_way_node_ptr: ; unlikely
	add way_p, 8 ; points on way storage for next node ptr
	cmp refs_p, refs_finish_p
		jne .next_ref
purge idx_slot_p
purge ref_bit_idx_msb_d
purge ref_bit_idx_msb
purge bit_val_select_b
purge bit_val_select
purge ref
purge prev_ref_d
purge prev_ref
;}}} refs_solve ------------------------------------------------------------------------------------
unserialize.refs_solved: ; unlikely don't align_nops
	; here way_p points on the start of the next/finish way
	mov [way_start_p + array.way_t.next], way_p
	mov [array.finish], way_p
purge way_p
;{{{ idx_insert_node -------------------------------------------------------------------------------
; scratch ~ rax
; from primgrp
;define flags		r15	; in/out
;define strtbl_idx_p	r8
;define lat_of_lon_of	xmm3 	; f64/f64
; local from parse
;define id		r12
;define refs_p		rdx	; presume it can be 0
;define refs_p_d	edx 	; presume it can be 0
;define refs_finish_p	rsi
; local from unserialize
;define way_start_p	r13
; local
define idx_finish_p	rbx
define id_bit_idx	r14
define id_bit_idx_d	r14d
define bit_val_select	rcx ; we use rcx here because we may use rcx compressed instructions
define bit_val_select_d	ecx
define bit_val_select_b	cl
define id_bit_idx_msb	rbp
define idx_slot_p	r9
idx_insert_node:
	mov idx_slot_p, [array.idx]
	mov idx_finish_p, [array.idx.finish]
	TRACE_WAYS_IDX 'id=%#016lx nodes.idx=%p nodes.finish=%p', id, idx_slot_p, idx_finish_p
	xor id_bit_idx_d, id_bit_idx_d
	xor bit_val_select_d, bit_val_select_d
	mov id_bit_idx_msb, -1 ; this is to make it work with id = 0
	bsr id_bit_idx_msb, id ; if id = 0, id_bit_idx_msb is untouched namely -1
	TRACE_WAYS_IDX 'id_bit_idx_msb=%lu', id_bit_idx_msb
align_nops
.next_id_bit:
	TRACE_WAYS_IDX 'id_bit_idx=%lu', id_bit_idx
	cmp id_bit_idx_msb, id_bit_idx
		jb .insert_here
	bt id, id_bit_idx
	setc bit_val_select_b ; bit_val_select = 1 if id[id_bit_idx] = 1, else 0
	TRACE_WAYS_IDX 'bit_val_select=%lu', bit_val_select
	mov rax, [idx_slot_p + 8 * bit_val_select]
	TRACE_WAYS_IDX 'slot=%p bit slot=%p', idx_slot_p, rax
	test rax, rax
		jnz .idx_existing_slot
	TRACE_WAYS_IDX 'non existing slot using finish=%p', idx_finish_p
	mov rax, idx_finish_p
	add idx_finish_p, array.idx.slot_t.bytes_n ; we could zero the mem here, but mmap does it for us
if ~SYSTEM_LEGACY
	prefetchw [idx_finish_p +  64 * 2]	; try to get ready
	prefetchw [idx_finish_p + 64 * 3]
end if
	mov [idx_slot_p + 8 * bit_val_select], rax
align_nops
.idx_existing_slot:
	mov idx_slot_p, rax
	TRACE_WAYS_IDX 'next_slot=%p', idx_slot_p
	inc id_bit_idx

	jmp .next_id_bit
align_nops
.insert_here:
if TRACE_WAYS_IDX_ENABLED
	mov rax, [idx_slot_p + array.idx.slot_t.way]
	test rax, rax
		jz .way_slot_available
	TRACE_WAYS_IDX 'WARNING: overwritting an existing way=%p', rax
.way_slot_available:
	TRACE_WAYS_IDX 'inserting way=%p in slot=%p', way_start_p, idx_slot_p
end if
	mov [idx_slot_p + array.idx.slot_t.way], way_start_p
	mov [array.idx.finish], idx_finish_p
	; XXX: if one day we need to be backed by a file we will need to DONT_NEED "madvise" the
	; the current way memory
purge idx_slot_p
purge id_bit_idx_msb
purge bit_val_select_b
purge bit_val_select_d
purge bit_val_select
purge id_bit_idx_d
purge id_bit_idx
purge idx_finish_p
;}}} idx_insert_node -------------------------------------------------------------------------------
	jmp primgrp.parse.return_from_ways
purge way_start_p
purge refs_finish_p
purge refs_p_d
purge refs_p
purge lat_of_lon_of
purge strtbl_idx_p
purge id
purge flags
;}}} unserialize -----------------------------------------------------------------------------------
;{{{ local memcpy(sse) -----------------------------------------------------------------------------
; XXX: lddqu performs aligned reads, carefull where you use this, but should be fine with classic
; paginated memory.
; scratch ~ rax xmm7 xmm6 xmm5 xmm4
align_nops
memcpy:
	cmp memcpy_bytes_n, 16 * 4
		jb .below64
	; since we can be cache line mis-aligned, speculatively do prefetch 2 cls ahead
	prefetchnta [memcpy_src + 64 * 2]  ; speculative non-temporal "3rd" cache line ahead
	prefetchnta [memcpy_src + 64 * 3]  ; speculative non-temporal "4th" cache line ahead
if ~SYSTEM_LEGACY
	prefetchw [memcpy_dst + 64 * 2]
	prefetchw [memcpy_dst + 64 * 3]
end if
	lddqu xmm7, [memcpy_src + 16 * 0]
	lddqu xmm6, [memcpy_src + 16 * 1]
	lddqu xmm5, [memcpy_src + 16 * 2]
	lddqu xmm4, [memcpy_src + 16 * 3]

	movdqu [memcpy_dst + 16 * 0], xmm7
	movdqu [memcpy_dst + 16 * 1], xmm6
	movdqu [memcpy_dst + 16 * 2], xmm5
	movdqu [memcpy_dst + 16 * 3], xmm4

	add memcpy_dst, 16 * 4
	sub memcpy_bytes_n, 16 * 4
		jz .done
	add memcpy_src, 16 * 4
	jmp memcpy
align_nops
.below64:
	cmp memcpy_bytes_n, 16 * 3
		jb .below48

	lddqu xmm7, [memcpy_src + 16 * 0]
	lddqu xmm6, [memcpy_src + 16 * 1]
	lddqu xmm5, [memcpy_src + 16 * 2]

	movdqu [memcpy_dst + 16 * 0], xmm7
	movdqu [memcpy_dst + 16 * 1], xmm6
	movdqu [memcpy_dst + 16 * 2], xmm5

	add memcpy_dst, 16 * 3
	sub memcpy_bytes_n, 16 * 3
		jz .done
	add memcpy_src, 16 * 3
	; fall-thru
align_nops
.below48:
	cmp memcpy_bytes_n, 16 * 2
		jb .below32

	lddqu xmm7, [memcpy_src + 16 * 0]
	lddqu xmm6, [memcpy_src + 16 * 1]

	movdqu [memcpy_dst + 16 * 0], xmm7
	movdqu [memcpy_dst + 16 * 1], xmm6

	add memcpy_dst, 16 * 2
	sub memcpy_bytes_n, 16 * 2
		jz .done
	add memcpy_src, 16 * 2
	; fall-thru
align_nops
.below32:
	cmp memcpy_bytes_n, 16 * 1 
		jb .below16

	lddqu xmm7, [memcpy_src + 16 * 0]
	movdqu [memcpy_dst + 16 * 0], xmm7
	
	add memcpy_dst, 16 * 1
	sub memcpy_bytes_n, 16 * 1
		jz .done
	add memcpy_src, 16 * 1
	; fall-thru
align_nops
.below16:
	cmp memcpy_bytes_n, 8 * 1 
		jb .below8
	mov rax, [memcpy_src + 8 * 0]
	mov [memcpy_dst + 8 * 0], rax

	add memcpy_dst, 8 * 1
	sub memcpy_bytes_n, 8 * 1
		jz .done
	add memcpy_src, 8 * 1
	; fall-thru
align_nops
.below8:
	cmp memcpy_bytes_n, 7
		je .cpy7
	cmp memcpy_bytes_n, 6
		je .cpy6
	cmp memcpy_bytes_n, 5
		je .cpy5
	cmp memcpy_bytes_n, 4
		je .cpy4
	cmp memcpy_bytes_n, 3
		je .cpy3
	cmp memcpy_bytes_n, 2
		je .cpy2
	cmp memcpy_bytes_n, 1
		je .cpy1
	jmp memcpy_link
align_nops
.cpy7:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy6:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy5:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy4:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy3:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy2:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.cpy1:
	mov al, [memcpy_src]
	mov [memcpy_dst], al
	inc memcpy_src
	inc memcpy_dst
align_nops
.done:
	jmp memcpy_link
;}}} local memcpy(sse) -----------------------------------------------------------------------------
;===================================================================================================
purge memcpy_link
purge memcpy_bytes_n
purge memcpy_src
purge memcpy_dst
;{{{ macros ----------------------------------------------------------------------------------------
if TRACE_WAYS_IDX_ENABLED
	macro TRACE_WAYS_IDX fmt, regs&
		TRACE_PREFIX 'WAYS_IDX', fmt, regs
	end macro
else
	macro TRACE_WAYS_IDX fmt, regs&
	end macro
end if
;}}} macros ----------------------------------------------------------------------------------------
end namespace ; ways
