; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
primblk: namespace primblk
align_nops
init_once:
	TRACE 'PBF:BLK:PRIMBLK:INIT_ONCE'
	call strtbl.init_once
		jnc .nok
	call primgrp.init_once
		jnc .nok
	stc ; return true
	ret
.nok:
	clc ; return false
	ret
;---------------------------------------------------------------------------------------------------
; scratch: rax rcx r11 r9 xmm7 xmm6 xmm5 xmm4
; from blk
define p		rbx ; in
define p_finish		r12 ; in
define flags		r15 ; in/out
; local
define primgrp_p	r13
define primgrp_p_finish	r14
define granularity	rbp ; f64
define strtbl_idx_p	r8
define strtbl_p		r10 ; tmp
define strtbl_p_finish	rsi ; tmp 
define lat_of_lon_of	xmm3 ; f64/f64
align_nops
parse:
	mov strtbl_idx_p, [strtbl.idx]
	mov granularity, 100.0 ; f64 nanodegrees default, see PBF specs
	; defaults is [0.0,0.0] nanodegrees, _NOT_ granularity units, see PBF specs
	mov rax, 0.0
	movq lat_of_lon_of, rax
	punpcklqdq lat_of_lon_of, lat_of_lon_of
align_nops
.next_key:
	cmp p, p_finish
		je blob.hdr.parse.return_from_blk
	varint_load r11, p ; = key
	mov r9, r11 ; = copy key
	shr r11, 3 ; = field num
	and r9, 111b ; = field type
	cmp r11, 1
		je .strtbl_found
	cmp r11, 2
		je .primgrp_found
	cmp r11, 17
		je .granularity_found
	cmp r11, 19
		je .lat_of_found
	cmp r11, 20
		je .lon_of_found
	TRACE 'PBF:BLK:PRIMBLK:MSG:FIELD %lu OF TYPE %lu', r11, r9
	val_skip p, r9, r11
	jmp .next_key
align_nops
.strtbl_found:
	varint_load r11, p ; = strtbl_sz
	TRACE 'PBF:BLK:PRIMBLK:STRTBL:start = %p:size = %lu(0x%lx) bytes', p, r11, r11
	mov strtbl_p, p
	add p, r11
	mov strtbl_p_finish, p
	jmp strtbl.parse ; leaf code
align_nops
.primgrp_found:
	varint_load r11, p ; = primgrp_sz
	TRACE 'PBF:BLK:PRIMBLK:PRIMGRP:start = %p:size = %lu(0x%lx) bytes', p, r11, r11
	mov primgrp_p, p
	add p, r11
	mov primgrp_p_finish, p
	jmp primgrp.parse
align_nops
.granularity_found:
	varint_load granularity, p ; int64 granularity
	TRACE 'PBF:BLK:PRIMBLK:granularity=%lu nanodegrees', r11
	cvtsi2sd xmm7, granularity
	movq granularity, xmm7 ; f64 granularity
	jmp .next_key
align_nops
.lat_of_found:
	varint_load r11, p
	TRACE 'PBF:BLK:PRIMBLK:latitude offset=%lu nanodegrees', r11
	cvtsi2sd xmm7, r11
	punpcklqdq lat_of_lon_of, xmm7 ; = lat_lon f64/f64
	jmp .next_key
align_nops
.lon_of_found:
	varint_load r11, p
	TRACE 'PBF:BLK:PRIMBLK:longitude offset=%lu nanodegrees', r11
	cvtsi2sd lat_of_lon_of, r11
	jmp .next_key
;---------------------------------------------------------------------------------------------------
purge p
purge p_finish
purge flags
purge primgrp_p
purge primgrp_p_finish
purge granularity
purge strtbl_idx_p
purge strtbl_p
purge strtbl_p_finish
purge lat_of_lon_of
;===================================================================================================
include 'strtbl/text.s'
include 'primgrp/text.s'
;===================================================================================================
end namespace ; primblk
