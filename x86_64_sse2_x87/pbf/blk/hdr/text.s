; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
hdr: namespace hdr
; we don't follow specs: we will try to go best effort in blk_primitive parsing
; scratch: rax rcx r11 r9
; from blk
define p 		rbx ; in
define p_finish		r12 ; in
;define flags		r15 ; in/out
align_nops
parse:
.key_next:
	cmp p, p_finish
		je blob.hdr.parse.return_from_blk
	varint_load r11, p ; key
	mov r9, r11; = copy key
	shr r11, 3 ;  = field num
	; print strings we are aware of
	cmp r11, 4 ; field num = HeaderBlock.required_features (see specs) ?
		je .str_print
	cmp r11, 5 ; field num = HeaderBlock.optional_features (see specs) ?
		je .str_print
	cmp r11, 16 ; field num = HeaderBlock.writingprogram (see specs) ?
		je .str_print
	cmp r11, 17 ; field num = HeaderBlock.source (see specs) ?
		je .str_print
	cmp r11, 34 ; field num = HeaderBlock.osmosis_replication_base_url (see specs) ?
		je .str_print
	and r9, 111b ;  = field type
	TRACE 'PBF:BLK:HDR:MSG:FIELD %lu OF TYPE %lu', r11, r9
	val_skip p, r9, r11
	jmp .key_next
align_nops 
.str_print: 
	varint_load r9, p ; = str_sz
	TRACE 'PBF:BLK:HDR:MSG:STRING %lu of VALUE "%.*s"', r11, r9, p
	add p, r9 ; skip the str val
	jmp .key_next
;---------------------------------------------------------------------------------------------------
purge p
purge p_finish
;purge flags
end namespace ; hdr
