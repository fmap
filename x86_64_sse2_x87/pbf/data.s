; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace pbf 
msg: namespace msg
	tile_db_dir_openat_failed db 'PBF:failed to open %s tile database directory (errno=%ld)',10,0
	opening db 'PBF:openning %s pbf file',10,0
	open_failed db 'PBF:failed to open %s pbf file (errno=%ld)',10,0
	stat_failed db 'PBF:failed to stat %s pbf file (errno=%ld)',10,0
	mmap_failed db 'PBF:failed to mmap %s pbf file (errno=%ld)',10,0
	stat_sz db 'PBF:size of %s is %lu bytes',10,0
	mmap_addr db 'PBF:file mapped at %p',10,0
	ctx: namespace ctx
		start		db 'PBF:CTX:START',10,0
		general		db 'FILE_PATH=%s',10,\
				   'TILE_DATABASE_DIRECTOR_PATH=%s',10,\
				   'ZOOM=%lu',10,0
		bbox: namespace bbox
			swlat	db 'BOUNDING_BOX:SOUTH WEST:LATITUDE=%.f nanodegrees %s',10,0
			swlon	db 'BOUNDING_BOX:SOUTH WEST:LONGITUDE=%.f nanodegrees %s',10,0
			nelat	db 'BOUNDING_BOX:NORTH EATH:LATITUDE=%.f nanodegrees %s',10,0
			nelon	db 'BOUNDING_BOX:NORTH EAST:LONGITUDE=%.f nanodegrees %s',10,0
			user	db '(user provided)',0
			adjust	db '(ajdusted)',0
		end namespace
		finish		db 'PBF:CTX:END',10,0
	end namespace
end namespace
stats: namespace stats
	msg	db 'dense nodes count: %lu',10,'ways count: %lu',10,'relations count: %lu', 10,0
end namespace
;---------------------------------------------------------------------------------------------------
include 'blob/data.s'
include 'blk/primblk/strtbl/data.s'
include 'blk/primblk/primgrp/dense/data.s'
include 'blk/primblk/primgrp/ways/data.s'
include 'blk/primblk/primgrp/relations/data.s'
include 'features/data.s'
end namespace ; pbf
