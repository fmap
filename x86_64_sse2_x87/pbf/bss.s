; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace pbf
align 64
ctx					rq 1
start					rq 1
fd					rq 1 ; s32
stat					rb linux.stat_t.bytes_n
;---------------------------------------------------------------------------------------------------
; this ctx is actually more of a user client code parameter passing than anything else
ctx_t: namespace ctx_t
	; INPUT -- START ---------------------------------------------------------------------------
	; TODO: move to defined bit idx
	; [0] bbox.sw.lat user provided
	; [1] bbox.sw.lon user provided
	; [2] bbox.ne.lat user provided
	; [3] bbox.ne.lon user provided
	flags			:= 0	; qw
	file_path		:= 8	; qw
	tile_db_dir.path	:= 16	; qw
	zoom			:= 24	; byte
	; pad 3 bytes
	; little endian lat/lon
	bbox.sw			:= 32	; xword, vec2f64
		bbox.sw.lon	:= 32	; f64 nanodegrees 
		bbox.sw.lat	:= 40	; f64 nanodegrees
	bbox.ne			:= 48	; xword, vec2f64
		bbox.ne.lon	:= 48	; f64 nanodegrees
		bbox.ne.lat	:= 56	; f64 nanodegrees
	; INPUT -- END -----------------------------------------------------------------------------
	; PRIVATE -- START -------------------------------------------------------------------------
	zoom.scaled.f64		:= 64	; for wm computation (1<<zoom)
	zoom.scaled.s64		:= 72	; (1<<zoom) used instead or recomputing (1<<zoom)
	zoom.finish.vec2s64	:= 80	; x_finish(1<<zoom)/y_finish(1<<zoom) as an "invalid" value
	zoom.finish.vec2s64.x	:= 80
	zoom.finish.vec2s64.y	:= 88
	tile_db_dir.fd		:= 96	; s64
	; PRIVATE -- END ---------------------------------------------------------------------------
	bytes_n			:= 104
end namespace ; ctx_t
;---------------------------------------------------------------------------------------------------
include 'blob/bss.s'
include 'blk/primblk/strtbl/bss.s'
include 'blk/primblk/primgrp/dense/bss.s'
include 'blk/primblk/primgrp/ways/bss.s'
include 'blk/primblk/primgrp/relations/bss.s'
include 'features/bss.s'
end namespace ; pbf
