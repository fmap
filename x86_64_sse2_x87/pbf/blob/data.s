; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
blob.msg: namespace blob.msg
	inflate_mmap_failed db 'ERROR(%ld):PBF:BLOB:INIT_ONCE:unable to mmap memory for inflated data',10,0
	inflate_blob_failed db 'ERROR(%ld):PBF:BLOB:unable to inflate/decompress data',10,0
end namespace
