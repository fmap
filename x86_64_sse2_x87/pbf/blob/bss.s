; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
namespace blob
align 64
inflated rq 1
inflated.bytes_n_max = 32 * 1024 * 1024 ; 32 MiB (see specs)
end namespace
