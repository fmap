; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
hdr: namespace hdr
; scratch: rax rcx r11 r9
define pbf_finish	rbx
define p		r12
define p_finish		r13
	define blob_p	r13
define blob_p_finish	r14
define flags		r15
align_nops
parse:
.next:
	; first, can we read blob hdr sz ?
	mov rax, p
	add rax, 4 ; blob hdr sz is be32
	cmp rax, pbf_finish
		jae pbf.tile_db_build.relations_2nd_pass
	xor eax, eax
if ~SYSTEM_LEGACY
	movbe eax, [p] ; blob hdr sz is big endian, see specs
else
	mov eax, [p]
	bswap eax
end if
	add p, 4 ; skip "sz" bytes
	TRACE 'PBF:BLOB:HDR:start = %p:size = %lu(0x%lx) bytes', p, rax, rax
	mov p_finish, p 
	add p_finish, rax ; hdr.start + sizeof(hdr.sz) = blop_p
	mov blob_p_finish, p_finish ; will add the sz later, init now
	and flags, not 1111b ; clear blob_hdr parsing related flags
	; fall-thru
align_nops
.key_next:
	cmp p, p_finish
		je pbf.tile_db_build.relations_2nd_pass
	varint_load r11, p ; = key
	mov r9, r11 ; = key cpy
	shr r11, 3 ; = field num
	and r9, 111b ; = field type
	cmp r11, 3 ; field num == BlobHeader.datasize (see specs) ?
		je datasize_found
	cmp r11, 1 ; field num == BlobHeader.type (see specs) ?
		je type_found
	TRACE 'PBF:BLOB:HDR:MSG:UNHANDLED FIELD %lu OF TYPE %lu', r11, r9
	val_skip p, r9, r11
	jmp .key_next
align_nops
.key_epilog:
	mov rax, flags
	and rax, 1011b; got the type and the datasize ? yes, then we can parse the blob.
	cmp rax, 1011b
		je blob.parse ; we can jump since blob hdr finish is blob start
	jmp .key_next
align_nops
.return_from_blk: ; return from blk parsing code
	mov pbf_finish,	[rsp + 8 * 0]
	mov p,		[rsp + 8 * 1]
	add rsp, 8 * 2
	jmp .next
;---------------------------------------------------------------------------------------------------
align_nops
type_found:
	or flags, 1b ; flags[0] = 1 -> found the blob_hdr_type field
	varint_load r11, p ; = type_str_sz
	cmp r11, lengthof 'OSMData' ; the most common
		jne .type_str_sz_next
.OSMData_str_cmp: ; hardcoded short str cmp
	cmp [p], dword 'OSMD'
		jne .unknown
	cmp [p + 4], word 'at'
		jne .unknown
	cmp [p + 6], byte 'a' 
		jne .unknown
	TRACE 'PBF:BLOB:HDR:type = "OSMData"'
	; flags[1] = 1 -> blob_hdr_type is known
	; flags[2] = 1 -> following blob contains an osm data blk
	or flags, 110b 

	add p, lengthof 'OSMData'
	jmp parse.key_epilog
align_nops
.type_str_sz_next:
	cmp r11, lengthof 'OSMHeader'
		jne .unknown
.OSMHeader_str_cmp: ; hardcoded short str cmp
	mov rax, 'OSMHeade'
	cmp [p], rax ; max immediate is 32bits
		jne .unknown
	cmp [p + 8], byte 'r'
		jne .unknown
	TRACE 'PBF:BLOB:HDR:type = "OSMHeader"'
	; flags[1] = 1 -> blob_hdr_type is known
	; flags[2] = 0 -> following blob contains an osm hdr blk
	and flags, not 110b
	or flags, 10b 

	add p, lengthof 'OSMHeader'
	jmp parse.key_epilog
align_nops
.unknown:
	TRACE 'PBF:BLOB:HDR:type = "%.*s"(UNKNOWN STRING OF %lu bytes)', rbx, rsi, rbx
	and flags, not 10b ; flags[1] = 0 -> blob_hdr_type is unknown
	add p, r11 ; skip the unkwown str
	jmp parse.key_epilog
;---------------------------------------------------------------------------------------------------
align_nops
datasize_found:
	or flags, 1000b ; flags[3] = 1 -> blob_hdr_datasize was found
	varint_load r11, p
	TRACE 'PBF:BLOB:HDR:datasize = %lu(0x%lx) bytes', r11, r11
	add blob_p_finish, r11
	jmp parse.key_epilog
;---------------------------------------------------------------------------------------------------
purge pbf_finish
purge p
purge p_finish
purge blob_p
purge blob_p_finish
purge flags
end namespace ; hdr
