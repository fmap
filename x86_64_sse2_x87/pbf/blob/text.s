; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
blob: namespace blob
;===================================================================================================
include 'hdr/text.s'
;===================================================================================================
align_nops
init_once:
	TRACE 'PBF:BLOB:INIT_ONCE'
	mov rbp, rsp ; align the stack now for the external calls
	and rsp, not 0xf

	xor edi, edi
	mov rsi, inflated.bytes_n_max
	mov rdx, 1b or 10b	; PROT_READ  | PROT_WRITE
	mov r10, 0x22		; MAP_PRIVATE | MAP_ANONYMOUS
	xor r8d, r8d
	xor r9d, r9d
	mov rax, linux.mmap
	syscall
	cmp rax, linux.errno.last
		jae .err_inflate_mmap_failed
	mov [inflated], rax
	TRACE 'PBF:BLOB:INIT_ONCE:inflated=%p', rax
	stc ; return true
	mov rsp, rbp
	ret
.err_inflate_mmap_failed:
	mov rdi, 2 ; stderr
	lea rsi, [msg.inflate_mmap_failed]
	mov rdx, rax ; errno
	call qword [libc.dprintf]
	clc ; return false
	mov rsp, rbp
	ret
;---------------------------------------------------------------------------------------------------
; scratch: rax rcx r11 r9
define pbf_finish		rbx
	define blk_p		rbx
define hdr_p			r12
	define blk_p_finish	r12
define p			r13
define p_finish			r14
define flags			r15
define tmp_blk_p		rbp
define tmp_blk_p_d		ebp
define tmp_blk_p_finish		r8
; XXX it is reused, if you change it, check the available storage is enough where it is reused
stack_storage = 2 ; qword
align_nops
parse:
	sub rsp, stack_storage * 8 ; allocate
	xor tmp_blk_p_d, tmp_blk_p_d
	xor tmp_blk_p_finish, tmp_blk_p_finish
align_nops
.key_next:
	cmp p, p_finish
		je .epilog
	varint_load r11, p ; key
	mov r9, r11 ; = key
	shr r11, 3 ; = field num
	and r9, 111b ; = field type
	cmp r11, 1 ; field num = Blob.data.raw (see specs) ?
		je .data_raw_found
	cmp r11, 2 ; field num = Blob.raw_size (see specs) ?
		je .raw_size_found
	cmp r11, 3 ; field num = Blob.data.zlib_data (see specs) ?
		je .data_zlib_data_found
	TRACE 'PBF:BLOB:MSG:UNHANDLED FIELD %lu OF TYPE %lu', r11, r9
	val_skip p, r11, r9
	jmp .key_next
align_nops
.epilog:
	; get ready to parse the next blob hdr at the finish of this blob
	mov hdr_p, p_finish
	; return will be happening in blob hdr to pop the vals, reuse the 2 qwords of stack storage
	mov [rsp + 8 * 0], pbf_finish
	mov [rsp + 8 * 1], hdr_p
	mov blk_p, tmp_blk_p
	mov blk_p_finish, tmp_blk_p_finish
	TRACE 'PBF:BLK:start=%p:finish=%p bytes', blk_p, blk_p_finish
	jmp blk.parse; the blob is parsed, now parsed the blk
align_nops
.raw_size_found:
	varint_load r11, p ; = Blob.raw_size(int32)
	TRACE 'PBF:BLOB:raw_size=%lu(0x%lx) bytes', r11, r11
	add tmp_blk_p_finish, r11 ; may already have loaded blk_p, or 0
	jmp .key_next
align_nops
.data_raw_found:
	varint_load r11, p ; load the data sz
	TRACE 'PBF:BLOB:data.raw found, size=%lu(0x%lu) bytes', r11, r11
	mov tmp_blk_p, p
	add p, r11 ; skip blk
	mov tmp_blk_p_finish, p
	jmp .key_next
align_nops
.data_zlib_data_found:
	mov tmp_blk_p, [inflated]	; callee-saved reg
	mov rdi, tmp_blk_p 		; arg0
	mov rsi, inflated.bytes_n_max	; arg1
	varint_load r11, p		; deflate_bytes_n
	TRACE 'PBF:BLOB:data.zlib_data found, %lu(0x%lx) bytes', r11, r11
	mov rdx, p			; arg2 = deflate_bytes_start
	add p, r11 ; deflate_bytes_start + deflate_bytes_n = deflate_bytes_finish
	mov rcx, r11			; arg3 = deflate_bytes_n
	mov [rsp], tmp_blk_p_finish	; save r8
	call inflate.do ; will do some C ABI external calls, expect usual clobbers
		jnc .err_inflate_blob_failed
	mov tmp_blk_p_finish, [rsp]	; restore r8
	add tmp_blk_p_finish, tmp_blk_p ; may already contain the raw size, or 0
	jmp .key_next

.err_inflate_blob_failed:
	mov rdi, 2 ; stderr
	mov rsi, rax ; errno
	lea rsi, [msg.inflate_blob_failed]
	call qword [libc.dprintf]
	mov rax, 1 ; return 1
	add rsp, stack_storage * 8 ; free
	jmp pbf.tile_db_build.exit_nok
;---------------------------------------------------------------------------------------------------
purge pbf_finish
purge blk_p
purge hdr_p
purge blk_p_finish
purge p
purge p_finish
purge flags
purge tmp_blk_p
purge tmp_blk_p_d
purge tmp_blk_p_finish
end namespace ; blob
