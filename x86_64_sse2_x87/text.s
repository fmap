; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
public fmap_entry
pbf_ctx_init:
	mov qword [pbf_ctx + pbf.ctx_t.flags], 0
	mov qword [pbf_ctx + pbf.ctx_t.file_path], 0
	mov qword [pbf_ctx + pbf.ctx_t.tile_db_dir.path], 0
	mov byte [pbf_ctx + pbf.ctx_t.zoom], 16

	mov rax, 0.0
	mov qword [pbf_ctx + pbf.ctx_t.bbox.sw.lat], rax 
	mov qword [pbf_ctx + pbf.ctx_t.bbox.sw.lon], rax 
	mov qword [pbf_ctx + pbf.ctx_t.bbox.ne.lat], rax 
	mov qword [pbf_ctx + pbf.ctx_t.bbox.ne.lon], rax 
	ret
;===================================================================================================
; naive
define p		rdi ; in
define digit		rax
define digit_b		al
define val		rcx
	define val_d	ecx
define sign		rdx
	define sign_d	edx
;---------------------------------------------------------------------------------------------------
dec2s64:
	xor val_d, val_d
	xor sign_d, sign_d
	; gobble up negative sign if any
	movzx digit, byte [p]
	test digit, digit ; 0 terminator
		jz .exit
	cmp digit_b, '-'
		jne .digit_acc
	sub sign, 1 ; -1
.next_digit:
	add p, 1
	movzx digit, byte [p]
	test digit, digit ; 0 terminator
		jz .exit
.digit_acc:
	sub digit, '0'
	; 123 = ((0 * 10 + 1) * 10 + 2) * 10 + 3
	lea val, [val + val * 4] ; val = val * 5
	lea val, [digit + val * 2] ; val = digit + val * 10
	jmp .next_digit
.exit:
	mov rax, val
	; neg(x) = not(x) + 1 = not(x + (-1)), modulo boolean arithmetics
	add rax, sign
	xor rax, sign ; xor(x,-1) = not(x)
	ret
;---------------------------------------------------------------------------------------------------
purge p
purge digit
purge digit_b
purge val
purge val_d
purge sign
purge sign_d
;===================================================================================================
usage_print:
	enter 0,0
	and rsp, not 0xf
	lea rdi, [msg.args.usage]
	call qword [libc.printf]
	leave
	ret
;===================================================================================================
; in: rdi = argv
define argv	rbx
define arg	r12
define flags	r13
define flags_d	r13d ; [0] have pbf file path, [1] have tile db directory path
;---------------------------------------------------------------------------------------------------
opts_parse:
	mov argv, rdi
	xor flags_d, flags_d
.next_arg:
	add argv, 8 ; will skip argv[0] = command line program file path
	mov arg, [argv]
	test arg, arg
		jz .exit_ok
	; options are max 8 ascii chars (64bits) including the '-' prefix
	mov rax, '-h'
	call .cmp
		jc .usage_print
	mov rax, '-z'
	call .cmp
		jc .zoom
	mov rax, '-swlat'
	call .cmp
		jc .swlat
	mov rax, '-swlon'
	call .cmp
		jc .swlon
	mov rax, '-nelat'
	call .cmp
		jc .nelat
	mov rax, '-nelon'
	call .cmp
		jc .nelon
	bts flags, 0
		jc .tile_db_dir_path
	mov qword [pbf_ctx + pbf.ctx_t.file_path], arg
	jmp .next_arg
.tile_db_dir_path:
	bts flags, 1
		jc .next_arg
	mov qword [pbf_ctx + pbf.ctx_t.tile_db_dir.path], arg
	jmp .next_arg
.exit_ok:
	stc
	ret
.exit_nok:
	clc
	ret
;---------------------------------------------------------------------------------------------------
define opt		rax ; in
define opt_byte		al
define arg_byte_p	rdx
.cmp:
	mov arg_byte_p, arg
.cmp.next_byte:
	mov cl, [arg_byte_p]
	test cl, cl
		jz .cmp.neq
	cmp cl, opt_byte
		jne .cmp.neq
	add arg_byte_p, 1
	shr opt, 8
	test opt_byte, opt_byte
		jz .cmp.eq
	jmp .cmp.next_byte
.cmp.neq:
	clc	; return false
	ret
.cmp.eq:
	stc	; return true
	ret
purge opt
purge opt_byte
purge arg_byte_p
;---------------------------------------------------------------------------------------------------
.usage_print:
	call usage_print
	jmp .next_arg
;---------------------------------------------------------------------------------------------------
.zoom:
	add argv, 8
	mov arg, [argv]
	test arg, arg ; 0-terminating ptr of argv ?
		jz .zoom.err_val_missing

	mov rdi, arg
	call dec2s64
	mov [pbf_ctx + pbf.ctx_t.zoom], al

	jmp .next_arg
.zoom.err_val_missing:
	mov rbp, rsp
	and rsp, not 0xf
	mov rdi, 2
	lea rsi, [msg.args.zoom.err_val_missing]
	call qword [libc.dprintf]
	mov rsp, rbp
	jmp .exit_nok
;---------------------------------------------------------------------------------------------------
.swlat:
	add argv, 8
	mov arg, [argv]
	test arg, arg
		jz .swlat.err_val_missing
	
	mov rdi, arg
	call dec2s64
	cvtsi2sd xmm0, rax
	movsd qword [pbf_ctx + pbf.ctx_t.bbox.sw.lat], xmm0
	or qword [pbf_ctx + pbf.ctx_t.flags], 1 ; flags[0] = 1, bbox.sw.lat user on

	jmp .next_arg
.swlat.err_val_missing:
	mov rbp, rsp
	and rsp, not 0xf
	mov rdi, 2
	lea rsi, [msg.args.swlat.err_val_missing]
	call qword [libc.dprintf]
	mov rsp, rbp
	jmp .exit_nok
;---------------------------------------------------------------------------------------------------
.swlon:
	add argv, 8
	mov arg, [argv]
	test arg, arg
		jz .swlon.err_val_missing

	mov rdi, arg
	call dec2s64
	cvtsi2sd xmm0, rax
	movsd qword [pbf_ctx + pbf.ctx_t.bbox.sw.lon], xmm0
	or qword [pbf_ctx + pbf.ctx_t.flags], 10b ; flags[1] = 1, bbox.sw.lon user on

	jmp .next_arg
.swlon.err_val_missing:
	mov rbp, rsp
	and rsp, not 0xf
	mov rdi, 2
	lea rsi, [msg.args.swlon.err_val_missing]
	call qword [libc.dprintf]
	mov rsp, rbp
	jmp .exit_nok
;---------------------------------------------------------------------------------------------------
.nelat:
	add argv, 8
	mov arg, [argv]
	test arg, arg
		jz .nelat.err_val_missing

	mov rdi, arg
	call dec2s64
	cvtsi2sd xmm0, rax
	movsd qword [pbf_ctx + pbf.ctx_t.bbox.ne.lat], xmm0
	or qword [pbf_ctx + pbf.ctx_t.flags], 100b ; flags[2] = 1, bbox.ne.lat user on

	jmp .next_arg
.nelat.err_val_missing:
	mov rbp, rsp
	and rsp, not 0xf
	mov rdi, 2
	lea rsi, [msg.args.nelat.err_val_missing]
	call qword [libc.dprintf]
	mov rsp, rbp
	jmp .exit_nok
;---------------------------------------------------------------------------------------------------
.nelon:
	add argv, 8
	mov arg, [argv]
	test arg, arg
		jz .nelon.err_val_missing

	mov rdi, arg
	call dec2s64
	cvtsi2sd xmm0, rax
	movsd qword [pbf_ctx + pbf.ctx_t.bbox.ne.lon], xmm0
	or qword [pbf_ctx + pbf.ctx_t.flags], 1000b ; flags[3] = 1, bbox.ne.lon user on

	jmp .next_arg
.nelon.err_val_missing:
	mov rbp, rsp
	and rsp, not 0xf
	mov rdi, 2
	lea rsi, [msg.args.nelon.err_val_missing]
	call qword [libc.dprintf]
	mov rsp, rbp
	jmp .exit_nok
;---------------------------------------------------------------------------------------------------
purge argv
purge arg
purge flags
purge flags_d
;===================================================================================================
define argc 		rbx
define argv 		r12
define pbf_result	r13
fmap_entry:
	; x86_64 ABI -- START
	mov [abi.before_exit_func], rdx
	mov argc, [rsp]
	lea argv, [rsp + 8]
	xor ebp, ebp
	; x86_64 ABI -- END

	call libc.init_once
		jnc .exit_nok
	call pbf_ctx_init

	mov rdi, argv
	call opts_parse
		jnc .exit_nok
	call inflate.init_once
		jnc .exit_nok
	call pbf.init_once
		jnc .exit_nok
	lea rdi, [pbf_ctx]
	call pbf.tile_db_build
	mov pbf_result, rax ; keep it to return it to the system
	call pbf.stats.print
;---------------------------------------------------------------------------------------------------
.exit_ok:
	; we must flush stdout (bug from glibc ELF DT_FINI_ARRAY?)
	mov rax, [libc.stdout]
	mov rdi, [rax]
	call [libc.fflush]

	; x86_64 ABI -- START
	mov rax, [abi.before_exit_func]
	test rax, rax
		jz .no_before_exit_func
	call rax 
.no_before_exit_func:
	mov rax, linux.exit_group
	mov rdi, pbf_result
	syscall ; no return
	; x86_64 ABI -- END
;---------------------------------------------------------------------------------------------------
.exit_nok:
	; we must flush stdout (bug from glibc ELF DT_FINI_ARRAY?)
	mov rax, [libc.stdout]
	mov rdi, [rax]
	call [libc.fflush]

	; x86_64 ABI -- START
	mov rax, [abi.before_exit_func]
	test rax, rax
		jz .exit_nok.no_before_exit_func
	call rax 
	call [abi.before_exit_func]
.exit_nok.no_before_exit_func:
	mov rax, linux.exit_group
	mov rdi, 1
	syscall ; no return
	; x86_64 ABI -- END
;---------------------------------------------------------------------------------------------------
purge argc
purge argv
purge pbf_result
