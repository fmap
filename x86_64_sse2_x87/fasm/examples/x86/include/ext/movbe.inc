; CPUID Fn0000_0001_ECX[MOVBE] = 1
calminstruction movbe? dest*,src*
	asmcmd	=x86.=parse_operand =@dest,dest
	asmcmd	=x86.=parse_operand =@src,src

	local	size

	check	@dest.size <> 0 &  @src.size <> 0 & @dest.size <> @src.size
	jyes	operand_sizes_do_not_match

	compute size, @dest.size or @src.size
	check	size > 1
	jno	invalid_operand_size

    main:
	check	@dest.type = 'reg' & @src.type = 'mem'
	jyes	movbe_reg_mem
	check	@dest.type = 'mem' & @src.type = 'reg'
	jyes	movbe_mem_reg

    invalid_combination_of_operands:
	asmcmd	=err 'invalid combination of operands'
	exit

    movbe_reg_mem:
	asmcmd	=x86.=select_operand_prefix =@src,size
	asmcmd	=x86.=store_instruction <0Fh,38h,0F0h>,=@src,=@dest.=rm
	exit

    movbe_mem_reg:
	asmcmd	=x86.=select_operand_prefix =@dest,size
	asmcmd	=x86.=store_instruction <0Fh,38h,0F1h>,=@dest,=@src.=rm
	exit

    invalid_operand_size:
	asmcmd	err 'invalid operand size'
	compute size, 0
	jump	main

    operand_sizes_do_not_match:
	asmcmd	err 'operand sizes do not match'
	compute size, 0
	jump	main
end calminstruction
