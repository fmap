; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
linux: namespace linux
;{{{ stat_t ----------------------------------------------------------------------------------------
stat_t: namespace stat_t
	dev		:=0		; qw/u
	ino		:=8		; qw/u
	nlink		:=16		; qw/u

	mode		:=24		; dw/u
	uid		:=28		; dw/u
	gid		:=32		; dw/u
	; 1 dw pad
	rdev		:=40		; qw/u
	sz		:=48		; qw/s
	blk_sz		:=56		; qw/s
	blks		:=64		; qw/s, num of 512-bytes blocks allocated

	atime		:=72		; qw/u
	atime_nsec	:=80		; qw/u
	mtime		:=88		; qw/u
	mtime_nsec	:=96		; qw/u
	ctime		:=104		; qw/u
	ctime_nsec	:=112		; qw/u
	bytes_n		:= 144
	; 3 qws pad
end namespace ;}}} stat_t --------------------------------------------------------------------------
errno.last := -4096
; syscalls
fstat		:= 5
write		:= 1
open		:= 2	; not on arm64, use openat
mmap		:= 9
munmap		:= 11
mremap		:= 25
exit_group	:= 231
openat		:= 257
mkdirat		:= 258
fstatat		:= 262

AT_FDCWD	:= -100
O_RDONLY	:= 001b
O_WRONLY	:= 010b
O_RDWR		:= 100b
O_CREAT		:= 1000000b
O_TRUNC		:= 1000000000b
O_DIRECTORY	:= 010000000000000000b
end namespace
