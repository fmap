; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
geo_to_wm:
; May significantly change once we have an optimized taylor series for direct computation of wm
; coords, and once we switch to full avx2.
define scaled_zoom_p	rdi	; in (1 << zoom) f64
; in blk, vec2f64 of geo coords
define lon		rsi + 8 * 0	; f64
define lat		rsi + 8 * 1	; f64
; out blk pointed by rcx, 2 vec2s64: vec of global tile coords then vec of location coords into the tile
define tile_x		rdx + 8 * 0 ; s64
define tile_y		rdx + 8 * 1 ; s64
define tile_u		rdx + 8 * 2 ; f64, location into the tile, [0,1[
define tile_v		rdx + 8 * 3 ; f64, location into the tile, [0,1[
.f64: ; lat/lon are f64
	; setup the floor fn (expect the "nearest" rounding mode being set)
	fnstcw [.cw]
	or word [.cw], 10000000000b ; round down = floor
	fldcw [.cw]
	; --- x and y
	fld qword [scaled_zoom_p]
	fld st0
	; --- x
	fld qword [.f64.360_billion]
	fld qword [.f64.180_billion]
	fld qword [lon]
	faddp ; lon + 180.0
	fdivrp ; (lon + 180.0) / 360.0 , billions are going away here
	fmulp ; (lon + 180.0) / 360.0 * (1 << zoom)
	fld st0 ; st0 = x, st1 = x
	frndint ; st0 = floor(x) = tile_x, st1 = x
	fsub st1, st0; st0 = floor(x) = tile_x, st1 = x - floor(x) = tile_u
	fistp qword [tile_x] ; st0 = tile_u
	fstp qword [tile_u]
	; --- y
	fld qword [.f64.180_billion]
	fld qword [lat]
	fldpi
	fmulp ; lat * PI
	fdivrp ; lat * PI / 180.0 = lat_rad , billions are going away here
	fsincos
	fxch ; st0 = sin(lat_rad), st1 = cos(lat_rad)
	fld1 ; st0 = 1.0, st1 = sin(lat_rad), st2 = cos(lat_rad)
	faddp ; st0 = 1.0 + sin(lat_rad), st1 = cos(lat_rad)
	fdivrp ; (1.0 + sin(lat_rad)) / cos(lat_rad)
	fldln2 ; ln(2)
	fxch
	fyl2x ; arsinh(tan(la_rad)) = ln((1.0 + sin(lat_rad)) / cos(lat_rad))
	fldpi
	fdivp ; arsinh(tan(la_rad) / PI
	fld1
	fsubrp ; 1.0 - (arsinh(tan(la_rad)) / PI)
	fld1
	fld1
	fscale ; 2.0
	fxch
	ffree st0 ; pop 1.0
	fincstp
	fdivp ; (1.0 - (arsinh(tan(la_rad)) / PI)) / 2.0
	fmulp ; (1.0 - (arsinh(tan(la_rad)) / PI)) / 2.0 * (1 << zoom)
	fld st0 ; st0 = y, st1 = y
	frndint ; st0 = floor(y) = tile_y, st1 = y
	fsub st1, st0 ; st0 = floor(y) = tile_y, st1 = y - floor(y) = tile_v
	fistp qword [tile_y] ; st0 = tile_v
	fstp qword [tile_v]
	ret
align 64
.f64.360_billion	dq 360000000000.0
.f64.180_billion	dq 180000000000.0
.cw			dw 0
purge scaled_zoom_p
purge lat_p
purge lon_p
purge tile_x
purge tile_y
purge tile_v
purge tile_u
