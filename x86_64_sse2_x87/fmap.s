; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
define SYSTEM_LEGACY 1
;***************************************************************************************************
include 'x64.inc'
if ~SYSTEM_LEGACY
	include 'ext/movbe.inc'
	include 'ext/avx2.inc'
else
	include 'ext/ssse3.inc'
end if
include 'format/elf64.inc'
use64
;***************************************************************************************************
define TRACE_DEBUG_ENABLED 0
define TRACE_GENERIC_ENABLED 0
define TRACE_NODE_IDX_ENABLED 0
define TRACE_WAYS_IDX_ENABLED 0
define TRACE_RELATIONS_IDX_ENABLED 0
define TRACE_RELATIONS_2ND_PASS_ENABLED 0
define TRACE_FEATURES_ENABLED 0
;---------------------------------------------------------------------------------------------------
include 'util_macros.s'
;***************************************************************************************************
include 'linux.s'
;***************************************************************************************************
section '.text' executable writeable align 64
include 'libc/text.s'
include 'inflate/zlib/text.s'
include 'pbf/text.s'
include 'text.s'
;***************************************************************************************************
section '.data' writeable align 64
include 'libc/data.s'
include 'inflate/zlib/data.s'
include 'pbf/data.s'
include 'data.s'
;***************************************************************************************************
section '.bss' writeable align 64
include 'libc/bss.s'
include 'inflate/zlib/bss.s'
include 'pbf/bss.s'
include 'bss.s'
