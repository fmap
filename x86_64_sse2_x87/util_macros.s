; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
; this is the basic macro, then different tracing macros are enabled or not
macro TRACE_PREFIX prefix, fmt, regs&
	local fmt_store
	local exit
	local greg_vals
	local dqreg_vals

	iterate reg, rax, rcx, rdx, rbx, rsp, rbp, rsi, rdi, r8, r9, r10, r11, r12, r13, r14, r15
		mov [greg_vals + 8 * (% - 1)], reg
	end iterate
	iterate reg, xmm0, xmm1, xmm2, xmm3, xmm4, xmm6, xmm7
		movdqa [dqreg_vals + 16 * (% - 1)], reg
	end iterate
	
	iterate reg, regs	
		local reg_idx
		local dest_reg_num
		if %% > 4
			err 'TRACE handle only 4 registers',10
		end if
		dest_reg_num = %

		; we must go internals because we could use 'register labels' a 64bits register is
		; an element with its metadata being a polynomial of element x86.r64 with its
		; constant being the register idx.
		reg_idx = (reg metadata 1) scale 0
		if reg_idx < 0 | 15 < reg_idx
			err 'BAD REG IDX',10
		end if
		iterate dest_reg, rdx, rcx, r8, r9
			if dest_reg_num = %
				mov dest_reg, [greg_vals + 8 * reg_idx]
				break
			end if
		end iterate
	end iterate

	mov rbp, rsp
	and rsp, not 0xf
	mov rdi, 2 ; the unbuffered stderr
	lea rsi, [fmt_store]
	call qword [libc.dprintf]
	mov rsp, rbp

	iterate reg, xmm0, xmm1, xmm2, xmm3, xmm4, xmm6, xmm7
		movdqa reg, [dqreg_vals + 16 * (% - 1)]
	end iterate
	iterate reg, rax, rcx, rdx, rbx, rsp, rbp, rsi, rdi, r8, r9, r10, r11, r12, r13, r14, r15
		mov reg, [greg_vals + 8 * (% - 1)]
	end iterate
	jmp exit
if prefix <> '' ; cmp str tokens should be ok I guess
fmt_store db 'TRACE:',prefix,':',fmt,10,0
else
fmt_store db 'TRACE:',fmt,10,0
end if
greg_vals dq 16 dup 0
align 16 ; 128 bits
dqreg_vals ddq 8 dup 0
exit:
end macro ; TRACE_PREFIX
;---------------------------------------------------------------------------------------------------
if TRACE_DEBUG_ENABLED
	macro T fmt, regs&
		TRACE_PREFIX '',fmt, regs
	end macro
else
	macro T fmt, regs&
	end macro
end if
;---------------------------------------------------------------------------------------------------
if TRACE_GENERIC_ENABLED
	macro TRACE fmt, regs&
		TRACE_PREFIX '',fmt, regs
	end macro
else
	macro TRACE fmt, regs&
	end macro
end if
;---------------------------------------------------------------------------------------------------
macro zreg reg ; zero reg
	if `reg = 'rax'
		xor eax, eax
	else if `reg = 'rbx'
		xor ebx, ebx
	else if `reg = 'rcx'
		xor ecx, ecx
	else if `reg = 'rdx'
		xor edx, edx
	else if `reg = 'rbp'
		xor ebp, ebp
	else if `reg = 'rsi'
		xor esi, esi
	else if `reg = 'rdi'
		xor edi, edi
	else
		xor reg, reg ; r8-r15, no gain at using eXX
	end if
end macro
; heavything rip
; align_macros.inc: multibyte nop alignment macros
; carefull of the new indirect branch declaration "nop" from latest micro-architecture
macro align_nops
	local a
	virtual
		align 16
		a = $ - $$
	end virtual
	if a = 15
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0x66, 0xf, 0x1f, 0x44, 0x00, 0x00
	else if a = 14
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0xf, 0x1f, 0x44, 0x00, 0x00
	else if a = 13
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0xf, 0x1f, 0x40, 0x00
	else if a = 12
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0xf, 0x1f, 0x00
	else if a = 11
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0x66, 0x90
	else if a = 10
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0x90
	else if a = 9
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
	else if a = 8
		db 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
	else if a = 7
		db 0xf, 0x1f, 0x80, 0x00, 0x00, 0x00, 0x00
	else if a = 6
		db 0x66, 0xf, 0x1f, 0x44, 0x00, 0x00
	else if a = 5
		db 0xf, 0x1f, 0x44, 0x00, 0x00
	else if a = 4
		db 0xf, 0x1f, 0x40, 0x00
	else if a = 3
		db 0xf, 0x1f, 0x00
	else if a = 2
		db 0x66, 0x90
	else if a = 1
		db 0x90
	end if
end macro
;===================================================================================================
TSC='visible'
; TODO: get rid of struct and go of tbl
;if PERF_ENABLED
;;---------------------------------------------------------------------------------------------------
;struc (name) TSC.STORAGE msg_str
;	name.start	dq 0
;	name.stop	dq 0
;	name.msg	db msg_str,10,0
;	align 16
;	name.gregs_save rq 16
;	name.vregs_save rdq 8
;end struc
;;---------------------------------------------------------------------------------------------------
;macro TSC.SAMPLE storage, dst
;	iterate reg, rax, rcx, rdx, rbx, rsp, rbp, rsi, rdi, r8, r9, r10, r11, r12, r13, r14, r15
;		mov [storage.gregs_save + 8 * (% - 1)], reg
;	end iterate
;	iterate reg, xmm0, xmm1, xmm2, xmm3, xmm4, xmm6, xmm7
;		movdqa [storage.vregs_save + 16 * (% - 1)], reg
;	end iterate
;
;	lfence
;	rdtsc
;
;	mov rcx, 0xffffffff
;	and rax, rcx
;	and rdx, rcx
;
;	shl rdx, 32
;	or edx, eax
;	mov [storage.dst], rdx
;
;	iterate reg, xmm0, xmm1, xmm2, xmm3, xmm4, xmm6, xmm7
;		movdqa reg, [storage.vregs_save + 16 * (% - 1)]
;	end iterate
;	iterate reg, rax, rcx, rdx, rbx, rsp, rbp, rsi, rdi, r8, r9, r10, r11, r12, r13, r14, r15
;		mov reg, [storage.gregs_save + 8 * (% - 1)]
;	end iterate
;end macro
;;---------------------------------------------------------------------------------------------------
;macro TSC.PRINT storage
;	iterate reg, rax, rcx, rdx, rbx, rsp, rbp, rsi, rdi, r8, r9, r10, r11, r12, r13, r14, r15
;		mov [storage.gregs_save + 8 * (% - 1)], reg
;	end iterate
;	iterate reg, xmm0, xmm1, xmm2, xmm3, xmm4, xmm6, xmm7
;		movdqa [storage.vregs_save + 16 * (% - 1)], reg
;	end iterate
;
;	mov rbp, rsp
;	and rsp, not 0xf
;	mov rdi, 2			; arg0 = stderr 
;	lea rsi, [storage.msg]		; arg1 = msg
;	mov rdx, [storage.stop]
;	sub rdx, [storage.start] 	; arg2 = TSC diff
;	call qword [libc.dprintf]
;	mov rsp, rbp
;
;	iterate reg, xmm0, xmm1, xmm2, xmm3, xmm4, xmm6, xmm7
;		movdqa reg, [storage.vregs_save + 16 * (% - 1)]
;	end iterate
;	iterate reg, rax, rcx, rdx, rbx, rsp, rbp, rsi, rdi, r8, r9, r10, r11, r12, r13, r14, r15
;		mov reg, [storage.gregs_save + 8 * (% - 1)]
;	end iterate
;end macro
;else ; PERF_ENABLED
macro TSC.PRINT storage
end macro
macro TSC.SAMPLE storage, dst
end macro
struc (name) TSC.STORAGE msg_str
end struc
;end if
;===================================================================================================
